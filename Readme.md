# Halo 5 Hackathon

Consuming `Retrofit` 2.0 and `RealmIO` with reactive extensions (`RxAndroid`) on an example of the public Halo 5 API

## Features

* Fancy Android Support Library (23) shit (Collapsing Toolbar, NavigationView, TabbedViewpagers)
* `Glide` (`Picasso` replacement) test 
* Substitute `AsyncTasks` and `AsyncLoaders` with RxAndroid
* Using `Ralmio` to persist POJO Models fetched with `Retrofit` 2.0, parsed with `GSON`

## Changelog

Version 0.5
-----------

* New: Leaderboards
* Improved: Error Handling

Version 0.4 00e62d7 *(2016-05-05)*
----------------------------------

* New: Collapsing Toolbar
* New: Implement Loader Pattern with RxSubscriptions
* New: `NavigationDrawer`
* New: Fagment implementations
    * `MapsFragment`
    * `CampaignMissionsFragment`
* New: `Realmio` to store MetaData
* Removed: `Retrolambda`


Version 0.3 5ee40fc *(2016-04-15)*
----------------------------------

* New: Models for the `MetadataRestInterface`
* New: `MetadataRestInterface` implementation
* New: Models for the `StatsRestInterface`
* New: `StatsRestInterface` implementation
* New: Models for the `ProfileRestInterface`
* New: `ProfileRestInterface` implementation
* New: RxAndroid adapter
* New: impoved `LoggingInterceptor`
* New: `BaseObserver`, `SimpleObserver`
* New: `RxUtils`:
    * `RedirectObserver`
    * `TextEventStringConverter`
    * `TextIsEmptyFilter`
    * `ExceptionUtl` to gracefully print exceptions
* New: `ImageLoader` Picasso Implementation
* New: `ProgressBar` Event implementation


Version 0.2 52bf782 *(2016-04-06)*
----------------------------

* New: `Gson`
* New: Basic Rest implementation (Interfaces, Models, Utility, Controllers)
* New: `LoggingInterceptor` - Custom HttpLoggingInterceptor for Request/Response logs

Version 0.1 b398ace *(2016-04-05)*
----------------------------

* New: Retrolambda, Retrofit and RxAndroid setup

Version 0.1 24518b0 *(2016-04-05)*
----------------------------

* New: Project setup