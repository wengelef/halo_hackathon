
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Impulse {

    @SerializedName("Id")
    @Expose
    private Long Id;
    @SerializedName("Count")
    @Expose
    private Integer Count;

    /**
     * 
     * @return
     *     The Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The Count
     */
    public Integer getCount() {
        return Count;
    }

    /**
     * 
     * @param Count
     *     The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

}
