
package example.wengelef.hackathon.rest.model.stats.servicerecordcustom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceRecordResult {

    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("ResultCode")
    @Expose
    private Integer ResultCode;
    @SerializedName("Result")
    @Expose
    private example.wengelef.hackathon.rest.model.stats.servicerecordcustom.Result Result;

    /**
     * 
     * @return
     *     The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The ResultCode
     */
    public Integer getResultCode() {
        return ResultCode;
    }

    /**
     * 
     * @param ResultCode
     *     The ResultCode
     */
    public void setResultCode(Integer ResultCode) {
        this.ResultCode = ResultCode;
    }

    /**
     * 
     * @return
     *     The Result
     */
    public example.wengelef.hackathon.rest.model.stats.servicerecordcustom.Result getResult() {
        return Result;
    }

    /**
     * 
     * @param Result
     *     The Result
     */
    public void setResult(example.wengelef.hackathon.rest.model.stats.servicerecordcustom.Result Result) {
        this.Result = Result;
    }

}
