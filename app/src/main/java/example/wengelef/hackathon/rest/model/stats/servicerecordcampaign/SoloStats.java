
package example.wengelef.hackathon.rest.model.stats.servicerecordcampaign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SoloStats {

    @SerializedName("Stat")
    @Expose
    private Statistic Statistic;

    /**
     * 
     * @return
     *     The Stat
     */
    public Statistic getStatistic() {
        return Statistic;
    }

    /**
     * 
     * @param Statistic
     *     The Stat
     */
    public void setStatistic(Statistic Statistic) {
        this.Statistic = Statistic;
    }

}
