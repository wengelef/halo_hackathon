
package example.wengelef.hackathon.rest.model.stats.servicerecordcustom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import example.wengelef.hackathon.rest.model.stats.PlayerId;

public class Result {

    @SerializedName("CustomStats")
    @Expose
    private CustomStats CustomStats;
    @SerializedName("PlayerId")
    @Expose
    private PlayerId PlayerId;
    @SerializedName("SpartanRank")
    @Expose
    private Integer SpartanRank;
    @SerializedName("Xp")
    @Expose
    private Integer Xp;

    /**
     * 
     * @return
     *     The CustomStats
     */
    public CustomStats getCustomStats() {
        return CustomStats;
    }

    /**
     * 
     * @param CustomStats
     *     The CustomStats
     */
    public void setCustomStats(CustomStats CustomStats) {
        this.CustomStats = CustomStats;
    }

    /**
     * 
     * @return
     *     The PlayerId
     */
    public PlayerId getPlayerId() {
        return PlayerId;
    }

    /**
     * 
     * @param PlayerId
     *     The PlayerId
     */
    public void setPlayerId(PlayerId PlayerId) {
        this.PlayerId = PlayerId;
    }

    /**
     * 
     * @return
     *     The SpartanRank
     */
    public Integer getSpartanRank() {
        return SpartanRank;
    }

    /**
     * 
     * @param SpartanRank
     *     The SpartanRank
     */
    public void setSpartanRank(Integer SpartanRank) {
        this.SpartanRank = SpartanRank;
    }

    /**
     * 
     * @return
     *     The Xp
     */
    public Integer getXp() {
        return Xp;
    }

    /**
     * 
     * @param Xp
     *     The Xp
     */
    public void setXp(Integer Xp) {
        this.Xp = Xp;
    }

}
