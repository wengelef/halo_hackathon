
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MatchEvents {

    @SerializedName("GameEvents")
    @Expose
    private List<GameEvent> GameEvents = new ArrayList<GameEvent>();
    @SerializedName("IsCompleteSetOfEvents")
    @Expose
    private Boolean IsCompleteSetOfEvents;
    @SerializedName("Links")
    @Expose
    private Object Links;

    /**
     * 
     * @return
     *     The GameEvents
     */
    public List<GameEvent> getGameEvents() {
        return GameEvents;
    }

    /**
     * 
     * @param GameEvents
     *     The GameEvents
     */
    public void setGameEvents(List<GameEvent> GameEvents) {
        this.GameEvents = GameEvents;
    }

    /**
     * 
     * @return
     *     The IsCompleteSetOfEvents
     */
    public Boolean getIsCompleteSetOfEvents() {
        return IsCompleteSetOfEvents;
    }

    /**
     * 
     * @param IsCompleteSetOfEvents
     *     The IsCompleteSetOfEvents
     */
    public void setIsCompleteSetOfEvents(Boolean IsCompleteSetOfEvents) {
        this.IsCompleteSetOfEvents = IsCompleteSetOfEvents;
    }

    /**
     * 
     * @return
     *     The Links
     */
    public Object getLinks() {
        return Links;
    }

    /**
     * 
     * @param Links
     *     The Links
     */
    public void setLinks(Object Links) {
        this.Links = Links;
    }

}
