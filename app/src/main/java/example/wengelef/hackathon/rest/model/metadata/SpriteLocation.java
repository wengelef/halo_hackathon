
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SpriteLocation extends RealmObject {

    @SerializedName("spriteSheetUri")
    @Expose
    private String spriteSheetUri;
    @SerializedName("left")
    @Expose
    private String left;
    @SerializedName("top")
    @Expose
    private String top;
    @SerializedName("width")
    @Expose
    private String width;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("spriteWidth")
    @Expose
    private String spriteWidth;
    @SerializedName("spriteHeight")
    @Expose
    private String spriteHeight;

    /**
     * 
     * @return
     *     The spriteSheetUri
     */
    public String getSpriteSheetUri() {
        return spriteSheetUri;
    }

    /**
     * 
     * @param spriteSheetUri
     *     The spriteSheetUri
     */
    public void setSpriteSheetUri(String spriteSheetUri) {
        this.spriteSheetUri = spriteSheetUri;
    }

    /**
     * 
     * @return
     *     The left
     */
    public String getLeft() {
        return left;
    }

    /**
     * 
     * @param left
     *     The left
     */
    public void setLeft(String left) {
        this.left = left;
    }

    /**
     * 
     * @return
     *     The top
     */
    public String getTop() {
        return top;
    }

    /**
     * 
     * @param top
     *     The top
     */
    public void setTop(String top) {
        this.top = top;
    }

    /**
     * 
     * @return
     *     The width
     */
    public String getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(String width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The height
     */
    public String getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The spriteWidth
     */
    public String getSpriteWidth() {
        return spriteWidth;
    }

    /**
     * 
     * @param spriteWidth
     *     The spriteWidth
     */
    public void setSpriteWidth(String spriteWidth) {
        this.spriteWidth = spriteWidth;
    }

    /**
     * 
     * @return
     *     The spriteHeight
     */
    public String getSpriteHeight() {
        return spriteHeight;
    }

    /**
     * 
     * @param spriteHeight
     *     The spriteHeight
     */
    public void setSpriteHeight(String spriteHeight) {
        this.spriteHeight = spriteHeight;
    }

}
