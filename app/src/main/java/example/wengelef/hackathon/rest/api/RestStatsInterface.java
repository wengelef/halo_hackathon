package example.wengelef.hackathon.rest.api;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import example.wengelef.hackathon.rest.model.stats.Leaderboards;
import example.wengelef.hackathon.rest.model.stats.MatchEvents;
import example.wengelef.hackathon.rest.model.stats.PlayerMatches;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportArena;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCampaign;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCustom;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportWarzone;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ServiceRecordArena;
import example.wengelef.hackathon.rest.model.stats.servicerecordcampaign.ServiceRecordCampaign;
import example.wengelef.hackathon.rest.model.stats.servicerecordcustom.ServiceRecordCustom;
import example.wengelef.hackathon.rest.model.stats.servicerecordwarzone.ServiceRecordWarzone;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by fwengelewski on 4/5/16.
 *
 *
 * RestStatsInterface
 *
 * Rest Interface for Stats information
 */
public interface RestStatsInterface {

    String GAME_MODE_ARENA = "arena";
    String GAME_MODE_CAMPAIGN = "campaign";
    String GAME_MODE_WARZONE = "warzone";
    String GAME_MODE_CUSTOM = "custom";

    @StringDef({GAME_MODE_ARENA, GAME_MODE_CAMPAIGN, GAME_MODE_CUSTOM, GAME_MODE_WARZONE})
    @Retention(RetentionPolicy.SOURCE)
    @interface GameMode {}

    @GET("stats/{title}/matches/{matchId}/events")
    Observable<MatchEvents> getMatchEvents(@Path("title") String title, @Path("matchId") String matchId);

    @GET("stats/{title}/players/{player}/matches")
    Observable<PlayerMatches> getPlayerMatches(@Path("title") String title, @Path("player") String playerName, @Query("modes") String modes, @Query("start") Integer start, @Query("count") Integer count);

    @GET("stats/{title}/player-leaderboards/csr/{seasonId}/{playlistId}")
    Observable<Leaderboards> getLeaderboards(@Path("title") String title, @Path("seasonId") String seasonId, @Path("playlistId") String playlistId, @Query("count") Integer count);

    @GET("stats/{title}/arena/matches/{matchId}")
    Observable<PostGameCarnageReportArena> getPostGameCarnageReportArena(@Path("title") String title, @Path("matchId") String matchId);

    @GET("stats/{title}/campaign/matches/{matchId}")
    Observable<PostGameCarnageReportCampaign> getPostGameCarnageReportCampaign(@Path("title") String title, @Path("matchId") String matchId);

    @GET("stats/{title}/custom/matches/{matchId}")
    Observable<PostGameCarnageReportCustom> getPostGameCarnageReportCustom(@Path("title") String title, @Path("matchId") String matchId);

    @GET("stats/{title}/warzone/matches/{matchId}")
    Observable<PostGameCarnageReportWarzone> getPostGameCarnageReportWarzone(@Path("title") String title, @Path("matchId") String matchId);

    @GET("stats/{title}/servicerecords/arena")
    Observable<ServiceRecordArena> getServiceRecordsArena(@Path("title") String title, @Query("players") String players, @Query("seasonId") String seasonId);

    @GET("stats/{title}/servicerecords/campaign")
    Observable<ServiceRecordCampaign> getServiceRecordsCampaign(@Path("title") String title, @Query("players") String players);

    @GET("stats/{title}/servicerecords/custom")
    Observable<ServiceRecordCustom> getServiceRecordsCustom(@Path("title") String title, @Query("players") String players);

    @GET("stats/{title}/servicerecords/warzone")
    Observable<ServiceRecordWarzone> getServiceRecordsWarzone(@Path("title") String title, @Query("players") String players);

    void showProgress();
}