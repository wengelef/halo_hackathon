
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import example.wengelef.hackathon.rest.model.stats.servicerecordcampaign.CampaignStat;


public class PlayerResult implements Serializable {

    @SerializedName("CampaignStat")
    @Expose
    private CampaignStat CampaignStat;
    @SerializedName("PlayerId")
    @Expose
    private PlayerId PlayerId;
    @SerializedName("SpartanRank")
    @Expose
    private Integer SpartanRank;
    @SerializedName("Xp")
    @Expose
    private Integer Xp;

    /**
     * 
     * @return
     *     The CampaignStat
     */
    public CampaignStat getCampaignStat() {
        return CampaignStat;
    }

    /**
     * 
     * @param CampaignStat
     *     The CampaignStat
     */
    public void setCampaignStat(CampaignStat CampaignStat) {
        this.CampaignStat = CampaignStat;
    }

    /**
     * 
     * @return
     *     The PlayerId
     */
    public PlayerId getPlayerId() {
        return PlayerId;
    }

    /**
     * 
     * @param PlayerId
     *     The PlayerId
     */
    public void setPlayerId(PlayerId PlayerId) {
        this.PlayerId = PlayerId;
    }

    /**
     * 
     * @return
     *     The SpartanRank
     */
    public Integer getSpartanRank() {
        return SpartanRank;
    }

    /**
     * 
     * @param SpartanRank
     *     The SpartanRank
     */
    public void setSpartanRank(Integer SpartanRank) {
        this.SpartanRank = SpartanRank;
    }

    /**
     * 
     * @return
     *     The Xp
     */
    public Integer getXp() {
        return Xp;
    }

    /**
     * 
     * @param Xp
     *     The Xp
     */
    public void setXp(Integer Xp) {
        this.Xp = Xp;
    }

}
