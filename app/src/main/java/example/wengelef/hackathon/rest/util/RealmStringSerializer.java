package example.wengelef.hackathon.rest.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import example.wengelef.hackathon.util.RealmString;
import io.realm.RealmList;

/**
 * Created by fwengelewski on 5/5/16.
 *
 *
 * RealmStringSerializer
 *
 * Needed to serialize RealmStrings to GSON
 */
public class RealmStringSerializer implements JsonSerializer<RealmList<RealmString>>, JsonDeserializer<RealmList<RealmString>> {

    @Override
    public JsonElement serialize(RealmList<RealmString> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (RealmString realmString : src) {
            jsonArray.add(context.serialize(realmString));
        }
        return jsonArray;
    }

    @Override
    public RealmList<RealmString> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RealmList<RealmString> realmStrings = new RealmList<>();

        JsonArray jsonArray = json.getAsJsonArray();

        for (JsonElement element : jsonArray) {
            realmStrings.add(new RealmString(element.getAsString()));
        }
        return realmStrings;
    }
}
