package example.wengelef.hackathon.rest.util;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by fwengelewski on 4/8/16.
 *
 *
 * HeaderInterceptor
 *
 * Intercept HTTP Requests and set Key/Value as Header
 */
public class HeaderInterceptor implements Interceptor {

    private String mKey;
    private String mValue;

    public HeaderInterceptor(@NonNull String key, @NonNull String value) {
        mKey = key;
        mValue = value;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        return chain.proceed(request.newBuilder().addHeader(mKey, mValue).build());
    }
}
