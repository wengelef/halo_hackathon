package example.wengelef.hackathon.rest.util;

import android.support.annotation.Nullable;

import okhttp3.Headers;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/7/16.
 */
public class RedirectHelper {

    @Nullable
    public static String getRedirectUrl(@Nullable Response response) {
        if (response == null) {
            Timber.e("Response must not be null");
            return null;
        }

        Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++) {
            if (responseHeaders.name(i).equals("Location")) {
                return responseHeaders.value(i);
            }
        }
        return null;
    }
}
