package example.wengelef.hackathon.rest.api;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

import example.wengelef.hackathon.event.ProgressEvent;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * DecoratedProfileInterface
 *
 * Decorates the {@link RestProfileInterface}
 */
public class DecoratedProfileInterface implements RestProfileInterface {

    private RestProfileInterface mProfileInterface;

    public DecoratedProfileInterface(@NonNull RestProfileInterface restProfileInterface) {
        mProfileInterface = restProfileInterface;
    }

    @Override
    public Observable<Void> getEmblemImage(@Path("title") String title, @Path("player") String player, @Query("size") @EmblemSize Integer size) {
        showProgress();
        return mProfileInterface.getEmblemImage(title, player, size);
    }

    @Override
    public Observable<Void> getSpartanImage(@Path("title") String title, @Path("player") String player, @Query("size") @EmblemSize Integer size, @Query("crop") @CropMode String cropMode) {
        showProgress();
        return mProfileInterface.getSpartanImage(title, player, size, cropMode);
    }

    @Override
    public void showProgress() {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.SHOW));
    }
}
