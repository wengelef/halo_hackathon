
package example.wengelef.hackathon.rest.model.stats.servicerecordwarzone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import example.wengelef.hackathon.rest.model.stats.DestroyedEnemyVehicle;
import example.wengelef.hackathon.rest.model.stats.EnemyKill;
import example.wengelef.hackathon.rest.model.stats.Impulse;
import example.wengelef.hackathon.rest.model.stats.MedalAward;
import example.wengelef.hackathon.rest.model.stats.WeaponStat;
import example.wengelef.hackathon.rest.model.stats.WeaponWithMostKills;

public class WarzoneStat {

    @SerializedName("TotalPiesEarned")
    @Expose
    private Integer TotalPiesEarned;
    @SerializedName("ScenarioStats")
    @Expose
    private List<ScenarioStat> ScenarioStats = new ArrayList<ScenarioStat>();
    @SerializedName("TotalKills")
    @Expose
    private Integer TotalKills;
    @SerializedName("TotalHeadshots")
    @Expose
    private Integer TotalHeadshots;
    @SerializedName("TotalWeaponDamage")
    @Expose
    private Double TotalWeaponDamage;
    @SerializedName("TotalShotsFired")
    @Expose
    private Integer TotalShotsFired;
    @SerializedName("TotalShotsLanded")
    @Expose
    private Integer TotalShotsLanded;
    @SerializedName("WeaponWithMostKills")
    @Expose
    private WeaponWithMostKills WeaponWithMostKills;
    @SerializedName("TotalMeleeKills")
    @Expose
    private Integer TotalMeleeKills;
    @SerializedName("TotalMeleeDamage")
    @Expose
    private Double TotalMeleeDamage;
    @SerializedName("TotalAssassinations")
    @Expose
    private Integer TotalAssassinations;
    @SerializedName("TotalGroundPoundKills")
    @Expose
    private Integer TotalGroundPoundKills;
    @SerializedName("TotalGroundPoundDamage")
    @Expose
    private Double TotalGroundPoundDamage;
    @SerializedName("TotalShoulderBashKills")
    @Expose
    private Integer TotalShoulderBashKills;
    @SerializedName("TotalShoulderBashDamage")
    @Expose
    private Double TotalShoulderBashDamage;
    @SerializedName("TotalGrenadeDamage")
    @Expose
    private Double TotalGrenadeDamage;
    @SerializedName("TotalPowerWeaponKills")
    @Expose
    private Integer TotalPowerWeaponKills;
    @SerializedName("TotalPowerWeaponDamage")
    @Expose
    private Double TotalPowerWeaponDamage;
    @SerializedName("TotalPowerWeaponGrabs")
    @Expose
    private Integer TotalPowerWeaponGrabs;
    @SerializedName("TotalPowerWeaponPossessionTime")
    @Expose
    private String TotalPowerWeaponPossessionTime;
    @SerializedName("TotalDeaths")
    @Expose
    private Integer TotalDeaths;
    @SerializedName("TotalAssists")
    @Expose
    private Integer TotalAssists;
    @SerializedName("TotalGamesCompleted")
    @Expose
    private Integer TotalGamesCompleted;
    @SerializedName("TotalGamesWon")
    @Expose
    private Integer TotalGamesWon;
    @SerializedName("TotalGamesLost")
    @Expose
    private Integer TotalGamesLost;
    @SerializedName("TotalGamesTied")
    @Expose
    private Integer TotalGamesTied;
    @SerializedName("TotalTimePlayed")
    @Expose
    private String TotalTimePlayed;
    @SerializedName("TotalGrenadeKills")
    @Expose
    private Integer TotalGrenadeKills;
    @SerializedName("MedalAwards")
    @Expose
    private List<MedalAward> MedalAwards = new ArrayList<MedalAward>();
    @SerializedName("DestroyedEnemyVehicles")
    @Expose
    private List<DestroyedEnemyVehicle> DestroyedEnemyVehicles = new ArrayList<DestroyedEnemyVehicle>();
    @SerializedName("EnemyKills")
    @Expose
    private List<EnemyKill> EnemyKills = new ArrayList<EnemyKill>();
    @SerializedName("WeaponStats")
    @Expose
    private List<WeaponStat> WeaponStats = new ArrayList<WeaponStat>();
    @SerializedName("Impulses")
    @Expose
    private List<Impulse> Impulses = new ArrayList<Impulse>();
    @SerializedName("TotalSpartanKills")
    @Expose
    private Integer TotalSpartanKills;

    /**
     * 
     * @return
     *     The TotalPiesEarned
     */
    public Integer getTotalPiesEarned() {
        return TotalPiesEarned;
    }

    /**
     * 
     * @param TotalPiesEarned
     *     The TotalPiesEarned
     */
    public void setTotalPiesEarned(Integer TotalPiesEarned) {
        this.TotalPiesEarned = TotalPiesEarned;
    }

    /**
     * 
     * @return
     *     The ScenarioStats
     */
    public List<ScenarioStat> getScenarioStats() {
        return ScenarioStats;
    }

    /**
     * 
     * @param ScenarioStats
     *     The ScenarioStats
     */
    public void setScenarioStats(List<ScenarioStat> ScenarioStats) {
        this.ScenarioStats = ScenarioStats;
    }

    /**
     * 
     * @return
     *     The TotalKills
     */
    public Integer getTotalKills() {
        return TotalKills;
    }

    /**
     * 
     * @param TotalKills
     *     The TotalKills
     */
    public void setTotalKills(Integer TotalKills) {
        this.TotalKills = TotalKills;
    }

    /**
     * 
     * @return
     *     The TotalHeadshots
     */
    public Integer getTotalHeadshots() {
        return TotalHeadshots;
    }

    /**
     * 
     * @param TotalHeadshots
     *     The TotalHeadshots
     */
    public void setTotalHeadshots(Integer TotalHeadshots) {
        this.TotalHeadshots = TotalHeadshots;
    }

    /**
     * 
     * @return
     *     The TotalWeaponDamage
     */
    public Double getTotalWeaponDamage() {
        return TotalWeaponDamage;
    }

    /**
     * 
     * @param TotalWeaponDamage
     *     The TotalWeaponDamage
     */
    public void setTotalWeaponDamage(Double TotalWeaponDamage) {
        this.TotalWeaponDamage = TotalWeaponDamage;
    }

    /**
     * 
     * @return
     *     The TotalShotsFired
     */
    public Integer getTotalShotsFired() {
        return TotalShotsFired;
    }

    /**
     * 
     * @param TotalShotsFired
     *     The TotalShotsFired
     */
    public void setTotalShotsFired(Integer TotalShotsFired) {
        this.TotalShotsFired = TotalShotsFired;
    }

    /**
     * 
     * @return
     *     The TotalShotsLanded
     */
    public Integer getTotalShotsLanded() {
        return TotalShotsLanded;
    }

    /**
     * 
     * @param TotalShotsLanded
     *     The TotalShotsLanded
     */
    public void setTotalShotsLanded(Integer TotalShotsLanded) {
        this.TotalShotsLanded = TotalShotsLanded;
    }

    /**
     * 
     * @return
     *     The WeaponWithMostKills
     */
    public WeaponWithMostKills getWeaponWithMostKills() {
        return WeaponWithMostKills;
    }

    /**
     * 
     * @param WeaponWithMostKills
     *     The WeaponWithMostKills
     */
    public void setWeaponWithMostKills(WeaponWithMostKills WeaponWithMostKills) {
        this.WeaponWithMostKills = WeaponWithMostKills;
    }

    /**
     * 
     * @return
     *     The TotalMeleeKills
     */
    public Integer getTotalMeleeKills() {
        return TotalMeleeKills;
    }

    /**
     * 
     * @param TotalMeleeKills
     *     The TotalMeleeKills
     */
    public void setTotalMeleeKills(Integer TotalMeleeKills) {
        this.TotalMeleeKills = TotalMeleeKills;
    }

    /**
     * 
     * @return
     *     The TotalMeleeDamage
     */
    public Double getTotalMeleeDamage() {
        return TotalMeleeDamage;
    }

    /**
     * 
     * @param TotalMeleeDamage
     *     The TotalMeleeDamage
     */
    public void setTotalMeleeDamage(Double TotalMeleeDamage) {
        this.TotalMeleeDamage = TotalMeleeDamage;
    }

    /**
     * 
     * @return
     *     The TotalAssassinations
     */
    public Integer getTotalAssassinations() {
        return TotalAssassinations;
    }

    /**
     * 
     * @param TotalAssassinations
     *     The TotalAssassinations
     */
    public void setTotalAssassinations(Integer TotalAssassinations) {
        this.TotalAssassinations = TotalAssassinations;
    }

    /**
     * 
     * @return
     *     The TotalGroundPoundKills
     */
    public Integer getTotalGroundPoundKills() {
        return TotalGroundPoundKills;
    }

    /**
     * 
     * @param TotalGroundPoundKills
     *     The TotalGroundPoundKills
     */
    public void setTotalGroundPoundKills(Integer TotalGroundPoundKills) {
        this.TotalGroundPoundKills = TotalGroundPoundKills;
    }

    /**
     * 
     * @return
     *     The TotalGroundPoundDamage
     */
    public Double getTotalGroundPoundDamage() {
        return TotalGroundPoundDamage;
    }

    /**
     * 
     * @param TotalGroundPoundDamage
     *     The TotalGroundPoundDamage
     */
    public void setTotalGroundPoundDamage(Double TotalGroundPoundDamage) {
        this.TotalGroundPoundDamage = TotalGroundPoundDamage;
    }

    /**
     * 
     * @return
     *     The TotalShoulderBashKills
     */
    public Integer getTotalShoulderBashKills() {
        return TotalShoulderBashKills;
    }

    /**
     * 
     * @param TotalShoulderBashKills
     *     The TotalShoulderBashKills
     */
    public void setTotalShoulderBashKills(Integer TotalShoulderBashKills) {
        this.TotalShoulderBashKills = TotalShoulderBashKills;
    }

    /**
     * 
     * @return
     *     The TotalShoulderBashDamage
     */
    public Double getTotalShoulderBashDamage() {
        return TotalShoulderBashDamage;
    }

    /**
     * 
     * @param TotalShoulderBashDamage
     *     The TotalShoulderBashDamage
     */
    public void setTotalShoulderBashDamage(Double TotalShoulderBashDamage) {
        this.TotalShoulderBashDamage = TotalShoulderBashDamage;
    }

    /**
     * 
     * @return
     *     The TotalGrenadeDamage
     */
    public Double getTotalGrenadeDamage() {
        return TotalGrenadeDamage;
    }

    /**
     * 
     * @param TotalGrenadeDamage
     *     The TotalGrenadeDamage
     */
    public void setTotalGrenadeDamage(Double TotalGrenadeDamage) {
        this.TotalGrenadeDamage = TotalGrenadeDamage;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponKills
     */
    public Integer getTotalPowerWeaponKills() {
        return TotalPowerWeaponKills;
    }

    /**
     * 
     * @param TotalPowerWeaponKills
     *     The TotalPowerWeaponKills
     */
    public void setTotalPowerWeaponKills(Integer TotalPowerWeaponKills) {
        this.TotalPowerWeaponKills = TotalPowerWeaponKills;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponDamage
     */
    public Double getTotalPowerWeaponDamage() {
        return TotalPowerWeaponDamage;
    }

    /**
     * 
     * @param TotalPowerWeaponDamage
     *     The TotalPowerWeaponDamage
     */
    public void setTotalPowerWeaponDamage(Double TotalPowerWeaponDamage) {
        this.TotalPowerWeaponDamage = TotalPowerWeaponDamage;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponGrabs
     */
    public Integer getTotalPowerWeaponGrabs() {
        return TotalPowerWeaponGrabs;
    }

    /**
     * 
     * @param TotalPowerWeaponGrabs
     *     The TotalPowerWeaponGrabs
     */
    public void setTotalPowerWeaponGrabs(Integer TotalPowerWeaponGrabs) {
        this.TotalPowerWeaponGrabs = TotalPowerWeaponGrabs;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponPossessionTime
     */
    public String getTotalPowerWeaponPossessionTime() {
        return TotalPowerWeaponPossessionTime;
    }

    /**
     * 
     * @param TotalPowerWeaponPossessionTime
     *     The TotalPowerWeaponPossessionTime
     */
    public void setTotalPowerWeaponPossessionTime(String TotalPowerWeaponPossessionTime) {
        this.TotalPowerWeaponPossessionTime = TotalPowerWeaponPossessionTime;
    }

    /**
     * 
     * @return
     *     The TotalDeaths
     */
    public Integer getTotalDeaths() {
        return TotalDeaths;
    }

    /**
     * 
     * @param TotalDeaths
     *     The TotalDeaths
     */
    public void setTotalDeaths(Integer TotalDeaths) {
        this.TotalDeaths = TotalDeaths;
    }

    /**
     * 
     * @return
     *     The TotalAssists
     */
    public Integer getTotalAssists() {
        return TotalAssists;
    }

    /**
     * 
     * @param TotalAssists
     *     The TotalAssists
     */
    public void setTotalAssists(Integer TotalAssists) {
        this.TotalAssists = TotalAssists;
    }

    /**
     * 
     * @return
     *     The TotalGamesCompleted
     */
    public Integer getTotalGamesCompleted() {
        return TotalGamesCompleted;
    }

    /**
     * 
     * @param TotalGamesCompleted
     *     The TotalGamesCompleted
     */
    public void setTotalGamesCompleted(Integer TotalGamesCompleted) {
        this.TotalGamesCompleted = TotalGamesCompleted;
    }

    /**
     * 
     * @return
     *     The TotalGamesWon
     */
    public Integer getTotalGamesWon() {
        return TotalGamesWon;
    }

    /**
     * 
     * @param TotalGamesWon
     *     The TotalGamesWon
     */
    public void setTotalGamesWon(Integer TotalGamesWon) {
        this.TotalGamesWon = TotalGamesWon;
    }

    /**
     * 
     * @return
     *     The TotalGamesLost
     */
    public Integer getTotalGamesLost() {
        return TotalGamesLost;
    }

    /**
     * 
     * @param TotalGamesLost
     *     The TotalGamesLost
     */
    public void setTotalGamesLost(Integer TotalGamesLost) {
        this.TotalGamesLost = TotalGamesLost;
    }

    /**
     * 
     * @return
     *     The TotalGamesTied
     */
    public Integer getTotalGamesTied() {
        return TotalGamesTied;
    }

    /**
     * 
     * @param TotalGamesTied
     *     The TotalGamesTied
     */
    public void setTotalGamesTied(Integer TotalGamesTied) {
        this.TotalGamesTied = TotalGamesTied;
    }

    /**
     * 
     * @return
     *     The TotalTimePlayed
     */
    public String getTotalTimePlayed() {
        return TotalTimePlayed;
    }

    /**
     * 
     * @param TotalTimePlayed
     *     The TotalTimePlayed
     */
    public void setTotalTimePlayed(String TotalTimePlayed) {
        this.TotalTimePlayed = TotalTimePlayed;
    }

    /**
     * 
     * @return
     *     The TotalGrenadeKills
     */
    public Integer getTotalGrenadeKills() {
        return TotalGrenadeKills;
    }

    /**
     * 
     * @param TotalGrenadeKills
     *     The TotalGrenadeKills
     */
    public void setTotalGrenadeKills(Integer TotalGrenadeKills) {
        this.TotalGrenadeKills = TotalGrenadeKills;
    }

    /**
     * 
     * @return
     *     The MedalAwards
     */
    public List<MedalAward> getMedalAwards() {
        return MedalAwards;
    }

    /**
     * 
     * @param MedalAwards
     *     The MedalAwards
     */
    public void setMedalAwards(List<MedalAward> MedalAwards) {
        this.MedalAwards = MedalAwards;
    }

    /**
     * 
     * @return
     *     The DestroyedEnemyVehicles
     */
    public List<DestroyedEnemyVehicle> getDestroyedEnemyVehicles() {
        return DestroyedEnemyVehicles;
    }

    /**
     * 
     * @param DestroyedEnemyVehicles
     *     The DestroyedEnemyVehicles
     */
    public void setDestroyedEnemyVehicles(List<DestroyedEnemyVehicle> DestroyedEnemyVehicles) {
        this.DestroyedEnemyVehicles = DestroyedEnemyVehicles;
    }

    /**
     * 
     * @return
     *     The EnemyKills
     */
    public List<EnemyKill> getEnemyKills() {
        return EnemyKills;
    }

    /**
     * 
     * @param EnemyKills
     *     The EnemyKills
     */
    public void setEnemyKills(List<EnemyKill> EnemyKills) {
        this.EnemyKills = EnemyKills;
    }

    /**
     * 
     * @return
     *     The WeaponStats
     */
    public List<WeaponStat> getWeaponStats() {
        return WeaponStats;
    }

    /**
     * 
     * @param WeaponStats
     *     The WeaponStats
     */
    public void setWeaponStats(List<WeaponStat> WeaponStats) {
        this.WeaponStats = WeaponStats;
    }

    /**
     * 
     * @return
     *     The Impulses
     */
    public List<Impulse> getImpulses() {
        return Impulses;
    }

    /**
     * 
     * @param Impulses
     *     The Impulses
     */
    public void setImpulses(List<Impulse> Impulses) {
        this.Impulses = Impulses;
    }

    /**
     * 
     * @return
     *     The TotalSpartanKills
     */
    public Integer getTotalSpartanKills() {
        return TotalSpartanKills;
    }

    /**
     * 
     * @param TotalSpartanKills
     *     The TotalSpartanKills
     */
    public void setTotalSpartanKills(Integer TotalSpartanKills) {
        this.TotalSpartanKills = TotalSpartanKills;
    }

}
