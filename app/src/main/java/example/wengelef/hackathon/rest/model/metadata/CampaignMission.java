package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by fwengelewski on 4/5/16.
 *
 *
 * CampaignMission
 *
 * Campaign Mission Model
 */
public class CampaignMission extends RealmObject {

    @PrimaryKey
    @SerializedName("missionNumber")
    @Expose
    private String missionNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     *
     * @return
     * The missionNumber
     */
    public String getMissionNumber() {
        return missionNumber;
    }

    /**
     *
     * @param missionNumber
     * The missionNumber
     */
    public void setMissionNumber(String missionNumber) {
        this.missionNumber = missionNumber;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     *
     * @param contentId
     * The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    /*private static class CampaignMission {

    }*/
}