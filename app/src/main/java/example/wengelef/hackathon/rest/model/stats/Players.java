
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Players implements Serializable {

    @SerializedName("Player")
    @Expose
    private PlayerId Player;
    @SerializedName("TeamId")
    @Expose
    private Integer TeamId;
    @SerializedName("Rank")
    @Expose
    private Integer Rank;
    @SerializedName("Result")
    @Expose
    private Integer Result;
    @SerializedName("TotalKills")
    @Expose
    private Integer TotalKills;
    @SerializedName("TotalDeaths")
    @Expose
    private Integer TotalDeaths;
    @SerializedName("TotalAssists")
    @Expose
    private Integer TotalAssists;
    @SerializedName("PreMatchRatings")
    @Expose
    private Object PreMatchRatings;
    @SerializedName("PostMatchRatings")
    @Expose
    private Object PostMatchRatings;

    /**
     * 
     * @return
     *     The Player
     */
    public PlayerId getPlayer() {
        return Player;
    }

    /**
     * 
     * @param Player
     *     The Player
     */
    public void setPlayer(PlayerId Player) {
        this.Player = Player;
    }

    /**
     * 
     * @return
     *     The TeamId
     */
    public Integer getTeamId() {
        return TeamId;
    }

    /**
     * 
     * @param TeamId
     *     The TeamId
     */
    public void setTeamId(Integer TeamId) {
        this.TeamId = TeamId;
    }

    /**
     * 
     * @return
     *     The Rank
     */
    public Integer getRank() {
        return Rank;
    }

    /**
     * 
     * @param Rank
     *     The Rank
     */
    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

    /**
     * 
     * @return
     *     The Result
     */
    public Integer getResult() {
        return Result;
    }

    /**
     * 
     * @param Result
     *     The Result
     */
    public void setResult(Integer Result) {
        this.Result = Result;
    }

    /**
     * 
     * @return
     *     The TotalKills
     */
    public Integer getTotalKills() {
        return TotalKills;
    }

    /**
     * 
     * @param TotalKills
     *     The TotalKills
     */
    public void setTotalKills(Integer TotalKills) {
        this.TotalKills = TotalKills;
    }

    /**
     * 
     * @return
     *     The TotalDeaths
     */
    public Integer getTotalDeaths() {
        return TotalDeaths;
    }

    /**
     * 
     * @param TotalDeaths
     *     The TotalDeaths
     */
    public void setTotalDeaths(Integer TotalDeaths) {
        this.TotalDeaths = TotalDeaths;
    }

    /**
     * 
     * @return
     *     The TotalAssists
     */
    public Integer getTotalAssists() {
        return TotalAssists;
    }

    /**
     * 
     * @param TotalAssists
     *     The TotalAssists
     */
    public void setTotalAssists(Integer TotalAssists) {
        this.TotalAssists = TotalAssists;
    }

    /**
     * 
     * @return
     *     The PreMatchRatings
     */
    public Object getPreMatchRatings() {
        return PreMatchRatings;
    }

    /**
     * 
     * @param PreMatchRatings
     *     The PreMatchRatings
     */
    public void setPreMatchRatings(Object PreMatchRatings) {
        this.PreMatchRatings = PreMatchRatings;
    }

    /**
     * 
     * @return
     *     The PostMatchRatings
     */
    public Object getPostMatchRatings() {
        return PostMatchRatings;
    }

    /**
     * 
     * @param PostMatchRatings
     *     The PostMatchRatings
     */
    public void setPostMatchRatings(Object PostMatchRatings) {
        this.PostMatchRatings = PostMatchRatings;
    }

}
