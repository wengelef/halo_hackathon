
package example.wengelef.hackathon.rest.model.stats.servicerecordarena;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import example.wengelef.hackathon.rest.model.stats.PlayerId;

public class Result {

    @SerializedName("ArenaStats")
    @Expose
    private ArenaStats ArenaStats;
    @SerializedName("PlayerId")
    @Expose
    private PlayerId PlayerId;
    @SerializedName("SpartanRank")
    @Expose
    private Long SpartanRank;
    @SerializedName("Xp")
    @Expose
    private Long Xp;

    /**
     * 
     * @return
     *     The ArenaStats
     */
    public ArenaStats getArenaStats() {
        return ArenaStats;
    }

    /**
     * 
     * @param ArenaStats
     *     The ArenaStats
     */
    public void setArenaStats(ArenaStats ArenaStats) {
        this.ArenaStats = ArenaStats;
    }

    /**
     * 
     * @return
     *     The PlayerId
     */
    public PlayerId getPlayerId() {
        return PlayerId;
    }

    /**
     * 
     * @param PlayerId
     *     The PlayerId
     */
    public void setPlayerId(PlayerId PlayerId) {
        this.PlayerId = PlayerId;
    }

    /**
     * 
     * @return
     *     The SpartanRank
     */
    public Long getSpartanRank() {
        return SpartanRank;
    }

    /**
     * 
     * @param SpartanRank
     *     The SpartanRank
     */
    public void setSpartanRank(Long SpartanRank) {
        this.SpartanRank = SpartanRank;
    }

    /**
     * 
     * @return
     *     The Xp
     */
    public Long getXp() {
        return Xp;
    }

    /**
     * 
     * @param Xp
     *     The Xp
     */
    public void setXp(Long Xp) {
        this.Xp = Xp;
    }

}
