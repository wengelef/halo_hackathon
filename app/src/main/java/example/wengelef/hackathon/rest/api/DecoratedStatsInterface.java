package example.wengelef.hackathon.rest.api;

import org.greenrobot.eventbus.EventBus;

import example.wengelef.hackathon.event.ProgressEvent;
import example.wengelef.hackathon.rest.model.stats.Leaderboards;
import example.wengelef.hackathon.rest.model.stats.MatchEvents;
import example.wengelef.hackathon.rest.model.stats.PlayerMatches;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportArena;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCampaign;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCustom;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportWarzone;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ServiceRecordArena;
import example.wengelef.hackathon.rest.model.stats.servicerecordcampaign.ServiceRecordCampaign;
import example.wengelef.hackathon.rest.model.stats.servicerecordcustom.ServiceRecordCustom;
import example.wengelef.hackathon.rest.model.stats.servicerecordwarzone.ServiceRecordWarzone;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * DecoratedStatsInterface
 *
 * Decorates the {@link RestStatsInterface}
 */
public class DecoratedStatsInterface implements RestStatsInterface {

    private RestStatsInterface mStatsInterface;

    public DecoratedStatsInterface(RestStatsInterface statsInterface) {
        this.mStatsInterface = statsInterface;
    }

    @Override
    public Observable<MatchEvents> getMatchEvents(@Path("title") String title, @Path("matchId") String matchId) {
        showProgress();
        return mStatsInterface.getMatchEvents(title, matchId);
    }

    @Override
    public Observable<PlayerMatches> getPlayerMatches(@Path("title") String title, @Path("player") String playerName, @Query("modes") String modes, @Query("start") Integer start, @Query("count") Integer count) {
        showProgress();
        return mStatsInterface.getPlayerMatches(title, playerName, modes, start, count);
    }

    @Override
    public Observable<Leaderboards> getLeaderboards(@Path("title") String title, @Path("seasonId") String seasonId, @Path("playlistId") String playlistId, @Query("count") Integer count) {
        showProgress();
        return mStatsInterface.getLeaderboards(title, seasonId, playlistId, count);
    }

    @Override
    public Observable<PostGameCarnageReportArena> getPostGameCarnageReportArena(@Path("title") String title, @Path("matchId") String matchId) {
        showProgress();
        return mStatsInterface.getPostGameCarnageReportArena(title, matchId);
    }

    @Override
    public Observable<PostGameCarnageReportCampaign> getPostGameCarnageReportCampaign(@Path("title") String title, @Path("matchId") String matchId) {
        showProgress();
        return mStatsInterface.getPostGameCarnageReportCampaign(title, matchId);
    }

    @Override
    public Observable<PostGameCarnageReportCustom> getPostGameCarnageReportCustom(@Path("title") String title, @Path("matchId") String matchId) {
        showProgress();
        return mStatsInterface.getPostGameCarnageReportCustom(title, matchId);
    }

    @Override
    public Observable<PostGameCarnageReportWarzone> getPostGameCarnageReportWarzone(@Path("title") String title, @Path("matchId") String matchId) {
        showProgress();
        return mStatsInterface.getPostGameCarnageReportWarzone(title, matchId);
    }

    @Override
    public Observable<ServiceRecordArena> getServiceRecordsArena(@Path("title") String title, @Query("players") String players, @Query("seasonId") String seasonId) {
        showProgress();
        return mStatsInterface.getServiceRecordsArena(title, players, seasonId);
    }

    @Override
    public Observable<ServiceRecordCampaign> getServiceRecordsCampaign(@Path("title") String title, @Query("players") String players) {
        showProgress();
        return mStatsInterface.getServiceRecordsCampaign(title, players);
    }

    @Override
    public Observable<ServiceRecordCustom> getServiceRecordsCustom(@Path("title") String title, @Query("players") String players) {
        showProgress();
        return mStatsInterface.getServiceRecordsCustom(title, players);
    }

    @Override
    public Observable<ServiceRecordWarzone> getServiceRecordsWarzone(@Path("title") String title, @Query("players") String players) {
        showProgress();
        return mStatsInterface.getServiceRecordsWarzone(title, players);
    }

    @Override
    public void showProgress() {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.SHOW));
    }
}
