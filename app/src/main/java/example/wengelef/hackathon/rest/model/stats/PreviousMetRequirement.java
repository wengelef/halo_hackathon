
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreviousMetRequirement {

    @SerializedName("Data1")
    @Expose
    private Integer Data1;
    @SerializedName("Data2")
    @Expose
    private Integer Data2;
    @SerializedName("Data3")
    @Expose
    private Integer Data3;
    @SerializedName("Data4")
    @Expose
    private Integer Data4;

    /**
     * 
     * @return
     *     The Data1
     */
    public Integer getData1() {
        return Data1;
    }

    /**
     * 
     * @param Data1
     *     The Data1
     */
    public void setData1(Integer Data1) {
        this.Data1 = Data1;
    }

    /**
     * 
     * @return
     *     The Data2
     */
    public Integer getData2() {
        return Data2;
    }

    /**
     * 
     * @param Data2
     *     The Data2
     */
    public void setData2(Integer Data2) {
        this.Data2 = Data2;
    }

    /**
     * 
     * @return
     *     The Data3
     */
    public Integer getData3() {
        return Data3;
    }

    /**
     * 
     * @param Data3
     *     The Data3
     */
    public void setData3(Integer Data3) {
        this.Data3 = Data3;
    }

    /**
     * 
     * @return
     *     The Data4
     */
    public Integer getData4() {
        return Data4;
    }

    /**
     * 
     * @param Data4
     *     The Data4
     */
    public void setData4(Integer Data4) {
        this.Data4 = Data4;
    }

}
