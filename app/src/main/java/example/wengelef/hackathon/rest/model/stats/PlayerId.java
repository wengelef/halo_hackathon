
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PlayerId extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("Gamertag")
    @Expose
    private String GamerTag;

    /**
     * 
     * @return
     *     The GamerTag
     */
    public String getGamerTag() {
        return GamerTag;
    }

    /**
     * 
     * @param GamerTag
     *     The GamerTag
     */
    public void setGamerTag(String GamerTag) {
        this.GamerTag = GamerTag;
    }

    public PlayerId(String gamerTag) {
        GamerTag = gamerTag;
    }

    public PlayerId() {

    }
}
