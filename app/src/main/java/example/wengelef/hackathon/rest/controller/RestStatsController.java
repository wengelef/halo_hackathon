package example.wengelef.hackathon.rest.controller;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import example.wengelef.hackathon.rest.RestModule;
import example.wengelef.hackathon.rest.api.RestStatsInterface;
import example.wengelef.hackathon.rest.model.stats.Leaderboards;
import example.wengelef.hackathon.rest.model.stats.MatchEvents;
import example.wengelef.hackathon.rest.model.stats.PlayerMatches;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportArena;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCampaign;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCustom;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportWarzone;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ServiceRecordArena;
import example.wengelef.hackathon.rest.model.stats.servicerecordcampaign.ServiceRecordCampaign;
import example.wengelef.hackathon.rest.model.stats.servicerecordcustom.ServiceRecordCustom;
import example.wengelef.hackathon.rest.model.stats.servicerecordwarzone.ServiceRecordWarzone;
import rx.Observable;

/**
 * Created by fwengelewski on 4/7/16.
 *
 *
 * RestStatsController
 *
 * RestController for Stats
 */
public class RestStatsController extends RestController {

    private final @NonNull RestStatsInterface mStatsInterface;

    @Inject
    public RestStatsController(@NonNull RestStatsInterface statsInterface) {
        this.mStatsInterface = statsInterface;
    }

    public Observable<MatchEvents> getMatchEvents(String matchId) {
        return mStatsInterface.getMatchEvents(RestModule.getTitle(), matchId);
    }

    public Observable<PlayerMatches> getPlayerMatches(String playerName, @Nullable List<String> modes, @Nullable Integer start, @Nullable @IntRange(from=0, to=25) Integer count) {
        return mStatsInterface.getPlayerMatches(RestModule.getTitle(), playerName, convertStringList(modes), start, count);
    }

    public Observable<Leaderboards> getLeaderboards(String seasonId, String playlistId, @Nullable @IntRange(from=1, to=250) Integer count) {
        return mStatsInterface.getLeaderboards(RestModule.getTitle(), seasonId, playlistId, count);
    }

    public Observable<PostGameCarnageReportArena> getPostGameReportArena(String matchId) {
        return mStatsInterface.getPostGameCarnageReportArena(RestModule.getTitle(), matchId);
    }

    public Observable<PostGameCarnageReportCampaign> getPostGameReportCampaign(String matchId) {
        return mStatsInterface.getPostGameCarnageReportCampaign(RestModule.getTitle(), matchId);
    }

    public Observable<PostGameCarnageReportWarzone> getPostGameReportWarzone(String matchId) {
        return mStatsInterface.getPostGameCarnageReportWarzone(RestModule.getTitle(), matchId);
    }

    public Observable<PostGameCarnageReportCustom> getPostGameReportCustom(String matchId) {
        return mStatsInterface.getPostGameCarnageReportCustom(RestModule.getTitle(), matchId);
    }

    public Observable<ServiceRecordArena> getServiceRecordsArena(@Nullable List<String> players, @Nullable String seasonId) {
        return mStatsInterface.getServiceRecordsArena(RestModule.getTitle(), convertStringList(players), seasonId);
    }

    public Observable<ServiceRecordArena> getServiceRecordsArena(@Nullable String player, @Nullable String seasonId) {
        return mStatsInterface.getServiceRecordsArena(RestModule.getTitle(), player, seasonId);
    }

    public Observable<ServiceRecordCampaign> getServiceRecordsCampaign(@Nullable List<String> players) {
        return mStatsInterface.getServiceRecordsCampaign(RestModule.getTitle(), convertStringList(players));
    }

    public Observable<ServiceRecordCustom> getServiceRecordsCustom(@Nullable List<String> players) {
        return mStatsInterface.getServiceRecordsCustom(RestModule.getTitle(), convertStringList(players));
    }

    public Observable<ServiceRecordWarzone> getServiceRecordsWarzone(@Nullable List<String> players) {
        return mStatsInterface.getServiceRecordsWarzone(RestModule.getTitle(), convertStringList(players));
    }

    @Nullable
    private String convertStringList(List<String> stringList) {
        if (stringList == null) {
            return null;
        }
        return Arrays.toString(stringList.toArray()).replaceAll("\\s|\\[|\\]", "");
    }
}
