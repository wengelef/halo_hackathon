
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DestroyedEnemyVehicle {

    @SerializedName("Enemy")
    @Expose
    private Enemy Enemy;
    @SerializedName("TotalKills")
    @Expose
    private Integer TotalKills;

    /**
     * 
     * @return
     *     The Enemy
     */
    public Enemy getEnemy() {
        return Enemy;
    }

    /**
     * 
     * @param Enemy
     *     The Enemy
     */
    public void setEnemy(Enemy Enemy) {
        this.Enemy = Enemy;
    }

    /**
     * 
     * @return
     *     The TotalKills
     */
    public Integer getTotalKills() {
        return TotalKills;
    }

    /**
     * 
     * @param TotalKills
     *     The TotalKills
     */
    public void setTotalKills(Integer TotalKills) {
        this.TotalKills = TotalKills;
    }

}
