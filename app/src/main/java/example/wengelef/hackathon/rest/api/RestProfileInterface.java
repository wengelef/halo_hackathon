package example.wengelef.hackathon.rest.api;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by fwengelewski on 4/5/16.
 *
 *
 * RestProfileInterface
 *
 * Rest Interface for Profile Information
 */
public interface RestProfileInterface {

    int EMBLEM_SIZE_95 = 95;
    int EMBLEM_SIZE_128 = 128;
    int EMBLEM_SIZE_190 = 190;
    int EMBLEM_SIZE_256 = 256;
    int EMBLEM_SIZE_512 = 512;

    String CROP_MODE_FULL = "full";
    String CROP_MODE_PORTRAIT = "portrait";

    @IntDef({EMBLEM_SIZE_95, EMBLEM_SIZE_128, EMBLEM_SIZE_190, EMBLEM_SIZE_256, EMBLEM_SIZE_512})
    @Retention(RetentionPolicy.SOURCE)
    public @interface EmblemSize {}

    @StringDef({CROP_MODE_FULL, CROP_MODE_PORTRAIT})
    @Retention(RetentionPolicy.SOURCE)
    @interface CropMode {}

    @GET("profile/{title}/profiles/{player}/emblem")
    Observable<Void> getEmblemImage(@Path("title") String title, @Path("player") String player, @Query("size") @EmblemSize Integer size);

    @GET("profile/{title}/profiles/{player}/spartan")
    Observable<Void> getSpartanImage(@Path("title") String title, @Path("player") String player, @Query("size") @EmblemSize Integer size, @Query("crop") @CropMode String cropMode);

    void showProgress();
}
