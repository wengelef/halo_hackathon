
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Team implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("Score")
    @Expose
    private Integer Score;
    @SerializedName("Rank")
    @Expose
    private Integer Rank;

    /**
     * 
     * @return
     *     The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The Score
     */
    public Integer getScore() {
        return Score;
    }

    /**
     * 
     * @param Score
     *     The Score
     */
    public void setScore(Integer Score) {
        this.Score = Score;
    }

    /**
     * 
     * @return
     *     The Rank
     */
    public Integer getRank() {
        return Rank;
    }

    /**
     * 
     * @param Rank
     *     The Rank
     */
    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

}
