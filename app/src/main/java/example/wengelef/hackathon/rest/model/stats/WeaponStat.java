
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeaponStat {

    @SerializedName("WeaponId")
    @Expose
    private WeaponId WeaponId;
    @SerializedName("TotalShotsFired")
    @Expose
    private Integer TotalShotsFired;
    @SerializedName("TotalShotsLanded")
    @Expose
    private Integer TotalShotsLanded;
    @SerializedName("TotalHeadshots")
    @Expose
    private Integer TotalHeadshots;
    @SerializedName("TotalKills")
    @Expose
    private Integer TotalKills;
    @SerializedName("TotalDamageDealt")
    @Expose
    private Double TotalDamageDealt;
    @SerializedName("TotalPossessionTime")
    @Expose
    private String TotalPossessionTime;

    /**
     * 
     * @return
     *     The WeaponId
     */
    public WeaponId getWeaponId() {
        return WeaponId;
    }

    /**
     * 
     * @param WeaponId
     *     The WeaponId
     */
    public void setWeaponId(WeaponId WeaponId) {
        this.WeaponId = WeaponId;
    }

    /**
     * 
     * @return
     *     The TotalShotsFired
     */
    public Integer getTotalShotsFired() {
        return TotalShotsFired;
    }

    /**
     * 
     * @param TotalShotsFired
     *     The TotalShotsFired
     */
    public void setTotalShotsFired(Integer TotalShotsFired) {
        this.TotalShotsFired = TotalShotsFired;
    }

    /**
     * 
     * @return
     *     The TotalShotsLanded
     */
    public Integer getTotalShotsLanded() {
        return TotalShotsLanded;
    }

    /**
     * 
     * @param TotalShotsLanded
     *     The TotalShotsLanded
     */
    public void setTotalShotsLanded(Integer TotalShotsLanded) {
        this.TotalShotsLanded = TotalShotsLanded;
    }

    /**
     * 
     * @return
     *     The TotalHeadshots
     */
    public Integer getTotalHeadshots() {
        return TotalHeadshots;
    }

    /**
     * 
     * @param TotalHeadshots
     *     The TotalHeadshots
     */
    public void setTotalHeadshots(Integer TotalHeadshots) {
        this.TotalHeadshots = TotalHeadshots;
    }

    /**
     * 
     * @return
     *     The TotalKills
     */
    public Integer getTotalKills() {
        return TotalKills;
    }

    /**
     * 
     * @param TotalKills
     *     The TotalKills
     */
    public void setTotalKills(Integer TotalKills) {
        this.TotalKills = TotalKills;
    }

    /**
     * 
     * @return
     *     The TotalDamageDealt
     */
    public Double getTotalDamageDealt() {
        return TotalDamageDealt;
    }

    /**
     * 
     * @param TotalDamageDealt
     *     The TotalDamageDealt
     */
    public void setTotalDamageDealt(Double TotalDamageDealt) {
        this.TotalDamageDealt = TotalDamageDealt;
    }

    /**
     * 
     * @return
     *     The TotalPossessionTime
     */
    public String getTotalPossessionTime() {
        return TotalPossessionTime;
    }

    /**
     * 
     * @param TotalPossessionTime
     *     The TotalPossessionTime
     */
    public void setTotalPossessionTime(String TotalPossessionTime) {
        this.TotalPossessionTime = TotalPossessionTime;
    }

}
