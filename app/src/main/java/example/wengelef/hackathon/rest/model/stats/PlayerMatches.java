
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlayerMatches {

    @SerializedName("Start")
    @Expose
    private Integer Start;
    @SerializedName("Count")
    @Expose
    private Integer Count;
    @SerializedName("ResultCount")
    @Expose
    private Integer ResultCount;
    @SerializedName("Results")
    @Expose
    private List<ServiceRecordResult> Results = new ArrayList<ServiceRecordResult>();

    /**
     * 
     * @return
     *     The Start
     */
    public Integer getStart() {
        return Start;
    }

    /**
     * 
     * @param Start
     *     The Start
     */
    public void setStart(Integer Start) {
        this.Start = Start;
    }

    /**
     * 
     * @return
     *     The Count
     */
    public Integer getCount() {
        return Count;
    }

    /**
     * 
     * @param Count
     *     The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

    /**
     * 
     * @return
     *     The ResultCount
     */
    public Integer getResultCount() {
        return ResultCount;
    }

    /**
     * 
     * @param ResultCount
     *     The ResultCount
     */
    public void setResultCount(Integer ResultCount) {
        this.ResultCount = ResultCount;
    }

    /**
     * 
     * @return
     *     The Results
     */
    public List<ServiceRecordResult> getResults() {
        return Results;
    }

    /**
     * 
     * @param Results
     *     The Results
     */
    public void setResults(List<ServiceRecordResult> Results) {
        this.Results = Results;
    }

}
