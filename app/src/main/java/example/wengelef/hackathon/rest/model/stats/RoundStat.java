
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoundStat {

    @SerializedName("RoundNumber")
    @Expose
    private Integer RoundNumber;
    @SerializedName("Rank")
    @Expose
    private Integer Rank;
    @SerializedName("Score")
    @Expose
    private Integer Score;

    /**
     * 
     * @return
     *     The RoundNumber
     */
    public Integer getRoundNumber() {
        return RoundNumber;
    }

    /**
     * 
     * @param RoundNumber
     *     The RoundNumber
     */
    public void setRoundNumber(Integer RoundNumber) {
        this.RoundNumber = RoundNumber;
    }

    /**
     * 
     * @return
     *     The Rank
     */
    public Integer getRank() {
        return Rank;
    }

    /**
     * 
     * @param Rank
     *     The Rank
     */
    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

    /**
     * 
     * @return
     *     The Score
     */
    public Integer getScore() {
        return Score;
    }

    /**
     * 
     * @param Score
     *     The Score
     */
    public void setScore(Integer Score) {
        this.Score = Score;
    }

}
