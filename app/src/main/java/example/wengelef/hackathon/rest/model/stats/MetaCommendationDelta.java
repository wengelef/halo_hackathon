
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MetaCommendationDelta {

    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("PreviousMetRequirements")
    @Expose
    private List<PreviousMetRequirement> PreviousMetRequirements = new ArrayList<PreviousMetRequirement>();
    @SerializedName("MetRequirements")
    @Expose
    private List<MetRequirement> MetRequirements = new ArrayList<MetRequirement>();

    /**
     * 
     * @return
     *     The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The PreviousMetRequirements
     */
    public List<PreviousMetRequirement> getPreviousMetRequirements() {
        return PreviousMetRequirements;
    }

    /**
     * 
     * @param PreviousMetRequirements
     *     The PreviousMetRequirements
     */
    public void setPreviousMetRequirements(List<PreviousMetRequirement> PreviousMetRequirements) {
        this.PreviousMetRequirements = PreviousMetRequirements;
    }

    /**
     * 
     * @return
     *     The MetRequirements
     */
    public List<MetRequirement> getMetRequirements() {
        return MetRequirements;
    }

    /**
     * 
     * @param MetRequirements
     *     The MetRequirements
     */
    public void setMetRequirements(List<MetRequirement> MetRequirements) {
        this.MetRequirements = MetRequirements;
    }

}
