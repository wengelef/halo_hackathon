
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Id implements Serializable {

    @SerializedName("MatchId")
    @Expose
    private String MatchId;
    @SerializedName("GameMode")
    @Expose
    private Integer GameMode;

    /**
     * 
     * @return
     *     The MatchId
     */
    public String getMatchId() {
        return MatchId;
    }

    /**
     * 
     * @param MatchId
     *     The MatchId
     */
    public void setMatchId(String MatchId) {
        this.MatchId = MatchId;
    }

    /**
     * 
     * @return
     *     The GameMode
     */
    public Integer getGameMode() {
        return GameMode;
    }

    /**
     * 
     * @param GameMode
     *     The GameMode
     */
    public void setGameMode(Integer GameMode) {
        this.GameMode = GameMode;
    }

}
