
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Enemy {

    @SerializedName("faction")
    @Expose
    private String faction;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("lageIconImageUrl")
    @Expose
    private String lageIconImageUrl;
    @SerializedName("smallIconImageUrl")
    @Expose
    private String smallIconImageUrl;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The faction
     */
    public String getFaction() {
        return faction;
    }

    /**
     * 
     * @param faction
     *     The faction
     */
    public void setFaction(String faction) {
        this.faction = faction;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The lageIconImageUrl
     */
    public String getLageIconImageUrl() {
        return lageIconImageUrl;
    }

    /**
     * 
     * @param lageIconImageUrl
     *     The lageIconImageUrl
     */
    public void setLageIconImageUrl(String lageIconImageUrl) {
        this.lageIconImageUrl = lageIconImageUrl;
    }

    /**
     * 
     * @return
     *     The smallIconImageUrl
     */
    public String getSmallIconImageUrl() {
        return smallIconImageUrl;
    }

    /**
     * 
     * @param smallIconImageUrl
     *     The smallIconImageUrl
     */
    public void setSmallIconImageUrl(String smallIconImageUrl) {
        this.smallIconImageUrl = smallIconImageUrl;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
