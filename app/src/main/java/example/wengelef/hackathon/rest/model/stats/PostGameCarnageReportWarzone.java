
package example.wengelef.hackathon.rest.model.stats;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class PostGameCarnageReportWarzone {

    @SerializedName("PlayerStats")
    @Expose
    private List<PlayerStat> PlayerStats = new ArrayList<PlayerStat>();
    @SerializedName("TeamStats")
    @Expose
    private List<TeamStat> TeamStats = new ArrayList<TeamStat>();
    @SerializedName("IsMatchOver")
    @Expose
    private Boolean IsMatchOver;
    @SerializedName("TotalDuration")
    @Expose
    private String TotalDuration;
    @SerializedName("MapVariantId")
    @Expose
    private String MapVariantId;
    @SerializedName("GameVariantId")
    @Expose
    private String GameVariantId;
    @SerializedName("PlaylistId")
    @Expose
    private String PlaylistId;
    @SerializedName("MapId")
    @Expose
    private String MapId;
    @SerializedName("GameBaseVariantId")
    @Expose
    private String GameBaseVariantId;
    @SerializedName("IsTeamGame")
    @Expose
    private Boolean IsTeamGame;
    @SerializedName("SeasonId")
    @Expose
    private Object SeasonId;

    /**
     * 
     * @return
     *     The PlayerStats
     */
    public List<PlayerStat> getPlayerStats() {
        return PlayerStats;
    }

    /**
     * 
     * @param PlayerStats
     *     The PlayerStats
     */
    public void setPlayerStats(List<PlayerStat> PlayerStats) {
        this.PlayerStats = PlayerStats;
    }

    /**
     * 
     * @return
     *     The TeamStats
     */
    public List<TeamStat> getTeamStats() {
        return TeamStats;
    }

    /**
     * 
     * @param TeamStats
     *     The TeamStats
     */
    public void setTeamStats(List<TeamStat> TeamStats) {
        this.TeamStats = TeamStats;
    }

    /**
     * 
     * @return
     *     The IsMatchOver
     */
    public Boolean getIsMatchOver() {
        return IsMatchOver;
    }

    /**
     * 
     * @param IsMatchOver
     *     The IsMatchOver
     */
    public void setIsMatchOver(Boolean IsMatchOver) {
        this.IsMatchOver = IsMatchOver;
    }

    /**
     * 
     * @return
     *     The TotalDuration
     */
    public String getTotalDuration() {
        return TotalDuration;
    }

    /**
     * 
     * @param TotalDuration
     *     The TotalDuration
     */
    public void setTotalDuration(String TotalDuration) {
        this.TotalDuration = TotalDuration;
    }

    /**
     * 
     * @return
     *     The MapVariantId
     */
    public String getMapVariantId() {
        return MapVariantId;
    }

    /**
     * 
     * @param MapVariantId
     *     The MapVariantId
     */
    public void setMapVariantId(String MapVariantId) {
        this.MapVariantId = MapVariantId;
    }

    /**
     * 
     * @return
     *     The GameVariantId
     */
    public String getGameVariantId() {
        return GameVariantId;
    }

    /**
     * 
     * @param GameVariantId
     *     The GameVariantId
     */
    public void setGameVariantId(String GameVariantId) {
        this.GameVariantId = GameVariantId;
    }

    /**
     * 
     * @return
     *     The PlaylistId
     */
    public String getPlaylistId() {
        return PlaylistId;
    }

    /**
     * 
     * @param PlaylistId
     *     The PlaylistId
     */
    public void setPlaylistId(String PlaylistId) {
        this.PlaylistId = PlaylistId;
    }

    /**
     * 
     * @return
     *     The MapId
     */
    public String getMapId() {
        return MapId;
    }

    /**
     * 
     * @param MapId
     *     The MapId
     */
    public void setMapId(String MapId) {
        this.MapId = MapId;
    }

    /**
     * 
     * @return
     *     The GameBaseVariantId
     */
    public String getGameBaseVariantId() {
        return GameBaseVariantId;
    }

    /**
     * 
     * @param GameBaseVariantId
     *     The GameBaseVariantId
     */
    public void setGameBaseVariantId(String GameBaseVariantId) {
        this.GameBaseVariantId = GameBaseVariantId;
    }

    /**
     * 
     * @return
     *     The IsTeamGame
     */
    public Boolean getIsTeamGame() {
        return IsTeamGame;
    }

    /**
     * 
     * @param IsTeamGame
     *     The IsTeamGame
     */
    public void setIsTeamGame(Boolean IsTeamGame) {
        this.IsTeamGame = IsTeamGame;
    }

    /**
     * 
     * @return
     *     The SeasonId
     */
    public Object getSeasonId() {
        return SeasonId;
    }

    /**
     * 
     * @param SeasonId
     *     The SeasonId
     */
    public void setSeasonId(Object SeasonId) {
        this.SeasonId = SeasonId;
    }



    @Nullable
    public List<PlayerStat> getPlayersForTeamId(Integer teamId) {
        List<PlayerStat> players = new ArrayList<>();

        for (PlayerStat stats : PlayerStats) {
            if (teamId.equals(stats.getTeamId())) {
                players.add(stats);
            }
        }

        Collections.sort(players, new Comparator<PlayerStat>() {
            @Override
            public int compare(PlayerStat lhs, PlayerStat rhs) {
                return lhs.getRank().compareTo(rhs.getRank());
            }
        });
        return players;
    }

    @Nullable
    public List<TeamStat> getTeamsByRank() {
        Collections.sort(TeamStats, new Comparator<TeamStat>() {
            @Override
            public int compare(TeamStat lhs, TeamStat rhs) {
                return lhs.getRank().compareTo(rhs.getRank());
            }
        });
        return TeamStats;
    }
}
