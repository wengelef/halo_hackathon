package example.wengelef.hackathon.rest;

import javax.inject.Singleton;

import dagger.Component;
import example.wengelef.hackathon.ui.MainActivity;
import example.wengelef.hackathon.ui.fragment.ArenaStatsFragment;
import example.wengelef.hackathon.ui.fragment.ArenaStatsPagerFragment;
import example.wengelef.hackathon.ui.fragment.BaseSpartanFragment;
import example.wengelef.hackathon.ui.fragment.CampaignStatsFragment;
import example.wengelef.hackathon.ui.fragment.CustomStatsFragment;
import example.wengelef.hackathon.ui.fragment.RecentGamesDetailsDialogFragment;
import example.wengelef.hackathon.ui.fragment.RecentGamesFragment;
import example.wengelef.hackathon.ui.fragment.CampaignMissionsFragment;
import example.wengelef.hackathon.ui.fragment.LeaderboardsPagerFragment;
import example.wengelef.hackathon.ui.fragment.MapsFragment;
import example.wengelef.hackathon.ui.fragment.SpartanSearchFragment;
import example.wengelef.hackathon.ui.fragment.SpartanStatsFragment;
import example.wengelef.hackathon.ui.fragment.WarzoneStatsFragment;

/**
 * Created by fwengelewski on 6/6/16.
 *
 *
 * RestComponent
 *
 * Dagger Injector for RestModule
 */
@Singleton
@Component(modules = {RestModule.class})
public interface RestComponent {
    void inject(MainActivity mainActivity);
    void inject(BaseSpartanFragment baseSpartanFragment);
    void inject(MapsFragment mapsFragment);
    void inject(CampaignMissionsFragment campaignMissionsFragment);
    void inject(SpartanStatsFragment spartanStatsFragment);
    void inject(SpartanSearchFragment spartanSearchFragment);
    void inject(LeaderboardsPagerFragment leaderboardsPagerFragment);
    void inject(RecentGamesFragment recentGamesFragment);
    void inject(WarzoneStatsFragment warzoneStatsFragment);
    void inject(ArenaStatsFragment arenaStatsFragment);
    void inject(CampaignStatsFragment campaignStatsFragment);
    void inject(CustomStatsFragment customStatsFragment);
    void inject(RecentGamesDetailsDialogFragment recentGamesDatailsDialogFragment);
    void inject(ArenaStatsPagerFragment arenaStatsPagerFragment);
}
