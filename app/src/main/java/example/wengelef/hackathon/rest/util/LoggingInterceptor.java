package example.wengelef.hackathon.rest.util;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * LoggingInterceptor
 *
 * Custom HttpLoggingInterceptor to print responses for Retrofit 2.0
 */
public class LoggingInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Timber.d("[HTTP %s] ---------->  %s", request.method(), request.url());

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Timber.d("[HTTP %d] <---------- for %s in %.1fms%n%s", response.code(), response.request().url(), (t2 - t1) / 1e6d, response.headers());

        final String responseString = new String(response.body().bytes());
        Timber.d("[BODY]: %s\n%s\n[/BODY]", response.request().url(), responseString);

        return  response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), responseString))
                .build();
    }
}
