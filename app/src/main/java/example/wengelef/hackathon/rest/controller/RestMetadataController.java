package example.wengelef.hackathon.rest.controller;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import example.wengelef.hackathon.rest.RestModule;
import example.wengelef.hackathon.rest.api.RestMetadataInterface;
import example.wengelef.hackathon.rest.model.metadata.CampaignMission;
import example.wengelef.hackathon.rest.model.metadata.Commendation;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.metadata.Enemy;
import example.wengelef.hackathon.rest.model.metadata.FlexibleStat;
import example.wengelef.hackathon.rest.model.metadata.GameBaseVariant;
import example.wengelef.hackathon.rest.model.metadata.GameVariantForId;
import example.wengelef.hackathon.rest.model.metadata.Impulse;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.rest.model.metadata.MapDetail;
import example.wengelef.hackathon.rest.model.metadata.Medal;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.metadata.Requisition;
import example.wengelef.hackathon.rest.model.metadata.RequisitionPack;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.rest.model.metadata.Skull;
import example.wengelef.hackathon.rest.model.metadata.SpartanRank;
import example.wengelef.hackathon.rest.model.metadata.TeamColor;
import example.wengelef.hackathon.rest.model.metadata.Vehicle;
import example.wengelef.hackathon.rest.model.metadata.Weapon;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * RestMetadataController
 *
 * RestController for Metadata
 */
public class RestMetadataController extends RestController {

    private final @NonNull RestMetadataInterface mMetadataInterface;

    @Inject
    public RestMetadataController(@NonNull RestMetadataInterface restMetadataInterface) {
        mMetadataInterface = restMetadataInterface;
    }

    public Observable<List<CampaignMission>> getCampaignMissions() {
        return mMetadataInterface.getCampaignMissions(RestModule.getTitle());
    }

    public Observable<List<Commendation>> getCommendations() {
        return mMetadataInterface.getCommendations(RestModule.getTitle());
    }

    public Observable<List<CsrDesignation>> getCsrDesignations() {
        return mMetadataInterface.getCsrDesignations(RestModule.getTitle());
    }

    public Observable<List<Enemy>> getEnemies() {
        return mMetadataInterface.getEnemies(RestModule.getTitle());
    }

    public Observable<List<FlexibleStat>> getFlexibleStats() {
        return mMetadataInterface.getFlexibleStats(RestModule.getTitle());
    }

    public Observable<List<GameBaseVariant>> getGameBaseVariants() {
        return mMetadataInterface.getGameBaseVariants(RestModule.getTitle());
    }

    public Observable<GameVariantForId> getGameVariant(String id) {
        return mMetadataInterface.getGameVariant(RestModule.getTitle(), id);
    }

    public Observable<List<Impulse>> getImpulses() {
        return mMetadataInterface.getImpulses(RestModule.getTitle());
    }

    public Observable<MapDetail> getMapDetail(String id) {
        return mMetadataInterface.getMapDetail(RestModule.getTitle(), id);
    }

    public Observable<List<MapData>> getMaps() {
        return mMetadataInterface.getMaps(RestModule.getTitle());
    }

    public Observable<List<Medal>> getMedals() {
        return mMetadataInterface.getMedals(RestModule.getTitle());
    }

    public Observable<List<Playlist>> getPlaylists() {
        return mMetadataInterface.getPlaylists(RestModule.getTitle());
    }

    public Observable<RequisitionPack> getReqPack(String id) {
        return mMetadataInterface.getReqPack(RestModule.getTitle(), id);
    }

    public Observable<Requisition> getRequisition(String id) {
        return mMetadataInterface.getRequisition(RestModule.getTitle(), id);
    }

    public Observable<List<Season>> getSeasons() {
        return mMetadataInterface.getSeasons(RestModule.getTitle());
    }

    public Observable<List<Skull>> getSkulls() {
        return mMetadataInterface.getSkulls(RestModule.getTitle());
    }

    public Observable<List<SpartanRank>> getSpartanRanks() {
        return mMetadataInterface.getSpartanRanks(RestModule.getTitle());
    }

    public Observable<List<TeamColor>> getTeamColors() {
        return mMetadataInterface.getTeamColors(RestModule.getTitle());
    }

    public Observable<List<Vehicle>> getVehicles() {
        return mMetadataInterface.getVehicles(RestModule.getTitle());
    }

    public Observable<List<Weapon>> getWeapons() {
        return mMetadataInterface.getWeapons(RestModule.getTitle());
    }

    @Override
    public void onResponse(Call call, Response response) {
        Timber.i("onResponse: %s", response.body().toString());
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        Timber.e("onFailure: %s", t.getCause());
    }
}
