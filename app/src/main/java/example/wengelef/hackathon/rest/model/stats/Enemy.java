
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Enemy {

    @SerializedName("BaseId")
    @Expose
    private Long BaseId;
    @SerializedName("Attachments")
    @Expose
    private List<Long> Attachments = new ArrayList<Long>();

    /**
     * 
     * @return
     *     The BaseId
     */
    public Long getBaseId() {
        return BaseId;
    }

    /**
     * 
     * @param BaseId
     *     The BaseId
     */
    public void setBaseId(Long BaseId) {
        this.BaseId = BaseId;
    }

    /**
     * 
     * @return
     *     The Attachments
     */
    public List<Long> getAttachments() {
        return Attachments;
    }

    /**
     * 
     * @param Attachments
     *     The Attachments
     */
    public void setAttachments(List<Long> Attachments) {
        this.Attachments = Attachments;
    }

}
