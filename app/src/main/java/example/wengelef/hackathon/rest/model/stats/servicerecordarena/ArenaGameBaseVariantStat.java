
package example.wengelef.hackathon.rest.model.stats.servicerecordarena;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import example.wengelef.hackathon.rest.model.stats.DestroyedEnemyVehicle;
import example.wengelef.hackathon.rest.model.stats.EnemyKill;
import example.wengelef.hackathon.rest.model.stats.FlexibleStats;
import example.wengelef.hackathon.rest.model.stats.Impulse;
import example.wengelef.hackathon.rest.model.stats.MedalAward;
import example.wengelef.hackathon.rest.model.stats.WeaponStat;
import example.wengelef.hackathon.rest.model.stats.WeaponWithMostKills;

public class ArenaGameBaseVariantStat {

    @SerializedName("FlexibleStats")
    @Expose
    private FlexibleStats FlexibleStats;
    @SerializedName("GameBaseVariantId")
    @Expose
    private String GameBaseVariantId;
    @SerializedName("TotalKills")
    @Expose
    private Long TotalKills;
    @SerializedName("TotalHeadshots")
    @Expose
    private Long TotalHeadshots;
    @SerializedName("TotalWeaponDamage")
    @Expose
    private Double TotalWeaponDamage;
    @SerializedName("TotalShotsFired")
    @Expose
    private Long TotalShotsFired;
    @SerializedName("TotalShotsLanded")
    @Expose
    private Long TotalShotsLanded;
    @SerializedName("WeaponWithMostKills")
    @Expose
    private WeaponWithMostKills WeaponWithMostKills;
    @SerializedName("TotalMeleeKills")
    @Expose
    private Long TotalMeleeKills;
    @SerializedName("TotalMeleeDamage")
    @Expose
    private Double TotalMeleeDamage;
    @SerializedName("TotalAssassinations")
    @Expose
    private Long TotalAssassinations;
    @SerializedName("TotalGroundPoundKills")
    @Expose
    private Long TotalGroundPoundKills;
    @SerializedName("TotalGroundPoundDamage")
    @Expose
    private Double TotalGroundPoundDamage;
    @SerializedName("TotalShoulderBashKills")
    @Expose
    private Long TotalShoulderBashKills;
    @SerializedName("TotalShoulderBashDamage")
    @Expose
    private Double TotalShoulderBashDamage;
    @SerializedName("TotalGrenadeDamage")
    @Expose
    private Double TotalGrenadeDamage;
    @SerializedName("TotalPowerWeaponKills")
    @Expose
    private Long TotalPowerWeaponKills;
    @SerializedName("TotalPowerWeaponDamage")
    @Expose
    private Double TotalPowerWeaponDamage;
    @SerializedName("TotalPowerWeaponGrabs")
    @Expose
    private Long TotalPowerWeaponGrabs;
    @SerializedName("TotalPowerWeaponPossessionTime")
    @Expose
    private String TotalPowerWeaponPossessionTime;
    @SerializedName("TotalDeaths")
    @Expose
    private Long TotalDeaths;
    @SerializedName("TotalAssists")
    @Expose
    private Long TotalAssists;
    @SerializedName("TotalGamesCompleted")
    @Expose
    private Long TotalGamesCompleted;
    @SerializedName("TotalGamesWon")
    @Expose
    private Long TotalGamesWon;
    @SerializedName("TotalGamesLost")
    @Expose
    private Long TotalGamesLost;
    @SerializedName("TotalGamesTied")
    @Expose
    private Long TotalGamesTied;
    @SerializedName("TotalTimePlayed")
    @Expose
    private String TotalTimePlayed;
    @SerializedName("TotalGrenadeKills")
    @Expose
    private Long TotalGrenadeKills;
    @SerializedName("MedalAwards")
    @Expose
    private List<MedalAward> MedalAwards = new ArrayList<MedalAward>();
    @SerializedName("DestroyedEnemyVehicles")
    @Expose
    private List<DestroyedEnemyVehicle> DestroyedEnemyVehicles = new ArrayList<DestroyedEnemyVehicle>();
    @SerializedName("EnemyKills")
    @Expose
    private List<EnemyKill> EnemyKills = new ArrayList<EnemyKill>();
    @SerializedName("WeaponStats")
    @Expose
    private List<WeaponStat> WeaponStats = new ArrayList<WeaponStat>();
    @SerializedName("Impulses")
    @Expose
    private List<Impulse> Impulses = new ArrayList<Impulse>();
    @SerializedName("TotalSpartanKills")
    @Expose
    private Long TotalSpartanKills;

    /**
     * 
     * @return
     *     The FlexibleStats
     */
    public FlexibleStats getFlexibleStats() {
        return FlexibleStats;
    }

    /**
     * 
     * @param FlexibleStats
     *     The FlexibleStats
     */
    public void setFlexibleStats(FlexibleStats FlexibleStats) {
        this.FlexibleStats = FlexibleStats;
    }

    /**
     * 
     * @return
     *     The GameBaseVariantId
     */
    public String getGameBaseVariantId() {
        return GameBaseVariantId;
    }

    /**
     * 
     * @param GameBaseVariantId
     *     The GameBaseVariantId
     */
    public void setGameBaseVariantId(String GameBaseVariantId) {
        this.GameBaseVariantId = GameBaseVariantId;
    }

    /**
     * 
     * @return
     *     The TotalKills
     */
    public Long getTotalKills() {
        return TotalKills;
    }

    /**
     * 
     * @param TotalKills
     *     The TotalKills
     */
    public void setTotalKills(Long TotalKills) {
        this.TotalKills = TotalKills;
    }

    /**
     * 
     * @return
     *     The TotalHeadshots
     */
    public Long getTotalHeadshots() {
        return TotalHeadshots;
    }

    /**
     * 
     * @param TotalHeadshots
     *     The TotalHeadshots
     */
    public void setTotalHeadshots(Long TotalHeadshots) {
        this.TotalHeadshots = TotalHeadshots;
    }

    /**
     * 
     * @return
     *     The TotalWeaponDamage
     */
    public Double getTotalWeaponDamage() {
        return TotalWeaponDamage;
    }

    /**
     * 
     * @param TotalWeaponDamage
     *     The TotalWeaponDamage
     */
    public void setTotalWeaponDamage(Double TotalWeaponDamage) {
        this.TotalWeaponDamage = TotalWeaponDamage;
    }

    /**
     * 
     * @return
     *     The TotalShotsFired
     */
    public Long getTotalShotsFired() {
        return TotalShotsFired;
    }

    /**
     * 
     * @param TotalShotsFired
     *     The TotalShotsFired
     */
    public void setTotalShotsFired(Long TotalShotsFired) {
        this.TotalShotsFired = TotalShotsFired;
    }

    /**
     * 
     * @return
     *     The TotalShotsLanded
     */
    public Long getTotalShotsLanded() {
        return TotalShotsLanded;
    }

    /**
     * 
     * @param TotalShotsLanded
     *     The TotalShotsLanded
     */
    public void setTotalShotsLanded(Long TotalShotsLanded) {
        this.TotalShotsLanded = TotalShotsLanded;
    }

    /**
     * 
     * @return
     *     The WeaponWithMostKills
     */
    public WeaponWithMostKills getWeaponWithMostKills() {
        return WeaponWithMostKills;
    }

    /**
     * 
     * @param WeaponWithMostKills
     *     The WeaponWithMostKills
     */
    public void setWeaponWithMostKills(WeaponWithMostKills WeaponWithMostKills) {
        this.WeaponWithMostKills = WeaponWithMostKills;
    }

    /**
     * 
     * @return
     *     The TotalMeleeKills
     */
    public Long getTotalMeleeKills() {
        return TotalMeleeKills;
    }

    /**
     * 
     * @param TotalMeleeKills
     *     The TotalMeleeKills
     */
    public void setTotalMeleeKills(Long TotalMeleeKills) {
        this.TotalMeleeKills = TotalMeleeKills;
    }

    /**
     * 
     * @return
     *     The TotalMeleeDamage
     */
    public Double getTotalMeleeDamage() {
        return TotalMeleeDamage;
    }

    /**
     * 
     * @param TotalMeleeDamage
     *     The TotalMeleeDamage
     */
    public void setTotalMeleeDamage(Double TotalMeleeDamage) {
        this.TotalMeleeDamage = TotalMeleeDamage;
    }

    /**
     * 
     * @return
     *     The TotalAssassinations
     */
    public Long getTotalAssassinations() {
        return TotalAssassinations;
    }

    /**
     * 
     * @param TotalAssassinations
     *     The TotalAssassinations
     */
    public void setTotalAssassinations(Long TotalAssassinations) {
        this.TotalAssassinations = TotalAssassinations;
    }

    /**
     * 
     * @return
     *     The TotalGroundPoundKills
     */
    public Long getTotalGroundPoundKills() {
        return TotalGroundPoundKills;
    }

    /**
     * 
     * @param TotalGroundPoundKills
     *     The TotalGroundPoundKills
     */
    public void setTotalGroundPoundKills(Long TotalGroundPoundKills) {
        this.TotalGroundPoundKills = TotalGroundPoundKills;
    }

    /**
     * 
     * @return
     *     The TotalGroundPoundDamage
     */
    public Double getTotalGroundPoundDamage() {
        return TotalGroundPoundDamage;
    }

    /**
     * 
     * @param TotalGroundPoundDamage
     *     The TotalGroundPoundDamage
     */
    public void setTotalGroundPoundDamage(Double TotalGroundPoundDamage) {
        this.TotalGroundPoundDamage = TotalGroundPoundDamage;
    }

    /**
     * 
     * @return
     *     The TotalShoulderBashKills
     */
    public Long getTotalShoulderBashKills() {
        return TotalShoulderBashKills;
    }

    /**
     * 
     * @param TotalShoulderBashKills
     *     The TotalShoulderBashKills
     */
    public void setTotalShoulderBashKills(Long TotalShoulderBashKills) {
        this.TotalShoulderBashKills = TotalShoulderBashKills;
    }

    /**
     * 
     * @return
     *     The TotalShoulderBashDamage
     */
    public Double getTotalShoulderBashDamage() {
        return TotalShoulderBashDamage;
    }

    /**
     * 
     * @param TotalShoulderBashDamage
     *     The TotalShoulderBashDamage
     */
    public void setTotalShoulderBashDamage(Double TotalShoulderBashDamage) {
        this.TotalShoulderBashDamage = TotalShoulderBashDamage;
    }

    /**
     * 
     * @return
     *     The TotalGrenadeDamage
     */
    public Double getTotalGrenadeDamage() {
        return TotalGrenadeDamage;
    }

    /**
     * 
     * @param TotalGrenadeDamage
     *     The TotalGrenadeDamage
     */
    public void setTotalGrenadeDamage(Double TotalGrenadeDamage) {
        this.TotalGrenadeDamage = TotalGrenadeDamage;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponKills
     */
    public Long getTotalPowerWeaponKills() {
        return TotalPowerWeaponKills;
    }

    /**
     * 
     * @param TotalPowerWeaponKills
     *     The TotalPowerWeaponKills
     */
    public void setTotalPowerWeaponKills(Long TotalPowerWeaponKills) {
        this.TotalPowerWeaponKills = TotalPowerWeaponKills;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponDamage
     */
    public Double getTotalPowerWeaponDamage() {
        return TotalPowerWeaponDamage;
    }

    /**
     * 
     * @param TotalPowerWeaponDamage
     *     The TotalPowerWeaponDamage
     */
    public void setTotalPowerWeaponDamage(Double TotalPowerWeaponDamage) {
        this.TotalPowerWeaponDamage = TotalPowerWeaponDamage;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponGrabs
     */
    public Long getTotalPowerWeaponGrabs() {
        return TotalPowerWeaponGrabs;
    }

    /**
     * 
     * @param TotalPowerWeaponGrabs
     *     The TotalPowerWeaponGrabs
     */
    public void setTotalPowerWeaponGrabs(Long TotalPowerWeaponGrabs) {
        this.TotalPowerWeaponGrabs = TotalPowerWeaponGrabs;
    }

    /**
     * 
     * @return
     *     The TotalPowerWeaponPossessionTime
     */
    public String getTotalPowerWeaponPossessionTime() {
        return TotalPowerWeaponPossessionTime;
    }

    /**
     * 
     * @param TotalPowerWeaponPossessionTime
     *     The TotalPowerWeaponPossessionTime
     */
    public void setTotalPowerWeaponPossessionTime(String TotalPowerWeaponPossessionTime) {
        this.TotalPowerWeaponPossessionTime = TotalPowerWeaponPossessionTime;
    }

    /**
     * 
     * @return
     *     The TotalDeaths
     */
    public Long getTotalDeaths() {
        return TotalDeaths;
    }

    /**
     * 
     * @param TotalDeaths
     *     The TotalDeaths
     */
    public void setTotalDeaths(Long TotalDeaths) {
        this.TotalDeaths = TotalDeaths;
    }

    /**
     * 
     * @return
     *     The TotalAssists
     */
    public Long getTotalAssists() {
        return TotalAssists;
    }

    /**
     * 
     * @param TotalAssists
     *     The TotalAssists
     */
    public void setTotalAssists(Long TotalAssists) {
        this.TotalAssists = TotalAssists;
    }

    /**
     * 
     * @return
     *     The TotalGamesCompleted
     */
    public Long getTotalGamesCompleted() {
        return TotalGamesCompleted;
    }

    /**
     * 
     * @param TotalGamesCompleted
     *     The TotalGamesCompleted
     */
    public void setTotalGamesCompleted(Long TotalGamesCompleted) {
        this.TotalGamesCompleted = TotalGamesCompleted;
    }

    /**
     * 
     * @return
     *     The TotalGamesWon
     */
    public Long getTotalGamesWon() {
        return TotalGamesWon;
    }

    /**
     * 
     * @param TotalGamesWon
     *     The TotalGamesWon
     */
    public void setTotalGamesWon(Long TotalGamesWon) {
        this.TotalGamesWon = TotalGamesWon;
    }

    /**
     * 
     * @return
     *     The TotalGamesLost
     */
    public Long getTotalGamesLost() {
        return TotalGamesLost;
    }

    /**
     * 
     * @param TotalGamesLost
     *     The TotalGamesLost
     */
    public void setTotalGamesLost(Long TotalGamesLost) {
        this.TotalGamesLost = TotalGamesLost;
    }

    /**
     * 
     * @return
     *     The TotalGamesTied
     */
    public Long getTotalGamesTied() {
        return TotalGamesTied;
    }

    /**
     * 
     * @param TotalGamesTied
     *     The TotalGamesTied
     */
    public void setTotalGamesTied(Long TotalGamesTied) {
        this.TotalGamesTied = TotalGamesTied;
    }

    /**
     * 
     * @return
     *     The TotalTimePlayed
     */
    public String getTotalTimePlayed() {
        return TotalTimePlayed;
    }

    /**
     * 
     * @param TotalTimePlayed
     *     The TotalTimePlayed
     */
    public void setTotalTimePlayed(String TotalTimePlayed) {
        this.TotalTimePlayed = TotalTimePlayed;
    }

    /**
     * 
     * @return
     *     The TotalGrenadeKills
     */
    public Long getTotalGrenadeKills() {
        return TotalGrenadeKills;
    }

    /**
     * 
     * @param TotalGrenadeKills
     *     The TotalGrenadeKills
     */
    public void setTotalGrenadeKills(Long TotalGrenadeKills) {
        this.TotalGrenadeKills = TotalGrenadeKills;
    }

    /**
     * 
     * @return
     *     The MedalAwards
     */
    public List<MedalAward> getMedalAwards() {
        return MedalAwards;
    }

    /**
     * 
     * @param MedalAwards
     *     The MedalAwards
     */
    public void setMedalAwards(List<MedalAward> MedalAwards) {
        this.MedalAwards = MedalAwards;
    }

    /**
     * 
     * @return
     *     The DestroyedEnemyVehicles
     */
    public List<DestroyedEnemyVehicle> getDestroyedEnemyVehicles() {
        return DestroyedEnemyVehicles;
    }

    /**
     * 
     * @param DestroyedEnemyVehicles
     *     The DestroyedEnemyVehicles
     */
    public void setDestroyedEnemyVehicles(List<DestroyedEnemyVehicle> DestroyedEnemyVehicles) {
        this.DestroyedEnemyVehicles = DestroyedEnemyVehicles;
    }

    /**
     * 
     * @return
     *     The EnemyKills
     */
    public List<EnemyKill> getEnemyKills() {
        return EnemyKills;
    }

    /**
     * 
     * @param EnemyKills
     *     The EnemyKills
     */
    public void setEnemyKills(List<EnemyKill> EnemyKills) {
        this.EnemyKills = EnemyKills;
    }

    /**
     * 
     * @return
     *     The WeaponStats
     */
    public List<WeaponStat> getWeaponStats() {
        return WeaponStats;
    }

    /**
     * 
     * @param WeaponStats
     *     The WeaponStats
     */
    public void setWeaponStats(List<WeaponStat> WeaponStats) {
        this.WeaponStats = WeaponStats;
    }

    /**
     * 
     * @return
     *     The Impulses
     */
    public List<Impulse> getImpulses() {
        return Impulses;
    }

    /**
     * 
     * @param Impulses
     *     The Impulses
     */
    public void setImpulses(List<Impulse> Impulses) {
        this.Impulses = Impulses;
    }

    /**
     * 
     * @return
     *     The TotalSpartanKills
     */
    public Long getTotalSpartanKills() {
        return TotalSpartanKills;
    }

    /**
     * 
     * @param TotalSpartanKills
     *     The TotalSpartanKills
     */
    public void setTotalSpartanKills(Long TotalSpartanKills) {
        this.TotalSpartanKills = TotalSpartanKills;
    }

}
