
package example.wengelef.hackathon.rest.model.stats;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceRecordResult implements Serializable {

    @SerializedName("Links")
    @Expose
    private Object Links;
    @SerializedName("Id")
    @Expose
    private Id Id;
    @SerializedName("HopperId")
    @Expose
    private String HopperId;
    @SerializedName("MapId")
    @Expose
    private String MapId;
    @SerializedName("MapVariant")
    @Expose
    private MapVariant MapVariant;
    @SerializedName("GameBaseVariantId")
    @Expose
    private String GameBaseVariantId;
    @SerializedName("GameVariant")
    @Expose
    private GameVariant GameVariant;
    @SerializedName("MatchDuration")
    @Expose
    private String MatchDuration;
    @SerializedName("MatchCompletedDate")
    @Expose
    private MatchCompletedDate MatchCompletedDate;
    @SerializedName("Teams")
    @Expose
    private List<Team> Teams = new ArrayList<Team>();
    @SerializedName("Players")
    @Expose
    private List<Players> Players = new ArrayList<Players>();
    @SerializedName("IsTeamGame")
    @Expose
    private Boolean IsTeamGame;
    @SerializedName("SeasonId")
    @Expose
    private String SeasonId;
    @SerializedName("MatchCompletedDateFidelity")
    @Expose
    private Integer MatchCompletedDateFidelity;

    /**
     * 
     * @return
     *     The Links
     */
    public Object getLinks() {
        return Links;
    }

    /**
     * 
     * @param Links
     *     The Links
     */
    public void setLinks(Object Links) {
        this.Links = Links;
    }

    /**
     * 
     * @return
     *     The Id
     */
    public Id getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(Id Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The HopperId
     */
    public String getHopperId() {
        return HopperId;
    }

    /**
     * 
     * @param HopperId
     *     The HopperId
     */
    public void setHopperId(String HopperId) {
        this.HopperId = HopperId;
    }

    /**
     * 
     * @return
     *     The MapId
     */
    public String getMapId() {
        return MapId;
    }

    /**
     * 
     * @param MapId
     *     The MapId
     */
    public void setMapId(String MapId) {
        this.MapId = MapId;
    }

    /**
     * 
     * @return
     *     The MapVariant
     */
    public MapVariant getMapVariant() {
        return MapVariant;
    }

    /**
     * 
     * @param MapVariant
     *     The MapVariant
     */
    public void setMapVariant(MapVariant MapVariant) {
        this.MapVariant = MapVariant;
    }

    /**
     * 
     * @return
     *     The GameBaseVariantId
     */
    public String getGameBaseVariantId() {
        return GameBaseVariantId;
    }

    /**
     * 
     * @param GameBaseVariantId
     *     The GameBaseVariantId
     */
    public void setGameBaseVariantId(String GameBaseVariantId) {
        this.GameBaseVariantId = GameBaseVariantId;
    }

    /**
     * 
     * @return
     *     The GameVariant
     */
    public GameVariant getGameVariant() {
        return GameVariant;
    }

    /**
     * 
     * @param GameVariant
     *     The GameVariant
     */
    public void setGameVariant(GameVariant GameVariant) {
        this.GameVariant = GameVariant;
    }

    /**
     * 
     * @return
     *     The MatchDuration
     */
    public String getMatchDuration() {
        return MatchDuration;
    }

    /**
     * 
     * @param MatchDuration
     *     The MatchDuration
     */
    public void setMatchDuration(String MatchDuration) {
        this.MatchDuration = MatchDuration;
    }

    /**
     * 
     * @return
     *     The MatchCompletedDate
     */
    public MatchCompletedDate getMatchCompletedDate() {
        return MatchCompletedDate;
    }

    /**
     * 
     * @param MatchCompletedDate
     *     The MatchCompletedDate
     */
    public void setMatchCompletedDate(MatchCompletedDate MatchCompletedDate) {
        this.MatchCompletedDate = MatchCompletedDate;
    }

    /**
     * 
     * @return
     *     The Teams
     */
    public List<Team> getTeams() {
        return Teams;
    }

    /**
     * 
     * @param Teams
     *     The Teams
     */
    public void setTeams(List<Team> Teams) {
        this.Teams = Teams;
    }

    /**
     * 
     * @return
     *     The Players
     */
    public List<Players> getPlayers() {
        return Players;
    }

    /**
     * 
     * @param Players
     *     The Players
     */
    public void setPlayers(List<Players> Players) {
        this.Players = Players;
    }

    /**
     * 
     * @return
     *     The IsTeamGame
     */
    public Boolean getIsTeamGame() {
        return IsTeamGame;
    }

    /**
     * 
     * @param IsTeamGame
     *     The IsTeamGame
     */
    public void setIsTeamGame(Boolean IsTeamGame) {
        this.IsTeamGame = IsTeamGame;
    }

    /**
     * 
     * @return
     *     The SeasonId
     */
    public String getSeasonId() {
        return SeasonId;
    }

    /**
     * 
     * @param SeasonId
     *     The SeasonId
     */
    public void setSeasonId(String SeasonId) {
        this.SeasonId = SeasonId;
    }

    /**
     * 
     * @return
     *     The MatchCompletedDateFidelity
     */
    public Integer getMatchCompletedDateFidelity() {
        return MatchCompletedDateFidelity;
    }

    /**
     * 
     * @param MatchCompletedDateFidelity
     *     The MatchCompletedDateFidelity
     */
    public void setMatchCompletedDateFidelity(Integer MatchCompletedDateFidelity) {
        this.MatchCompletedDateFidelity = MatchCompletedDateFidelity;
    }

    /**
     * Format Team Scores
     * @return Ex. "12 - 15"
     */
    @Nullable
    public String formattedTeamScores() {
        if (Teams != null && Teams.size() > 1) {
            return String.format("%s - %s", Teams.get(0).getScore(), Teams.get(1).getScore());
        }
        return null;
    }
}
