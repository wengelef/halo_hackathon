package example.wengelef.hackathon.rest.api;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import example.wengelef.hackathon.event.ProgressEvent;
import example.wengelef.hackathon.rest.model.metadata.CampaignMission;
import example.wengelef.hackathon.rest.model.metadata.Commendation;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.metadata.Enemy;
import example.wengelef.hackathon.rest.model.metadata.FlexibleStat;
import example.wengelef.hackathon.rest.model.metadata.GameBaseVariant;
import example.wengelef.hackathon.rest.model.metadata.GameVariantForId;
import example.wengelef.hackathon.rest.model.metadata.Impulse;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.rest.model.metadata.MapDetail;
import example.wengelef.hackathon.rest.model.metadata.Medal;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.metadata.Requisition;
import example.wengelef.hackathon.rest.model.metadata.RequisitionPack;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.rest.model.metadata.Skull;
import example.wengelef.hackathon.rest.model.metadata.SpartanRank;
import example.wengelef.hackathon.rest.model.metadata.TeamColor;
import example.wengelef.hackathon.rest.model.metadata.Vehicle;
import example.wengelef.hackathon.rest.model.metadata.Weapon;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * DecoratedMetadataInterface
 *
 * Decorates the {@link RestMetadataInterface}
 */
public class DecoratedMetadataInterface implements RestMetadataInterface {

    private final RestMetadataInterface mMetadataInterface;

    public DecoratedMetadataInterface(@NonNull RestMetadataInterface restMetadataInterface) {
        showProgress();
        mMetadataInterface = restMetadataInterface;
    }

    @Override
    public Observable<List<CampaignMission>> getCampaignMissions(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getCampaignMissions(title);
    }

    @Override
    public Observable<List<Commendation>> getCommendations(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getCommendations(title);
    }

    @Override
    public Observable<List<CsrDesignation>> getCsrDesignations(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getCsrDesignations(title);
    }

    @Override
    public Observable<List<Enemy>> getEnemies(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getEnemies(title);
    }

    @Override
    public Observable<List<FlexibleStat>> getFlexibleStats(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getFlexibleStats(title);
    }

    @Override
    public Observable<List<GameBaseVariant>> getGameBaseVariants(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getGameBaseVariants(title);
    }

    @Override
    public Observable<GameVariantForId> getGameVariant(@Path("title") String title, @Path("id") String id) {
        showProgress();
        return mMetadataInterface.getGameVariant(title, id);
    }

    @Override
    public Observable<List<Impulse>> getImpulses(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getImpulses(title);
    }

    @Override
    public Observable<List<MapData>> getMaps(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getMaps(title);
    }

    @Override
    public Observable<MapDetail> getMapDetail(@Path("title") String title, @Path("id") String id) {
        showProgress();
        return mMetadataInterface.getMapDetail(title, id);
    }

    @Override
    public Observable<List<Medal>> getMedals(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getMedals(title);
    }

    @Override
    public Observable<List<Playlist>> getPlaylists(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getPlaylists(title);
    }

    @Override
    public Observable<RequisitionPack> getReqPack(@Path("title") String title, @Path("id") String id) {
        showProgress();
        return mMetadataInterface.getReqPack(title, id);
    }

    @Override
    public Observable<Requisition> getRequisition(@Path("title") String title, @Path("id") String id) {
        showProgress();
        return mMetadataInterface.getRequisition(title, id);
    }

    @Override
    public Observable<List<Season>> getSeasons(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getSeasons(title);
    }

    @Override
    public Observable<List<Skull>> getSkulls(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getSkulls(title);
    }

    @Override
    public Observable<List<SpartanRank>> getSpartanRanks(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getSpartanRanks(title);
    }

    @Override
    public Observable<List<TeamColor>> getTeamColors(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getTeamColors(title);
    }

    @Override
    public Observable<List<Vehicle>> getVehicles(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getVehicles(title);
    }

    @Override
    public Observable<List<Weapon>> getWeapons(@Path("title") String title) {
        showProgress();
        return mMetadataInterface.getWeapons(title);
    }

    @Override
    public void showProgress() {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.SHOW));
    }
}
