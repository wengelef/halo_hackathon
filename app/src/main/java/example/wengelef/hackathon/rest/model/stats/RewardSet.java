
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RewardSet {

    @SerializedName("RewardSet")
    @Expose
    private String RewardSet;
    @SerializedName("RewardSourceType")
    @Expose
    private Integer RewardSourceType;
    @SerializedName("SpartanRankSource")
    @Expose
    private Integer SpartanRankSource;
    @SerializedName("CommendationLevelId")
    @Expose
    private String CommendationLevelId;
    @SerializedName("CommendationSource")
    @Expose
    private String CommendationSource;

    /**
     * 
     * @return
     *     The RewardSet
     */
    public String getRewardSet() {
        return RewardSet;
    }

    /**
     * 
     * @param RewardSet
     *     The RewardSet
     */
    public void setRewardSet(String RewardSet) {
        this.RewardSet = RewardSet;
    }

    /**
     * 
     * @return
     *     The RewardSourceType
     */
    public Integer getRewardSourceType() {
        return RewardSourceType;
    }

    /**
     * 
     * @param RewardSourceType
     *     The RewardSourceType
     */
    public void setRewardSourceType(Integer RewardSourceType) {
        this.RewardSourceType = RewardSourceType;
    }

    /**
     * 
     * @return
     *     The SpartanRankSource
     */
    public Integer getSpartanRankSource() {
        return SpartanRankSource;
    }

    /**
     * 
     * @param SpartanRankSource
     *     The SpartanRankSource
     */
    public void setSpartanRankSource(Integer SpartanRankSource) {
        this.SpartanRankSource = SpartanRankSource;
    }

    /**
     * 
     * @return
     *     The CommendationLevelId
     */
    public String getCommendationLevelId() {
        return CommendationLevelId;
    }

    /**
     * 
     * @param CommendationLevelId
     *     The CommendationLevelId
     */
    public void setCommendationLevelId(String CommendationLevelId) {
        this.CommendationLevelId = CommendationLevelId;
    }

    /**
     * 
     * @return
     *     The CommendationSource
     */
    public String getCommendationSource() {
        return CommendationSource;
    }

    /**
     * 
     * @param CommendationSource
     *     The CommendationSource
     */
    public void setCommendationSource(String CommendationSource) {
        this.CommendationSource = CommendationSource;
    }

}
