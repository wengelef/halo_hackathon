package example.wengelef.hackathon.rest.controller;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * Rest Controller
 *
 * Abstract Base Class for RestControllers
 */
public abstract class RestController<ResponseType> implements Callback<ResponseType> {

    @Override
    public void onResponse(Call<ResponseType> call, Response<ResponseType> response) {
        if (response != null && response.body() != null) {
            Timber.i("onResponse: %s", response.body().toString());
        }
    }

    @Override
    public void onFailure(Call<ResponseType> call, Throwable t) {
        if (t.getCause() != null) {
            Timber.e("onFailure: %s", t.getCause().toString());
        }
    }
}
