
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopGameBaseVariant {

    @SerializedName("GameBaseVariantRank")
    @Expose
    private Long GameBaseVariantRank;
    @SerializedName("NumberOfMatchesCompleted")
    @Expose
    private Long NumberOfMatchesCompleted;
    @SerializedName("GameBaseVariantId")
    @Expose
    private String GameBaseVariantId;
    @SerializedName("NumberOfMatchesWon")
    @Expose
    private Long NumberOfMatchesWon;

    /**
     * 
     * @return
     *     The GameBaseVariantRank
     */
    public Long getGameBaseVariantRank() {
        return GameBaseVariantRank;
    }

    /**
     * 
     * @param GameBaseVariantRank
     *     The GameBaseVariantRank
     */
    public void setGameBaseVariantRank(Long GameBaseVariantRank) {
        this.GameBaseVariantRank = GameBaseVariantRank;
    }

    /**
     * 
     * @return
     *     The NumberOfMatchesCompleted
     */
    public Long getNumberOfMatchesCompleted() {
        return NumberOfMatchesCompleted;
    }

    /**
     * 
     * @param NumberOfMatchesCompleted
     *     The NumberOfMatchesCompleted
     */
    public void setNumberOfMatchesCompleted(Long NumberOfMatchesCompleted) {
        this.NumberOfMatchesCompleted = NumberOfMatchesCompleted;
    }

    /**
     * 
     * @return
     *     The GameBaseVariantId
     */
    public String getGameBaseVariantId() {
        return GameBaseVariantId;
    }

    /**
     * 
     * @param GameBaseVariantId
     *     The GameBaseVariantId
     */
    public void setGameBaseVariantId(String GameBaseVariantId) {
        this.GameBaseVariantId = GameBaseVariantId;
    }

    /**
     * 
     * @return
     *     The NumberOfMatchesWon
     */
    public Long getNumberOfMatchesWon() {
        return NumberOfMatchesWon;
    }

    /**
     * 
     * @param NumberOfMatchesWon
     *     The NumberOfMatchesWon
     */
    public void setNumberOfMatchesWon(Long NumberOfMatchesWon) {
        this.NumberOfMatchesWon = NumberOfMatchesWon;
    }

}
