
package example.wengelef.hackathon.rest.model.stats;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProgressiveCommendationDelta {

    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("PreviousProgress")
    @Expose
    private Integer PreviousProgress;
    @SerializedName("Progress")
    @Expose
    private Integer Progress;

    /**
     * 
     * @return
     *     The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The PreviousProgress
     */
    public Integer getPreviousProgress() {
        return PreviousProgress;
    }

    /**
     * 
     * @param PreviousProgress
     *     The PreviousProgress
     */
    public void setPreviousProgress(Integer PreviousProgress) {
        this.PreviousProgress = PreviousProgress;
    }

    /**
     * 
     * @return
     *     The Progress
     */
    public Integer getProgress() {
        return Progress;
    }

    /**
     * 
     * @param Progress
     *     The Progress
     */
    public void setProgress(Integer Progress) {
        this.Progress = Progress;
    }

}
