
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedalAward {

    @SerializedName("MedalId")
    @Expose
    private Long MedalId;
    @SerializedName("Count")
    @Expose
    private Integer Count;

    /**
     * 
     * @return
     *     The MedalId
     */
    public Long getMedalId() {
        return MedalId;
    }

    /**
     * 
     * @param MedalId
     *     The MedalId
     */
    public void setMedalId(Long MedalId) {
        this.MedalId = MedalId;
    }

    /**
     * 
     * @return
     *     The Count
     */
    public Integer getCount() {
        return Count;
    }

    /**
     * 
     * @param Count
     *     The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

}
