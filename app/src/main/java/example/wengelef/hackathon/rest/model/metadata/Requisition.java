
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Requisition {

    @SerializedName("supportedGameModes")
    @Expose
    private List<String> supportedGameModes = new ArrayList<String>();
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("rarityType")
    @Expose
    private String rarityType;
    @SerializedName("rarity")
    @Expose
    private String rarity;
    @SerializedName("isMythic")
    @Expose
    private String isMythic;
    @SerializedName("isCertification")
    @Expose
    private String isCertification;
    @SerializedName("isWearable")
    @Expose
    private String isWearable;
    @SerializedName("hideIfNotAcquired")
    @Expose
    private String hideIfNotAcquired;
    @SerializedName("useType")
    @Expose
    private String useType;
    @SerializedName("largeImageUrl")
    @Expose
    private String largeImageUrl;
    @SerializedName("mediumImageUrl")
    @Expose
    private String mediumImageUrl;
    @SerializedName("smallImageUrl")
    @Expose
    private String smallImageUrl;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("internalCategoryName")
    @Expose
    private String internalCategoryName;
    @SerializedName("subcategoryName")
    @Expose
    private String subcategoryName;
    @SerializedName("subcategoryOrder")
    @Expose
    private String subcategoryOrder;
    @SerializedName("sellPrice")
    @Expose
    private String sellPrice;
    @SerializedName("levelRequirement")
    @Expose
    private String levelRequirement;
    @SerializedName("creditsAwarded")
    @Expose
    private String creditsAwarded;
    @SerializedName("certificationRequisitionId")
    @Expose
    private String certificationRequisitionId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The supportedGameModes
     */
    public List<String> getSupportedGameModes() {
        return supportedGameModes;
    }

    /**
     * 
     * @param supportedGameModes
     *     The supportedGameModes
     */
    public void setSupportedGameModes(List<String> supportedGameModes) {
        this.supportedGameModes = supportedGameModes;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The rarityType
     */
    public String getRarityType() {
        return rarityType;
    }

    /**
     * 
     * @param rarityType
     *     The rarityType
     */
    public void setRarityType(String rarityType) {
        this.rarityType = rarityType;
    }

    /**
     * 
     * @return
     *     The rarity
     */
    public String getRarity() {
        return rarity;
    }

    /**
     * 
     * @param rarity
     *     The rarity
     */
    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    /**
     * 
     * @return
     *     The isMythic
     */
    public String getIsMythic() {
        return isMythic;
    }

    /**
     * 
     * @param isMythic
     *     The isMythic
     */
    public void setIsMythic(String isMythic) {
        this.isMythic = isMythic;
    }

    /**
     * 
     * @return
     *     The isCertification
     */
    public String getIsCertification() {
        return isCertification;
    }

    /**
     * 
     * @param isCertification
     *     The isCertification
     */
    public void setIsCertification(String isCertification) {
        this.isCertification = isCertification;
    }

    /**
     * 
     * @return
     *     The isWearable
     */
    public String getIsWearable() {
        return isWearable;
    }

    /**
     * 
     * @param isWearable
     *     The isWearable
     */
    public void setIsWearable(String isWearable) {
        this.isWearable = isWearable;
    }

    /**
     * 
     * @return
     *     The hideIfNotAcquired
     */
    public String getHideIfNotAcquired() {
        return hideIfNotAcquired;
    }

    /**
     * 
     * @param hideIfNotAcquired
     *     The hideIfNotAcquired
     */
    public void setHideIfNotAcquired(String hideIfNotAcquired) {
        this.hideIfNotAcquired = hideIfNotAcquired;
    }

    /**
     * 
     * @return
     *     The useType
     */
    public String getUseType() {
        return useType;
    }

    /**
     * 
     * @param useType
     *     The useType
     */
    public void setUseType(String useType) {
        this.useType = useType;
    }

    /**
     * 
     * @return
     *     The largeImageUrl
     */
    public String getLargeImageUrl() {
        return largeImageUrl;
    }

    /**
     * 
     * @param largeImageUrl
     *     The largeImageUrl
     */
    public void setLargeImageUrl(String largeImageUrl) {
        this.largeImageUrl = largeImageUrl;
    }

    /**
     * 
     * @return
     *     The mediumImageUrl
     */
    public String getMediumImageUrl() {
        return mediumImageUrl;
    }

    /**
     * 
     * @param mediumImageUrl
     *     The mediumImageUrl
     */
    public void setMediumImageUrl(String mediumImageUrl) {
        this.mediumImageUrl = mediumImageUrl;
    }

    /**
     * 
     * @return
     *     The smallImageUrl
     */
    public String getSmallImageUrl() {
        return smallImageUrl;
    }

    /**
     * 
     * @param smallImageUrl
     *     The smallImageUrl
     */
    public void setSmallImageUrl(String smallImageUrl) {
        this.smallImageUrl = smallImageUrl;
    }

    /**
     * 
     * @return
     *     The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 
     * @param categoryName
     *     The categoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 
     * @return
     *     The internalCategoryName
     */
    public String getInternalCategoryName() {
        return internalCategoryName;
    }

    /**
     * 
     * @param internalCategoryName
     *     The internalCategoryName
     */
    public void setInternalCategoryName(String internalCategoryName) {
        this.internalCategoryName = internalCategoryName;
    }

    /**
     * 
     * @return
     *     The subcategoryName
     */
    public String getSubcategoryName() {
        return subcategoryName;
    }

    /**
     * 
     * @param subcategoryName
     *     The subcategoryName
     */
    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    /**
     * 
     * @return
     *     The subcategoryOrder
     */
    public String getSubcategoryOrder() {
        return subcategoryOrder;
    }

    /**
     * 
     * @param subcategoryOrder
     *     The subcategoryOrder
     */
    public void setSubcategoryOrder(String subcategoryOrder) {
        this.subcategoryOrder = subcategoryOrder;
    }

    /**
     * 
     * @return
     *     The sellPrice
     */
    public String getSellPrice() {
        return sellPrice;
    }

    /**
     * 
     * @param sellPrice
     *     The sellPrice
     */
    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 
     * @return
     *     The levelRequirement
     */
    public String getLevelRequirement() {
        return levelRequirement;
    }

    /**
     * 
     * @param levelRequirement
     *     The levelRequirement
     */
    public void setLevelRequirement(String levelRequirement) {
        this.levelRequirement = levelRequirement;
    }

    /**
     * 
     * @return
     *     The creditsAwarded
     */
    public String getCreditsAwarded() {
        return creditsAwarded;
    }

    /**
     * 
     * @param creditsAwarded
     *     The creditsAwarded
     */
    public void setCreditsAwarded(String creditsAwarded) {
        this.creditsAwarded = creditsAwarded;
    }

    /**
     * 
     * @return
     *     The certificationRequisitionId
     */
    public String getCertificationRequisitionId() {
        return certificationRequisitionId;
    }

    /**
     * 
     * @param certificationRequisitionId
     *     The certificationRequisitionId
     */
    public void setCertificationRequisitionId(String certificationRequisitionId) {
        this.certificationRequisitionId = certificationRequisitionId;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
