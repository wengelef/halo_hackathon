
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import example.wengelef.hackathon.util.RealmString;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class GameBaseVariant extends RealmObject {

    @PrimaryKey
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("internalName")
    @Expose
    private String internalName;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("supportedGameModes")
    @Expose
    private RealmList<RealmString> supportedGameModes = new RealmList<>();
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The internalName
     */
    public String getInternalName() {
        return internalName;
    }

    /**
     * 
     * @param internalName
     *     The internalName
     */
    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    /**
     * 
     * @return
     *     The iconUrl
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * 
     * @param iconUrl
     *     The iconUrl
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * 
     * @return
     *     The supportedGameModes
     */
    public RealmList<RealmString> getSupportedGameModes() {
        return supportedGameModes;
    }

    /**
     * 
     * @param supportedGameModes
     *     The supportedGameModes
     */
    public void setSupportedGameModes(RealmList<RealmString> supportedGameModes) {
        this.supportedGameModes = supportedGameModes;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
