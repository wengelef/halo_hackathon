package example.wengelef.hackathon.rest.util;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.greenrobot.eventbus.EventBus;

import example.wengelef.hackathon.R;
import example.wengelef.hackathon.event.ProgressEvent;
import example.wengelef.hackathon.ui.widget.ProgressImageView;

/**
 * Created by fwengelewski on 4/7/16.
 *
 *
 * ImageLoader
 *
 * Load images with Glide
 */
public class ImageLoader {

    public static void loadFittedImage(String url, ProgressImageView imageView) {
        if (url != null){
            imageView.showProgress();
            Glide.with(imageView.getContext())
                    .load(url)
                    .listener(getCallback(imageView))
                    .centerCrop()
                    .crossFade()
                    .error(R.drawable.ic_error_black_24dp)
                    .into(imageView.getImageView());
        }
    }

    private static RequestListener<String, GlideDrawable> getCallback(final ProgressImageView imageView) {
        return new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.HIDE));
                imageView.hideProgress();
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.HIDE));
                imageView.hideProgress();
                return false;
            }
        };
    }
}
