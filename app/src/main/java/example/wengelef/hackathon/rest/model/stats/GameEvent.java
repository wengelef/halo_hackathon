
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GameEvent {

    @SerializedName("Assistants")
    @Expose
    private List<Assistant> Assistants = new ArrayList<Assistant>();
    @SerializedName("DeathDisposition")
    @Expose
    private String DeathDisposition;
    @SerializedName("IsAssassination")
    @Expose
    private String IsAssassination;
    @SerializedName("IsGroundPound")
    @Expose
    private String IsGroundPound;
    @SerializedName("IsHeadshot")
    @Expose
    private String IsHeadshot;
    @SerializedName("IsMelee")
    @Expose
    private String IsMelee;
    @SerializedName("IsShoulderBash")
    @Expose
    private String IsShoulderBash;
    @SerializedName("IsWeapon")
    @Expose
    private String IsWeapon;
    @SerializedName("Killer")
    @Expose
    private Killer Killer;
    @SerializedName("KillerAgent")
    @Expose
    private Integer KillerAgent;
    @SerializedName("KillerWeaponAttachmentIds")
    @Expose
    private List<Long> KillerWeaponAttachmentIds = new ArrayList<Long>();
    @SerializedName("KillerWeaponStockId")
    @Expose
    private Long KillerWeaponStockId;
    @SerializedName("KillerWorldLocation")
    @Expose
    private KillerWorldLocation KillerWorldLocation;
    @SerializedName("Victim")
    @Expose
    private Victim Victim;
    @SerializedName("VictimAgent")
    @Expose
    private Integer VictimAgent;
    @SerializedName("VictimAttachmentIds")
    @Expose
    private List<Long> VictimAttachmentIds = new ArrayList<Long>();
    @SerializedName("VictimStockId")
    @Expose
    private Long VictimStockId;
    @SerializedName("VictimWorldLocation")
    @Expose
    private VictimWorldLocation VictimWorldLocation;
    @SerializedName("EventName")
    @Expose
    private String EventName;
    @SerializedName("TimeSinceStart")
    @Expose
    private String TimeSinceStart;

    /**
     *
     * @return
     *     The Assistants
     */
    public List<Assistant> getAssistants() {
        return Assistants;
    }

    /**
     *
     * @param Assistants
     *     The Assistants
     */
    public void setAssistants(List<Assistant> Assistants) {
        this.Assistants = Assistants;
    }

    /**
     *
     * @return
     *     The DeathDisposition
     */
    public String getDeathDisposition() {
        return DeathDisposition;
    }

    /**
     *
     * @param DeathDisposition
     *     The DeathDisposition
     */
    public void setDeathDisposition(String DeathDisposition) {
        this.DeathDisposition = DeathDisposition;
    }

    /**
     *
     * @return
     *     The IsAssassination
     */
    public String getIsAssassination() {
        return IsAssassination;
    }

    /**
     *
     * @param IsAssassination
     *     The IsAssassination
     */
    public void setIsAssassination(String IsAssassination) {
        this.IsAssassination = IsAssassination;
    }

    /**
     *
     * @return
     *     The IsGroundPound
     */
    public String getIsGroundPound() {
        return IsGroundPound;
    }

    /**
     *
     * @param IsGroundPound
     *     The IsGroundPound
     */
    public void setIsGroundPound(String IsGroundPound) {
        this.IsGroundPound = IsGroundPound;
    }

    /**
     *
     * @return
     *     The IsHeadshot
     */
    public String getIsHeadshot() {
        return IsHeadshot;
    }

    /**
     *
     * @param IsHeadshot
     *     The IsHeadshot
     */
    public void setIsHeadshot(String IsHeadshot) {
        this.IsHeadshot = IsHeadshot;
    }

    /**
     *
     * @return
     *     The IsMelee
     */
    public String getIsMelee() {
        return IsMelee;
    }

    /**
     *
     * @param IsMelee
     *     The IsMelee
     */
    public void setIsMelee(String IsMelee) {
        this.IsMelee = IsMelee;
    }

    /**
     *
     * @return
     *     The IsShoulderBash
     */
    public String getIsShoulderBash() {
        return IsShoulderBash;
    }

    /**
     *
     * @param IsShoulderBash
     *     The IsShoulderBash
     */
    public void setIsShoulderBash(String IsShoulderBash) {
        this.IsShoulderBash = IsShoulderBash;
    }

    /**
     *
     * @return
     *     The IsWeapon
     */
    public String getIsWeapon() {
        return IsWeapon;
    }

    /**
     *
     * @param IsWeapon
     *     The IsWeapon
     */
    public void setIsWeapon(String IsWeapon) {
        this.IsWeapon = IsWeapon;
    }

    /**
     *
     * @return
     *     The Killer
     */
    public Killer getKiller() {
        return Killer;
    }

    /**
     *
     * @param Killer
     *     The Killer
     */
    public void setKiller(Killer Killer) {
        this.Killer = Killer;
    }

    /**
     *
     * @return
     *     The KillerAgent
     */
    public Integer getKillerAgent() {
        return KillerAgent;
    }

    /**
     *
     * @param KillerAgent
     *     The KillerAgent
     */
    public void setKillerAgent(Integer KillerAgent) {
        this.KillerAgent = KillerAgent;
    }

    /**
     *
     * @return
     *     The KillerWeaponAttachmentIds
     */
    public List<Long> getKillerWeaponAttachmentIds() {
        return KillerWeaponAttachmentIds;
    }

    /**
     *
     * @param KillerWeaponAttachmentIds
     *     The KillerWeaponAttachmentIds
     */
    public void setKillerWeaponAttachmentIds(List<Long> KillerWeaponAttachmentIds) {
        this.KillerWeaponAttachmentIds = KillerWeaponAttachmentIds;
    }

    /**
     *
     * @return
     *     The KillerWeaponStockId
     */
    public Long getKillerWeaponStockId() {
        return KillerWeaponStockId;
    }

    /**
     *
     * @param KillerWeaponStockId
     *     The KillerWeaponStockId
     */
    public void setKillerWeaponStockId(Long KillerWeaponStockId) {
        this.KillerWeaponStockId = KillerWeaponStockId;
    }

    /**
     *
     * @return
     *     The KillerWorldLocation
     */
    public KillerWorldLocation getKillerWorldLocation() {
        return KillerWorldLocation;
    }

    /**
     *
     * @param KillerWorldLocation
     *     The KillerWorldLocation
     */
    public void setKillerWorldLocation(KillerWorldLocation KillerWorldLocation) {
        this.KillerWorldLocation = KillerWorldLocation;
    }

    /**
     *
     * @return
     *     The Victim
     */
    public Victim getVictim() {
        return Victim;
    }

    /**
     *
     * @param Victim
     *     The Victim
     */
    public void setVictim(Victim Victim) {
        this.Victim = Victim;
    }

    /**
     *
     * @return
     *     The VictimAgent
     */
    public Integer getVictimAgent() {
        return VictimAgent;
    }

    /**
     *
     * @param VictimAgent
     *     The VictimAgent
     */
    public void setVictimAgent(Integer VictimAgent) {
        this.VictimAgent = VictimAgent;
    }

    /**
     *
     * @return
     *     The VictimAttachmentIds
     */
    public List<Long> getVictimAttachmentIds() {
        return VictimAttachmentIds;
    }

    /**
     *
     * @param VictimAttachmentIds
     *     The VictimAttachmentIds
     */
    public void setVictimAttachmentIds(List<Long> VictimAttachmentIds) {
        this.VictimAttachmentIds = VictimAttachmentIds;
    }

    /**
     *
     * @return
     *     The VictimStockId
     */
    public Long getVictimStockId() {
        return VictimStockId;
    }

    /**
     *
     * @param VictimStockId
     *     The VictimStockId
     */
    public void setVictimStockId(Long VictimStockId) {
        this.VictimStockId = VictimStockId;
    }

    /**
     *
     * @return
     *     The VictimWorldLocation
     */
    public VictimWorldLocation getVictimWorldLocation() {
        return VictimWorldLocation;
    }

    /**
     *
     * @param VictimWorldLocation
     *     The VictimWorldLocation
     */
    public void setVictimWorldLocation(VictimWorldLocation VictimWorldLocation) {
        this.VictimWorldLocation = VictimWorldLocation;
    }

    /**
     * 
     * @return
     *     The EventName
     */
    public String getEventName() {
        return EventName;
    }

    /**
     * 
     * @param EventName
     *     The EventName
     */
    public void setEventName(String EventName) {
        this.EventName = EventName;
    }

    /**
     * 
     * @return
     *     The TimeSinceStart
     */
    public String getTimeSinceStart() {
        return TimeSinceStart;
    }

    /**
     * 
     * @param TimeSinceStart
     *     The TimeSinceStart
     */
    public void setTimeSinceStart(String TimeSinceStart) {
        this.TimeSinceStart = TimeSinceStart;
    }

}
