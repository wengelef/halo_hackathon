
package example.wengelef.hackathon.rest.model.stats.servicerecordcampaign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import example.wengelef.hackathon.rest.model.stats.PlayerResult;


public class ServiceRecordResult implements Serializable {

    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("ResultCode")
    @Expose
    private Integer ResultCode;
    @SerializedName("Result")
    @Expose
    private PlayerResult Result;

    /**
     * 
     * @return
     *     The Id
     */
    public String getId() {
        return Id;
    }

    /**
     * 
     * @param Id
     *     The Id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * 
     * @return
     *     The ResultCode
     */
    public Integer getResultCode() {
        return ResultCode;
    }

    /**
     * 
     * @param ResultCode
     *     The ResultCode
     */
    public void setResultCode(Integer ResultCode) {
        this.ResultCode = ResultCode;
    }

    /**
     * 
     * @return
     *     The Result
     */
    public PlayerResult getResult() {
        return Result;
    }

    /**
     * 
     * @param Result
     *     The Result
     */
    public void setResult(PlayerResult Result) {
        this.Result = Result;
    }

}
