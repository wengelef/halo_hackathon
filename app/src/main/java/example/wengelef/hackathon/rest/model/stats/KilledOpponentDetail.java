
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KilledOpponentDetail {

    @SerializedName("GamerTag")
    @Expose
    private String GamerTag;
    @SerializedName("TotalKills")
    @Expose
    private Integer TotalKills;

    /**
     * 
     * @return
     *     The GamerTag
     */
    public String getGamerTag() {
        return GamerTag;
    }

    /**
     * 
     * @param GamerTag
     *     The GamerTag
     */
    public void setGamerTag(String GamerTag) {
        this.GamerTag = GamerTag;
    }

    /**
     * 
     * @return
     *     The TotalKills
     */
    public Integer getTotalKills() {
        return TotalKills;
    }

    /**
     * 
     * @param TotalKills
     *     The TotalKills
     */
    public void setTotalKills(Integer TotalKills) {
        this.TotalKills = TotalKills;
    }

}
