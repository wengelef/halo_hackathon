
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FlexibleStats {

    @SerializedName("MedalStatCounts")
    @Expose
    private List<MedalStatCount> MedalStatCounts = new ArrayList<MedalStatCount>();
    @SerializedName("ImpulseStatCounts")
    @Expose
    private List<ImpulseStatCount> ImpulseStatCounts = new ArrayList<ImpulseStatCount>();
    @SerializedName("MedalTimelapses")
    @Expose
    private List<MedalTimelapse> MedalTimelapses = new ArrayList<MedalTimelapse>();
    @SerializedName("ImpulseTimelapses")
    @Expose
    private List<ImpulseTimelapse> ImpulseTimelapses = new ArrayList<ImpulseTimelapse>();

    /**
     * 
     * @return
     *     The MedalStatCounts
     */
    public List<MedalStatCount> getMedalStatCounts() {
        return MedalStatCounts;
    }

    /**
     * 
     * @param MedalStatCounts
     *     The MedalStatCounts
     */
    public void setMedalStatCounts(List<MedalStatCount> MedalStatCounts) {
        this.MedalStatCounts = MedalStatCounts;
    }

    /**
     * 
     * @return
     *     The ImpulseStatCounts
     */
    public List<ImpulseStatCount> getImpulseStatCounts() {
        return ImpulseStatCounts;
    }

    /**
     * 
     * @param ImpulseStatCounts
     *     The ImpulseStatCounts
     */
    public void setImpulseStatCounts(List<ImpulseStatCount> ImpulseStatCounts) {
        this.ImpulseStatCounts = ImpulseStatCounts;
    }

    /**
     * 
     * @return
     *     The MedalTimelapses
     */
    public List<MedalTimelapse> getMedalTimelapses() {
        return MedalTimelapses;
    }

    /**
     * 
     * @param MedalTimelapses
     *     The MedalTimelapses
     */
    public void setMedalTimelapses(List<MedalTimelapse> MedalTimelapses) {
        this.MedalTimelapses = MedalTimelapses;
    }

    /**
     * 
     * @return
     *     The ImpulseTimelapses
     */
    public List<ImpulseTimelapse> getImpulseTimelapses() {
        return ImpulseTimelapses;
    }

    /**
     * 
     * @param ImpulseTimelapses
     *     The ImpulseTimelapses
     */
    public void setImpulseTimelapses(List<ImpulseTimelapse> ImpulseTimelapses) {
        this.ImpulseTimelapses = ImpulseTimelapses;
    }

}
