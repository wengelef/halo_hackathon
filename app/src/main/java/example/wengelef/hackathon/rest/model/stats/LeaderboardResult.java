
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class LeaderboardResult extends RealmObject {

    @SerializedName("Player")
    @Expose
    private PlayerId PlayerId;
    @SerializedName("Rank")
    @Expose
    private Integer Rank;
    @SerializedName("Score")
    @Expose
    private Score Score;

    /**
     * 
     * @return
     *     The PlayerId
     */
    public PlayerId getPlayerId() {
        return PlayerId;
    }

    /**
     * 
     * @param PlayerId
     *     The PlayerId
     */
    public void setPlayerId(PlayerId PlayerId) {
        this.PlayerId = PlayerId;
    }

    /**
     * 
     * @return
     *     The Rank
     */
    public Integer getRank() {
        return Rank;
    }

    /**
     * 
     * @param Rank
     *     The Rank
     */
    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

    /**
     * 
     * @return
     *     The Score
     */
    public Score getScore() {
        return Score;
    }

    /**
     * 
     * @param Score
     *     The Score
     */
    public void setScore(Score Score) {
        this.Score = Score;
    }

}
