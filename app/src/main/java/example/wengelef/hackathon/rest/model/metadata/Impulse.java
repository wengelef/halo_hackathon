
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Impulse {

    @SerializedName("internalName")
    @Expose
    private String internalName;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The internalName
     */
    public String getInternalName() {
        return internalName;
    }

    /**
     * 
     * @param internalName
     *     The internalName
     */
    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
