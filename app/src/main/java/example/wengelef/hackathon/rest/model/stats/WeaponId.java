
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WeaponId {

    @SerializedName("StockId")
    @Expose
    private Long StockId;
    @SerializedName("Attachments")
    @Expose
    private List<Integer> Attachments = new ArrayList<Integer>();

    /**
     * 
     * @return
     *     The StockId
     */
    public Long getStockId() {
        return StockId;
    }

    /**
     * 
     * @param StockId
     *     The StockId
     */
    public void setStockId(Long StockId) {
        this.StockId = StockId;
    }

    /**
     * 
     * @return
     *     The Attachments
     */
    public List<Integer> getAttachments() {
        return Attachments;
    }

    /**
     * 
     * @param Attachments
     *     The Attachments
     */
    public void setAttachments(List<Integer> Attachments) {
        this.Attachments = Attachments;
    }

}
