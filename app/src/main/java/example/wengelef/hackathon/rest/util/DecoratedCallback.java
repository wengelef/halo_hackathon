package example.wengelef.hackathon.rest.util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * DecoratedCallback
 *
 * Decorates Retrofit 2.0 Callback with standard Ops like Stop Progress, Log outputs
 */
public final class DecoratedCallback<T> implements Callback<T> {

    private final Callback<T> mCallback;

    public DecoratedCallback(Callback<T> callback) {
        mCallback = callback;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        mCallback.onResponse(call, response);
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Timber.i("onFailure: %s", t.getCause());
        mCallback.onFailure(call, t);
    }
}
