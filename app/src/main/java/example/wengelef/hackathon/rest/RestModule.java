package example.wengelef.hackathon.rest;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import example.wengelef.hackathon.rest.api.DecoratedMetadataInterface;
import example.wengelef.hackathon.rest.api.DecoratedProfileInterface;
import example.wengelef.hackathon.rest.api.DecoratedStatsInterface;
import example.wengelef.hackathon.rest.api.RestMetadataInterface;
import example.wengelef.hackathon.rest.api.RestProfileInterface;
import example.wengelef.hackathon.rest.api.RestStatsInterface;
import example.wengelef.hackathon.rest.util.HeaderInterceptor;
import example.wengelef.hackathon.rest.util.LoggingInterceptor;
import example.wengelef.hackathon.rest.util.RealmStringSerializer;
import example.wengelef.hackathon.util.RealmString;
import io.realm.RealmList;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fwengelewski on 4/5/16.
 *
 *
 * RestConfig
 *
 * Static Rest Configuration
 */
@Singleton
@Module
public class RestModule {

    /**
     * @return Request Url as String
     */
    private static String getRequestUrl() {
        return "https://www.haloapi.com";
    }

    /**
     * @return The Title ID. This should always be "h5".
     */
    public static String getTitle() {
        return "h5";
    }

    /**
     * Api Subscription Key, should be in every Request Header
     * @return Api Subscription Key
     */
    private static String getApiSubscriptionKeyPrimary() {
        return "953f654703fe46fface8edbb54f6a861";
    }

    /**
     * No idea what this is for
     * @return i dont know!
     */
    static String getApiSubscriptionKeySecondary() {
        return "cc729f618f62419092217c7bf600eb3e";
    }

    private static String getDateFormat() {
        return "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'";
    }

    @Provides @NonNull @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(getRequestUrl())
                .client(provideHttpClient())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .build();
    }

    @Provides @NonNull @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat(getDateFormat())
                .registerTypeAdapter(new TypeToken<RealmList<RealmString>>(){}.getType(), new RealmStringSerializer())
                .create();
    }

    @Provides @NonNull @Singleton
    OkHttpClient provideHttpClient() {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new HeaderInterceptor("Ocp-Apim-Subscription-Key", getApiSubscriptionKeyPrimary()))
                .addInterceptor(new LoggingInterceptor())
                .followRedirects(false)
                .build();
    }

    @Provides @NonNull @Singleton
    RestMetadataInterface provideMetadataInterface() {
        return new DecoratedMetadataInterface(provideRetrofit().create(RestMetadataInterface.class));
    }

    @Provides @NonNull @Singleton
    RestProfileInterface provideProfileInterface() {
        return new DecoratedProfileInterface(provideRetrofit().create(RestProfileInterface.class));
    }

    @Provides @NonNull @Singleton
    RestStatsInterface provideStatsInterface() {
        return new DecoratedStatsInterface(provideRetrofit().create(RestStatsInterface.class));
    }
}