package example.wengelef.hackathon.rest.controller;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import example.wengelef.hackathon.RxUtil.RedirectObserver;
import example.wengelef.hackathon.rest.RestModule;
import example.wengelef.hackathon.rest.api.RestProfileInterface;
import example.wengelef.hackathon.rest.util.DecoratedCallback;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.rest.util.RedirectHelper;
import example.wengelef.hackathon.ui.widget.ProgressImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/7/16.
 *
 *
 * RestProfileController
 *
 * RestController for Profile Images
 */
public class RestProfileController extends RestController {

    private final @NonNull RestProfileInterface mProfileInterface;

    @Inject
    public RestProfileController(@NonNull RestProfileInterface restProfileInterface) {
        mProfileInterface = restProfileInterface;
    }

    public Observable<Void> getEmblemImage(String player, @RestProfileInterface.EmblemSize Integer size) {
        return mProfileInterface.getEmblemImage(RestModule.getTitle(), player, size);
    }

    public Observable<Void> getSpartanImage(String player, @RestProfileInterface.EmblemSize Integer size, @RestProfileInterface.CropMode String cropMode) {
        return mProfileInterface.getSpartanImage(RestModule.getTitle(), player, size, cropMode);
    }

    /**
     * @deprecated replaced by {@link RedirectObserver}
     * @param imageView
     * @return
     */
    @SuppressWarnings("unused")
    public Callback<Void> getCallback(@NonNull final ProgressImageView imageView) {
        return new DecoratedCallback<>(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                ImageLoader.loadFittedImage(RedirectHelper.getRedirectUrl(response), imageView);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (t.getCause() != null) {
                    Timber.e("onFailure: %s", t.getCause().toString());
                }
            }
        });
    }
}
