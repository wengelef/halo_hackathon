
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Commendation extends RealmObject {

    @SerializedName("type")
    @Expose
    private String type;
    @PrimaryKey
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("iconImageUrl")
    @Expose
    private String iconImageUrl;
    @SerializedName("levels")
    @Expose
    private RealmList<Level> levels = new RealmList<>();
    @SerializedName("requiredLevels")
    @Expose
    private RealmList<RequiredLevel> requiredLevels = new RealmList<>();
    @SerializedName("reward")
    @Expose
    private Reward reward;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("enabled")
    @Expose
    private String enabled;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The iconImageUrl
     */
    public String getIconImageUrl() {
        return iconImageUrl;
    }

    /**
     * 
     * @param iconImageUrl
     *     The iconImageUrl
     */
    public void setIconImageUrl(String iconImageUrl) {
        this.iconImageUrl = iconImageUrl;
    }

    /**
     * 
     * @return
     *     The levels
     */
    public List<Level> getLevels() {
        return levels;
    }

    /**
     * 
     * @param levels
     *     The levels
     */
    public void setLevels(RealmList<Level> levels) {
        this.levels = levels;
    }

    /**
     * 
     * @return
     *     The requiredLevels
     */
    public List<RequiredLevel> getRequiredLevels() {
        return requiredLevels;
    }

    /**
     * 
     * @param requiredLevels
     *     The requiredLevels
     */
    public void setRequiredLevels(RealmList<RequiredLevel> requiredLevels) {
        this.requiredLevels = requiredLevels;
    }

    /**
     * 
     * @return
     *     The reward
     */
    public Reward getReward() {
        return reward;
    }

    /**
     * 
     * @param reward
     *     The reward
     */
    public void setReward(Reward reward) {
        this.reward = reward;
    }

    /**
     * 
     * @return
     *     The category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The enabled
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * 
     * @param enabled
     *     The enabled
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
