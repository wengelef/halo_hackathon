package example.wengelef.hackathon.rest.api;

import java.util.List;

import example.wengelef.hackathon.rest.model.metadata.CampaignMission;
import example.wengelef.hackathon.rest.model.metadata.Commendation;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.metadata.Enemy;
import example.wengelef.hackathon.rest.model.metadata.FlexibleStat;
import example.wengelef.hackathon.rest.model.metadata.GameBaseVariant;
import example.wengelef.hackathon.rest.model.metadata.GameVariantForId;
import example.wengelef.hackathon.rest.model.metadata.Impulse;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.rest.model.metadata.MapDetail;
import example.wengelef.hackathon.rest.model.metadata.Medal;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.metadata.Requisition;
import example.wengelef.hackathon.rest.model.metadata.RequisitionPack;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.rest.model.metadata.Skull;
import example.wengelef.hackathon.rest.model.metadata.SpartanRank;
import example.wengelef.hackathon.rest.model.metadata.TeamColor;
import example.wengelef.hackathon.rest.model.metadata.Vehicle;
import example.wengelef.hackathon.rest.model.metadata.Weapon;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by fwengelewski on 4/5/16.
 *
 *
 * RestMetadataInterface
 *
 * Rest Interface for Metadata
 */
public interface RestMetadataInterface {
    @GET("metadata/{title}/metadata/campaign-missions")
    Observable<List<CampaignMission>> getCampaignMissions(@Path("title") String title);

    @GET("metadata/{title}/metadata/commendations")
    Observable<List<Commendation>> getCommendations(@Path("title") String title);

    @GET("metadata/{title}/metadata/csr-designations")
    Observable<List<CsrDesignation>> getCsrDesignations(@Path("title") String title);

    @GET("metadata/{title}/metadata/enemies")
    Observable<List<Enemy>> getEnemies(@Path("title") String title);

    @GET("metadata/{title}/metadata/flexible-stats")
    Observable<List<FlexibleStat>> getFlexibleStats(@Path("title") String title);

    @GET("metadata/{title}/metadata/game-base-variants")
    Observable<List<GameBaseVariant>> getGameBaseVariants(@Path("title") String title);

    @GET("metadata/{title}/metadata/game-variants/{id}")
    Observable<GameVariantForId> getGameVariant(@Path("title") String title, @Path("id") String id);

    @GET("metadata/{title}/metadata/impulses")
    Observable<List<Impulse>> getImpulses(@Path("title") String title);

    @GET("metadata/{title}/metadata/maps")
    Observable<List<MapData>> getMaps(@Path("title") String title);

    @GET("metadata/{title}/metadata/map-variants/{id}")
    Observable<MapDetail> getMapDetail(@Path("title") String title, @Path("id") String id);

    @GET("metadata/{title}/metadata/medals")
    Observable<List<Medal>> getMedals(@Path("title") String title);

    @GET("metadata/{title}/metadata/playlists")
    Observable<List<Playlist>> getPlaylists(@Path("title") String title);

    @GET("metadata/{title}/metadata/requisition-packs/{id}")
    Observable<RequisitionPack> getReqPack(@Path("title") String title, @Path("id") String id);

    @GET("metadata/{title}/metadata/requisitions/{id}")
    Observable<Requisition> getRequisition(@Path("title") String title, @Path("id") String id);

    @GET("metadata/{title}/metadata/seasons")
    Observable<List<Season>> getSeasons(@Path("title") String title);

    @GET("metadata/{title}/metadata/skulls")
    Observable<List<Skull>> getSkulls(@Path("title") String title);

    @GET("metadata/{title}/metadata/spartan-ranks")
    Observable<List<SpartanRank>> getSpartanRanks(@Path("title") String title);

    @GET("metadata/{title}/metadata/team-colors")
    Observable<List<TeamColor>> getTeamColors(@Path("title") String title);

    @GET("metadata/{title}/metadata/vehicles")
    Observable<List<Vehicle>> getVehicles(@Path("title") String title);

    @GET("metadata/{title}/metadata/weapons")
    Observable<List<Weapon>> getWeapons(@Path("title") String title);

    void showProgress();
}
