
package example.wengelef.hackathon.rest.model.stats;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class XpInfo {

    @SerializedName("PrevSpartanRank")
    @Expose
    private Integer PrevSpartanRank;
    @SerializedName("SpartanRank")
    @Expose
    private Integer SpartanRank;
    @SerializedName("PrevTotalXP")
    @Expose
    private Integer PrevTotalXP;
    @SerializedName("TotalXP")
    @Expose
    private Integer TotalXP;
    @SerializedName("SpartanRankMatchXPScalar")
    @Expose
    private Double SpartanRankMatchXPScalar;
    @SerializedName("PlayerTimePerformanceXPAward")
    @Expose
    private Integer PlayerTimePerformanceXPAward;
    @SerializedName("PerformanceXP")
    @Expose
    private Integer PerformanceXP;
    @SerializedName("PlayerRankXPAward")
    @Expose
    private Integer PlayerRankXPAward;
    @SerializedName("BoostAmount")
    @Expose
    private Integer BoostAmount;

    /**
     * 
     * @return
     *     The PrevSpartanRank
     */
    public Integer getPrevSpartanRank() {
        return PrevSpartanRank;
    }

    /**
     * 
     * @param PrevSpartanRank
     *     The PrevSpartanRank
     */
    public void setPrevSpartanRank(Integer PrevSpartanRank) {
        this.PrevSpartanRank = PrevSpartanRank;
    }

    /**
     * 
     * @return
     *     The SpartanRank
     */
    public Integer getSpartanRank() {
        return SpartanRank;
    }

    /**
     * 
     * @param SpartanRank
     *     The SpartanRank
     */
    public void setSpartanRank(Integer SpartanRank) {
        this.SpartanRank = SpartanRank;
    }

    /**
     * 
     * @return
     *     The PrevTotalXP
     */
    public Integer getPrevTotalXP() {
        return PrevTotalXP;
    }

    /**
     * 
     * @param PrevTotalXP
     *     The PrevTotalXP
     */
    public void setPrevTotalXP(Integer PrevTotalXP) {
        this.PrevTotalXP = PrevTotalXP;
    }

    /**
     * 
     * @return
     *     The TotalXP
     */
    public Integer getTotalXP() {
        return TotalXP;
    }

    /**
     * 
     * @param TotalXP
     *     The TotalXP
     */
    public void setTotalXP(Integer TotalXP) {
        this.TotalXP = TotalXP;
    }

    /**
     * 
     * @return
     *     The SpartanRankMatchXPScalar
     */
    public Double getSpartanRankMatchXPScalar() {
        return SpartanRankMatchXPScalar;
    }

    /**
     * 
     * @param SpartanRankMatchXPScalar
     *     The SpartanRankMatchXPScalar
     */
    public void setSpartanRankMatchXPScalar(Double SpartanRankMatchXPScalar) {
        this.SpartanRankMatchXPScalar = SpartanRankMatchXPScalar;
    }

    /**
     * 
     * @return
     *     The PlayerTimePerformanceXPAward
     */
    public Integer getPlayerTimePerformanceXPAward() {
        return PlayerTimePerformanceXPAward;
    }

    /**
     * 
     * @param PlayerTimePerformanceXPAward
     *     The PlayerTimePerformanceXPAward
     */
    public void setPlayerTimePerformanceXPAward(Integer PlayerTimePerformanceXPAward) {
        this.PlayerTimePerformanceXPAward = PlayerTimePerformanceXPAward;
    }

    /**
     * 
     * @return
     *     The PerformanceXP
     */
    public Integer getPerformanceXP() {
        return PerformanceXP;
    }

    /**
     * 
     * @param PerformanceXP
     *     The PerformanceXP
     */
    public void setPerformanceXP(Integer PerformanceXP) {
        this.PerformanceXP = PerformanceXP;
    }

    /**
     * 
     * @return
     *     The PlayerRankXPAward
     */
    public Integer getPlayerRankXPAward() {
        return PlayerRankXPAward;
    }

    /**
     * 
     * @param PlayerRankXPAward
     *     The PlayerRankXPAward
     */
    public void setPlayerRankXPAward(Integer PlayerRankXPAward) {
        this.PlayerRankXPAward = PlayerRankXPAward;
    }

    /**
     * 
     * @return
     *     The BoostAmount
     */
    public Integer getBoostAmount() {
        return BoostAmount;
    }

    /**
     * 
     * @param BoostAmount
     *     The BoostAmount
     */
    public void setBoostAmount(Integer BoostAmount) {
        this.BoostAmount = BoostAmount;
    }

}
