
package example.wengelef.hackathon.rest.model.stats.servicerecordcampaign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CoopStats {

    @SerializedName("Stat")
    @Expose
    private example.wengelef.hackathon.rest.model.stats.servicerecordcampaign.Statistic Statistic;

    /**
     * 
     * @return
     *     The Stat
     */
    public Statistic getStatistic() {
        return Statistic;
    }

    /**
     * 
     * @param Statistic
     *     The Stat
     */
    public void setStatistic(Statistic Statistic) {
        this.Statistic = Statistic;
    }

}
