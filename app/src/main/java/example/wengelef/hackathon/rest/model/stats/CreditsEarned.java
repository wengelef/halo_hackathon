
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreditsEarned {

    @SerializedName("Result")
    @Expose
    private Integer Result;
    @SerializedName("TotalCreditsEarned")
    @Expose
    private Integer TotalCreditsEarned;
    @SerializedName("SpartanRankModifier")
    @Expose
    private Double SpartanRankModifier;
    @SerializedName("PlayerRankAmount")
    @Expose
    private Integer PlayerRankAmount;
    @SerializedName("TimePlayedAmount")
    @Expose
    private Double TimePlayedAmount;
    @SerializedName("BoostAmount")
    @Expose
    private Integer BoostAmount;

    /**
     * 
     * @return
     *     The Result
     */
    public Integer getResult() {
        return Result;
    }

    /**
     * 
     * @param Result
     *     The Result
     */
    public void setResult(Integer Result) {
        this.Result = Result;
    }

    /**
     * 
     * @return
     *     The TotalCreditsEarned
     */
    public Integer getTotalCreditsEarned() {
        return TotalCreditsEarned;
    }

    /**
     * 
     * @param TotalCreditsEarned
     *     The TotalCreditsEarned
     */
    public void setTotalCreditsEarned(Integer TotalCreditsEarned) {
        this.TotalCreditsEarned = TotalCreditsEarned;
    }

    /**
     * 
     * @return
     *     The SpartanRankModifier
     */
    public Double getSpartanRankModifier() {
        return SpartanRankModifier;
    }

    /**
     * 
     * @param SpartanRankModifier
     *     The SpartanRankModifier
     */
    public void setSpartanRankModifier(Double SpartanRankModifier) {
        this.SpartanRankModifier = SpartanRankModifier;
    }

    /**
     * 
     * @return
     *     The PlayerRankAmount
     */
    public Integer getPlayerRankAmount() {
        return PlayerRankAmount;
    }

    /**
     * 
     * @param PlayerRankAmount
     *     The PlayerRankAmount
     */
    public void setPlayerRankAmount(Integer PlayerRankAmount) {
        this.PlayerRankAmount = PlayerRankAmount;
    }

    /**
     * 
     * @return
     *     The TimePlayedAmount
     */
    public Double getTimePlayedAmount() {
        return TimePlayedAmount;
    }

    /**
     * 
     * @param TimePlayedAmount
     *     The TimePlayedAmount
     */
    public void setTimePlayedAmount(Double TimePlayedAmount) {
        this.TimePlayedAmount = TimePlayedAmount;
    }

    /**
     * 
     * @return
     *     The BoostAmount
     */
    public Integer getBoostAmount() {
        return BoostAmount;
    }

    /**
     * 
     * @param BoostAmount
     *     The BoostAmount
     */
    public void setBoostAmount(Integer BoostAmount) {
        this.BoostAmount = BoostAmount;
    }

}
