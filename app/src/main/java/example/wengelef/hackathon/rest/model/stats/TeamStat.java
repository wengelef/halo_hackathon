
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TeamStat {

    @SerializedName("TeamId")
    @Expose
    private Integer TeamId;
    @SerializedName("Score")
    @Expose
    private Integer Score;
    @SerializedName("Rank")
    @Expose
    private Integer Rank;
    @SerializedName("RoundStats")
    @Expose
    private List<RoundStat> RoundStats = new ArrayList<RoundStat>();

    /**
     * 
     * @return
     *     The TeamId
     */
    public Integer getTeamId() {
        return TeamId;
    }

    /**
     * 
     * @param TeamId
     *     The TeamId
     */
    public void setTeamId(Integer TeamId) {
        this.TeamId = TeamId;
    }

    /**
     * 
     * @return
     *     The Score
     */
    public Integer getScore() {
        return Score;
    }

    /**
     * 
     * @param Score
     *     The Score
     */
    public void setScore(Integer Score) {
        this.Score = Score;
    }

    /**
     * 
     * @return
     *     The Rank
     */
    public Integer getRank() {
        return Rank;
    }

    /**
     * 
     * @param Rank
     *     The Rank
     */
    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

    /**
     * 
     * @return
     *     The RoundStats
     */
    public List<RoundStat> getRoundStats() {
        return RoundStats;
    }

    /**
     * 
     * @param RoundStats
     *     The RoundStats
     */
    public void setRoundStats(List<RoundStat> RoundStats) {
        this.RoundStats = RoundStats;
    }

}
