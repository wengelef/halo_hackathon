
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Leaderboards extends RealmObject {

    @PrimaryKey
    @SerializedName("Start")
    @Expose
    private Integer Start;
    @SerializedName("Count")
    @Expose
    private Integer Count;
    @SerializedName("ResultCount")
    @Expose
    private Integer ResultCount;
    @SerializedName("Results")
    @Expose
    private RealmList<LeaderboardResult> LeaderboardResults;

    /**
     * 
     * @return
     *     The Start
     */
    public Integer getStart() {
        return Start;
    }

    /**
     * 
     * @param Start
     *     The Start
     */
    public void setStart(Integer Start) {
        this.Start = Start;
    }

    /**
     * 
     * @return
     *     The Count
     */
    public Integer getCount() {
        return Count;
    }

    /**
     * 
     * @param Count
     *     The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

    /**
     * 
     * @return
     *     The ResultCount
     */
    public Integer getResultCount() {
        return ResultCount;
    }

    /**
     * 
     * @param ResultCount
     *     The ResultCount
     */
    public void setResultCount(Integer ResultCount) {
        this.ResultCount = ResultCount;
    }

    /**
     * 
     * @return
     *     The LeaderboardResults
     */
    public RealmList<LeaderboardResult> getLeaderboardResults() {
        return LeaderboardResults;
    }

    /**
     * 
     * @param LeaderboardResults
     *     The LeaderboardResults
     */
    public void setLeaderboardResults(RealmList<LeaderboardResult> LeaderboardResults) {
        this.LeaderboardResults = LeaderboardResults;
    }

}
