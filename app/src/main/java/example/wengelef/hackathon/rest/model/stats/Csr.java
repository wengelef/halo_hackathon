
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Csr {

    @SerializedName("Tier")
    @Expose
    private Integer Tier;
    @SerializedName("DesignationId")
    @Expose
    private Integer DesignationId;
    @SerializedName("Csr")
    @Expose
    private Integer Csr;
    @SerializedName("PercentToNextTier")
    @Expose
    private Integer PercentToNextTier;
    @SerializedName("Rank")
    @Expose
    private Integer Rank;

    /**
     * 
     * @return
     *     The Tier
     */
    public Integer getTier() {
        return Tier;
    }

    /**
     * 
     * @param Tier
     *     The Tier
     */
    public void setTier(Integer Tier) {
        this.Tier = Tier;
    }

    /**
     * 
     * @return
     *     The DesignationId
     */
    public Integer getDesignationId() {
        return DesignationId;
    }

    /**
     * 
     * @param DesignationId
     *     The DesignationId
     */
    public void setDesignationId(Integer DesignationId) {
        this.DesignationId = DesignationId;
    }

    /**
     * 
     * @return
     *     The Csr
     */
    public Integer getCsr() {
        return Csr;
    }

    /**
     * 
     * @param Csr
     *     The Csr
     */
    public void setCsr(Integer Csr) {
        this.Csr = Csr;
    }

    /**
     * 
     * @return
     *     The PercentToNextTier
     */
    public Integer getPercentToNextTier() {
        return PercentToNextTier;
    }

    /**
     * 
     * @param PercentToNextTier
     *     The PercentToNextTier
     */
    public void setPercentToNextTier(Integer PercentToNextTier) {
        this.PercentToNextTier = PercentToNextTier;
    }

    /**
     * 
     * @return
     *     The Rank
     */
    public Integer getRank() {
        return Rank;
    }

    /**
     * 
     * @param Rank
     *     The Rank
     */
    public void setRank(Integer Rank) {
        this.Rank = Rank;
    }

}
