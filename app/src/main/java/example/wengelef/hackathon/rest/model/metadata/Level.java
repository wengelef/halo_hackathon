
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Level extends RealmObject {

    @SerializedName("reward")
    @Expose
    private Reward reward;
    @SerializedName("threshold")
    @Expose
    private String threshold;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The reward
     */
    public Reward getReward() {
        return reward;
    }

    /**
     * 
     * @param reward
     *     The reward
     */
    public void setReward(Reward reward) {
        this.reward = reward;
    }

    /**
     * 
     * @return
     *     The threshold
     */
    public String getThreshold() {
        return threshold;
    }

    /**
     * 
     * @param threshold
     *     The threshold
     */
    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
