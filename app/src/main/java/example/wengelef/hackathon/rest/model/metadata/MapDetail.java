
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapDetail {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("mapImageUrl")
    @Expose
    private String mapImageUrl;
    @SerializedName("mapId")
    @Expose
    private String mapId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The mapImageUrl
     */
    public String getMapImageUrl() {
        return mapImageUrl;
    }

    /**
     * 
     * @param mapImageUrl
     *     The mapImageUrl
     */
    public void setMapImageUrl(String mapImageUrl) {
        this.mapImageUrl = mapImageUrl;
    }

    /**
     * 
     * @return
     *     The mapId
     */
    public String getMapId() {
        return mapId;
    }

    /**
     * 
     * @param mapId
     *     The mapId
     */
    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
