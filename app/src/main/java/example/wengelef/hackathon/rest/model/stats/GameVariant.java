
package example.wengelef.hackathon.rest.model.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GameVariant implements Serializable {

    @SerializedName("ResourceType")
    @Expose
    private Integer ResourceType;
    @SerializedName("ResourceId")
    @Expose
    private String ResourceId;
    @SerializedName("OwnerType")
    @Expose
    private Integer OwnerType;
    @SerializedName("Owner")
    @Expose
    private String Owner;

    /**
     * 
     * @return
     *     The ResourceType
     */
    public Integer getResourceType() {
        return ResourceType;
    }

    /**
     * 
     * @param ResourceType
     *     The ResourceType
     */
    public void setResourceType(Integer ResourceType) {
        this.ResourceType = ResourceType;
    }

    /**
     * 
     * @return
     *     The ResourceId
     */
    public String getResourceId() {
        return ResourceId;
    }

    /**
     * 
     * @param ResourceId
     *     The ResourceId
     */
    public void setResourceId(String ResourceId) {
        this.ResourceId = ResourceId;
    }

    /**
     * 
     * @return
     *     The OwnerType
     */
    public Integer getOwnerType() {
        return OwnerType;
    }

    /**
     * 
     * @param OwnerType
     *     The OwnerType
     */
    public void setOwnerType(Integer OwnerType) {
        this.OwnerType = OwnerType;
    }

    /**
     * 
     * @return
     *     The Owner
     */
    public Object getOwner() {
        return Owner;
    }

    /**
     * 
     * @param Owner
     *     The Owner
     */
    public void setOwner(String Owner) {
        this.Owner = Owner;
    }

}
