
package example.wengelef.hackathon.rest.model.stats.servicerecordcampaign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Statistic {

    @SerializedName("HighestScore")
    @Expose
    private Integer HighestScore;
    @SerializedName("FastestCompletionTime")
    @Expose
    private String FastestCompletionTime;
    @SerializedName("Skulls")
    @Expose
    private List<Integer> Skulls = new ArrayList<Integer>();
    @SerializedName("TotalTimesCompleted")
    @Expose
    private Integer TotalTimesCompleted;
    @SerializedName("AllSkullsOn")
    @Expose
    private Boolean AllSkullsOn;

    /**
     * 
     * @return
     *     The HighestScore
     */
    public Integer getHighestScore() {
        return HighestScore;
    }

    /**
     * 
     * @param HighestScore
     *     The HighestScore
     */
    public void setHighestScore(Integer HighestScore) {
        this.HighestScore = HighestScore;
    }

    /**
     * 
     * @return
     *     The FastestCompletionTime
     */
    public String getFastestCompletionTime() {
        return FastestCompletionTime;
    }

    /**
     * 
     * @param FastestCompletionTime
     *     The FastestCompletionTime
     */
    public void setFastestCompletionTime(String FastestCompletionTime) {
        this.FastestCompletionTime = FastestCompletionTime;
    }

    /**
     * 
     * @return
     *     The Skulls
     */
    public List<Integer> getSkulls() {
        return Skulls;
    }

    /**
     * 
     * @param Skulls
     *     The Skulls
     */
    public void setSkulls(List<Integer> Skulls) {
        this.Skulls = Skulls;
    }

    /**
     * 
     * @return
     *     The TotalTimesCompleted
     */
    public Integer getTotalTimesCompleted() {
        return TotalTimesCompleted;
    }

    /**
     * 
     * @param TotalTimesCompleted
     *     The TotalTimesCompleted
     */
    public void setTotalTimesCompleted(Integer TotalTimesCompleted) {
        this.TotalTimesCompleted = TotalTimesCompleted;
    }

    /**
     * 
     * @return
     *     The AllSkullsOn
     */
    public Boolean getAllSkullsOn() {
        return AllSkullsOn;
    }

    /**
     * 
     * @param AllSkullsOn
     *     The AllSkullsOn
     */
    public void setAllSkullsOn(Boolean AllSkullsOn) {
        this.AllSkullsOn = AllSkullsOn;
    }

}
