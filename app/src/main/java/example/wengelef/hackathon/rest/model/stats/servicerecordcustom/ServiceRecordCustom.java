
package example.wengelef.hackathon.rest.model.stats.servicerecordcustom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ServiceRecordCustom {

    @SerializedName("Results")
    @Expose
    private List<ServiceRecordResult> Results = new ArrayList<ServiceRecordResult>();
    @SerializedName("Links")
    @Expose
    private Object Links;

    /**
     * 
     * @return
     *     The Results
     */
    public List<ServiceRecordResult> getResults() {
        return Results;
    }

    /**
     * 
     * @param Results
     *     The Results
     */
    public void setResults(List<ServiceRecordResult> Results) {
        this.Results = Results;
    }

    /**
     * 
     * @return
     *     The Links
     */
    public Object getLinks() {
        return Links;
    }

    /**
     * 
     * @param Links
     *     The Links
     */
    public void setLinks(Object Links) {
        this.Links = Links;
    }

}
