
package example.wengelef.hackathon.rest.model.metadata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CsrDesignation extends RealmObject {

    @PrimaryKey
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bannerImageUrl")
    @Expose
    private String bannerImageUrl;
    @SerializedName("tiers")
    @Expose
    private RealmList<Tier> tiers;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("contentId")
    @Expose
    private String contentId;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The bannerImageUrl
     */
    public String getBannerImageUrl() {
        return bannerImageUrl;
    }

    /**
     * 
     * @param bannerImageUrl
     *     The bannerImageUrl
     */
    public void setBannerImageUrl(String bannerImageUrl) {
        this.bannerImageUrl = bannerImageUrl;
    }

    /**
     * 
     * @return
     *     The tiers
     */
    public RealmList<Tier> getTiers() {
        return tiers;
    }

    /**
     * 
     * @param tiers
     *     The tiers
     */
    public void setTiers(RealmList<Tier> tiers) {
        this.tiers = tiers;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The contentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param contentId
     *     The contentId
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

}
