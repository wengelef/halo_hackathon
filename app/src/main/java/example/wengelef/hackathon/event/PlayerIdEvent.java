package example.wengelef.hackathon.event;

import android.support.annotation.NonNull;

import example.wengelef.hackathon.rest.model.stats.PlayerId;

/**
 * Created by fwengelewski on 5/6/16.
 *
 *
 * PlayerIdEvent
 *
 * Used from the LeaderBoard to Start a Search
 */
public class PlayerIdEvent {

    @NonNull
    private final PlayerId mPlayerId;

    public PlayerIdEvent(@NonNull PlayerId mPlayerId) {
        this.mPlayerId = mPlayerId;
    }

    public PlayerId getPlayerId() {
        return mPlayerId;
    }
}
