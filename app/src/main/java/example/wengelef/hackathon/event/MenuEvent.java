package example.wengelef.hackathon.event;

/**
 * Created by fwengelewski on 6/8/16.
 *
 *
 * MenuEvent
 *
 * Event for Menu Navigation
 */
public class MenuEvent {

    private final int menuId;

    public MenuEvent(int menuId) {
        this.menuId = menuId;
    }

    public int getItemId() {
        return menuId;
    }
}
