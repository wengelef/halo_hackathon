package example.wengelef.hackathon.event;

import android.support.annotation.Nullable;

import example.wengelef.hackathon.ui.SpartanActivity;

/**
 * Created by Flo on 10.04.2016.
 *
 *
 * ProgressEvent
 *
 * EventType to show or hide the ProgressBar
 *
 * {@link SpartanActivity}
 */
public class ProgressEvent {
    public enum Type {
        SHOW, HIDE
    }

    public Type mType;

    @Nullable
    public Throwable mError = null;

    public ProgressEvent(Type type) {
        mType = type;
    }

    public ProgressEvent(Type type, @Nullable Throwable error) {
        mType = type;
        mError = error;
    }
}
