package example.wengelef.hackathon.event;

import android.support.annotation.NonNull;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * SearchEvent
 *
 * This Event will be sent when a Player Search is beeing fired
 */
public class SearchEvent {

    @NonNull
    private final String mName;

    public SearchEvent(@NonNull String name) {
        this.mName = name;
    }

    @NonNull
    public String getName() {
        return mName;
    }
}
