package example.wengelef.hackathon.event;

import android.support.annotation.NonNull;

import example.wengelef.hackathon.ui.fragment.BaseSpartanFragment;
import example.wengelef.hackathon.ui.fragment.ViewPagerFragment;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * NavigationEvent
 *
 * Event to dispatch when Fragments are beeing navigated
 */
public class NavigationEvent {

    private ViewPagerFragment mFragment;

    public NavigationEvent(@NonNull ViewPagerFragment fragment) {
        mFragment = fragment;
    }

    @NonNull
    public ViewPagerFragment getFragment() {
        return mFragment;
    }
}
