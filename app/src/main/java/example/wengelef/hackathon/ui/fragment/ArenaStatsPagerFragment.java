package example.wengelef.hackathon.ui.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.f2prateek.dart.InjectExtra;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.Module;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.databinding.FrArenaStatsPagerBinding;
import example.wengelef.hackathon.event.SearchEvent;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ArenaPlaylistStat;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ArenaStats;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ServiceRecordArena;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.ui.viewmodel.ArenaStatsViewModel;
import example.wengelef.hackathon.ui.widget.ProgressImageView;
import io.realm.Realm;
import io.realm.RealmObject;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by fwengelewski on 5/6/16.
 * <p>
 * <p>
 * ArenaStatsPagerFragment
 * <p>
 * Display ArenaStats for a Season
 */
@Module
public class ArenaStatsPagerFragment extends BaseSpartanFragment {

    public static final String KEY_SEASON_NAME = "key_season_name";

    @BindView(R.id.csr_des_background)
    ProgressImageView mCsrDesBackground;

    @BindView(R.id.csr_des_icon)
    ProgressImageView mCsrDesIcon;

    @BindView(R.id.arena_weapon_most_kills_icon)
    ProgressImageView mArenaWeaponMostKillsIcon;

    @BindView(R.id.playlist_spinner)
    AppCompatSpinner mSpinner;

    @InjectExtra(KEY_SEASON_NAME)
    String mSeasonName;

    @Inject
    RestStatsController mRestStatsController;

    private ArenaStats mArenaStatsForSeason;

    private ArenaStatsViewModel mArenaStatsViewModel = new ArenaStatsViewModel();

    public static ArenaStatsPagerFragment newInstance(@NonNull Season season) {
        Bundle args = new Bundle(1);
        args.putString(KEY_SEASON_NAME, season.getName());
        ArenaStatsPagerFragment fragment = new ArenaStatsPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FrArenaStatsPagerBinding binding = DataBindingUtil.bind(view);
        binding.setArenaStats(mArenaStatsViewModel);
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_arena_stats_pager;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(final SearchEvent searchEvent) {
        Realm.getDefaultInstance().where(Season.class).equalTo("name", mSeasonName).findFirstAsync().asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Func1<RealmObject, Boolean>() {
                    @Override
                    public Boolean call(RealmObject realmObject) {
                        return realmObject.isLoaded();
                    }
                })
                .map(new Func1<RealmObject, Season>() {
                    @Override
                    public Season call(RealmObject realmObject) {
                        return (Season) Realm.getDefaultInstance().copyFromRealm(realmObject);
                    }
                })
                .flatMap(new Func1<Season, Observable<ServiceRecordArena>>() {
                    @Override
                    public Observable<ServiceRecordArena> call(Season seasons) {
                        return mRestStatsController.getServiceRecordsArena(searchEvent.getName(), seasons.getId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                })
                .filter(new Func1<ServiceRecordArena, Boolean>() {
                    @Override
                    public Boolean call(ServiceRecordArena serviceRecordArena) {
                        return serviceRecordArena.getResults().get(0).getResultCode().equals(0L);
                    }
                })
                .map(new Func1<ServiceRecordArena, ArenaStats>() {
                    @Override
                    public ArenaStats call(ServiceRecordArena serviceRecordArena) {
                        return serviceRecordArena.getResults().get(0).getResult().getArenaStats();
                    }
                })
                .subscribe(new SimpleObserver<ArenaStats>() {

                    @Override
                    public void onNext(ArenaStats arenaStats) {
                        super.onNext(arenaStats);

                        mArenaStatsForSeason = arenaStats;

                        List<Playlist> playlists = new ArrayList<>();
                        for (ArenaPlaylistStat arenaPlaylistStat : arenaStats.getArenaPlaylistStats()) {
                            Playlist playlist = Realm.getDefaultInstance().where(Playlist.class).equalTo("id", arenaPlaylistStat.getPlaylistId()).findFirst();

                            if (playlist != null && playlist.getName() != null) {
                                playlists.add(playlist);
                            }
                        }

                        mSpinner.setAdapter(new PlaylistSpinnerAdapter(getContext(), playlists));
                        mSpinner.setOnItemSelectedListener(getItemClicklistener());

                        updateBinding(mArenaStatsForSeason.getArenaPlaylistStats().get(0));
                    }
                });
    }

    private void updateBinding(final ArenaPlaylistStat arenaPlaylistStat) {
        mArenaStatsViewModel.setArenaStats(arenaPlaylistStat);
        ImageLoader.loadFittedImage(mArenaStatsViewModel.getWeaponUrl(), mArenaWeaponMostKillsIcon);
        ImageLoader.loadFittedImage(mArenaStatsViewModel.getCsrIconUrl(), mCsrDesIcon);
        ImageLoader.loadFittedImage(mArenaStatsViewModel.getCsrBackgroundUrl(), mCsrDesBackground);
    }

    @NonNull
    private AdapterView.OnItemSelectedListener getItemClicklistener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Playlist playlist = (Playlist) parent.getItemAtPosition(position);
                if (playlist != null) {
                    if (playlist.getId() != null && mArenaStatsForSeason != null) {
                        updateBinding(mArenaStatsForSeason.getArenaPlaylistStatForPlaylist(playlist.getId()));
                    } else {
                        parent.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }

    private static class PlaylistSpinnerAdapter extends ArrayAdapter<Playlist> {

        @NonNull
        private final List<Playlist> mPlaylists;

        PlaylistSpinnerAdapter(Context context, @NonNull List<Playlist> playlists) {
            super(context, R.layout.wg_playlist_spinner_item, playlists);
            mPlaylists = playlists;
            if (mPlaylists.isEmpty()) {
                Playlist emptyPlaylist = new Playlist();
                emptyPlaylist.setName("No Data");
                mPlaylists.add(emptyPlaylist);
            }
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = LayoutInflater.from(getContext()).inflate(R.layout.wg_playlist_spinner_item, parent, false);
            }

            Playlist playlist = mPlaylists.get(position);
            if (playlist != null) {
                TextView playlistName = (TextView) row.findViewById(R.id.playlist_name);
                playlistName.setText(playlist.getName());
            }
            return row;
        }

        @Override
        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = LayoutInflater.from(getContext()).inflate(R.layout.wg_playlist_spinner_item, parent, false);
            }

            Playlist playlist = mPlaylists.get(position);
            if (playlist != null) {
                TextView playlistName = (TextView) row.findViewById(R.id.playlist_name);
                playlistName.setText(playlist.getName());
            }
            return row;
        }
    }
}
