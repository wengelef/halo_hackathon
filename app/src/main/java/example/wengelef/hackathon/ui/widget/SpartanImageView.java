package example.wengelef.hackathon.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * SpartanImageView
 *
 * ImageView that can react to drawable changes
 */
public class SpartanImageView extends ImageView {

    @Nullable
    private OnImageChangeListener mOnImageChangeListener = null;

    public SpartanImageView(Context context) {
        super(context);
    }

    public SpartanImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpartanImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setImageChangeListener(@NonNull OnImageChangeListener onImageChangeListener) {
        mOnImageChangeListener = onImageChangeListener;
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (mOnImageChangeListener != null) {
            mOnImageChangeListener.onImageChanged(this);
        }
    }

    public interface OnImageChangeListener {
        void onImageChanged(ImageView imageView);
    }
}
