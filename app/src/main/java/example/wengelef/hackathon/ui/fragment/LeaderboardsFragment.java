package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RealmQueryFilter;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.ui.adapter.SeasonsPagerAdapter;
import example.wengelef.hackathon.ui.util.ZoomOutPageTransformer;
import example.wengelef.hackathon.util.CsrHelper;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * Created by fwengelewski on 5/5/16.
 *
 *
 * LeaderboardsFragment
 *
 * Display Global LeaderBoards
 */
public class LeaderboardsFragment extends ViewPagerFragment {

    @BindView(R.id.seasons_viewpager)
    ViewPager mSeasonsViewPager;

    @BindView(R.id.season_tabs)
    TabLayout mSeasonsTabs;

    private SeasonsPagerAdapter mSeasonsPagerAdapter;

    private Subscription mSeasonSubscription;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_leaderboards;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mSeasonsViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mSeasonsViewPager.setAdapter(mSeasonsPagerAdapter = new LeaderboardsSeasonsPagerAdapter(getChildFragmentManager()));

        CsrHelper.initialize(Realm.getDefaultInstance().where(CsrDesignation.class).findAll());

        mSeasonSubscription = Realm.getDefaultInstance().where(Season.class).findAllAsync().sort("startDate", Sort.DESCENDING).asObservable()
                .filter(new RealmQueryFilter<RealmResults<Season>>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<RealmResults<Season>>() {
                    @Override
                    public void onNext(RealmResults<Season> seasons) {
                        super.onNext(seasons);
                        mSeasonsViewPager.setOffscreenPageLimit(seasons.size());
                        mSeasonsPagerAdapter.setData(seasons);
                        mSeasonsTabs.setupWithViewPager(mSeasonsViewPager);
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mSeasonSubscription != null && !mSeasonSubscription.isUnsubscribed()) {
            mSeasonSubscription.unsubscribe();
        }
    }

    @Override
    public String getTitle() {
        return Loca.getString(R.string.title_leaderboards);
    }

    private static class LeaderboardsSeasonsPagerAdapter extends SeasonsPagerAdapter {

        public LeaderboardsSeasonsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (mSeasons != null) {
                return LeaderboardsPagerFragment.newInstance(mSeasons.get(position));
            }
            return null;
        }
    }
}
