package example.wengelef.hackathon.ui;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.HttpURLConnection;

import example.wengelef.hackathon.R;
import example.wengelef.hackathon.event.NavigationEvent;
import example.wengelef.hackathon.event.ProgressEvent;
import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.ui.widget.ButteryProgressBar;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Flo on 10.04.2016.
 *
 *
 * SpartanActivity
 *
 * Simple Base Class for MainActivity to handle ProgressBarEvents and NavigationEvents (Set ActionBar title)
 */
public abstract class SpartanActivity extends AppCompatActivity {

    protected ButteryProgressBar mProgressBar;
    protected CollapsingToolbarLayout mCollapsingToolbar;

    public SpartanApplication getApp() {
        return (SpartanApplication) getApplication();
    }

    @Override
    protected void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProgressEvent(ProgressEvent event) {
        if (mProgressBar.getVisibility() == View.VISIBLE && event.mType.equals(ProgressEvent.Type.HIDE)) {
            mProgressBar.setVisibility(View.INVISIBLE);
        } else if (mProgressBar.getVisibility() == View.INVISIBLE && event.mType.equals(ProgressEvent.Type.SHOW)) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        // Catch 429 ("Too Many Requests") from the public Halo API
        if (event.mError != null) {
            if (event.mError instanceof HttpException) {
                if (((HttpException) event.mError).code() == 429) {
                    Toast.makeText(this, Loca.getString(R.string.error_too_many_requests), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNavigationEvent(NavigationEvent navigationEvent) {
        if (mCollapsingToolbar != null) {
            mCollapsingToolbar.setTitle(navigationEvent.getFragment().getTitle());
        }
    }
}
