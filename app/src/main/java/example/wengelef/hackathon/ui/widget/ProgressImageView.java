package example.wengelef.hackathon.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.github.ybq.android.spinkit.SpinKitView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * ProgressImageView
 *
 * Show Progress until Background is set
 */
public class ProgressImageView extends FrameLayout {

    @BindView(R.id.progress_bar)
    SpinKitView mProgressBar;

    @BindView(R.id.image_view)
    SpartanImageView mImageView;

    public ProgressImageView(Context context) {
        super(context);
        init();
    }

    public ProgressImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        ButterKnife.bind(inflate(getContext(), R.layout.wg_progress_imageview, this));
    }

    public void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(VISIBLE);
        }
    }

    public void hideProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(INVISIBLE);
        }
    }

    @NonNull
    public ImageView getImageView() {
        return mImageView;
    }
}
