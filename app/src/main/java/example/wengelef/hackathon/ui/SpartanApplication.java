package example.wengelef.hackathon.ui;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.DaggerRestComponent;
import example.wengelef.hackathon.rest.RestComponent;
import example.wengelef.hackathon.util.FlavorConfig;
import example.wengelef.hackathon.util.SpartanRealmMigration;
import io.fabric.sdk.android.BuildConfig;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Created by fwengelewski on 4/6/16.
 */
public class SpartanApplication extends MultiDexApplication {

    private RestComponent mRestComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // Create Dagger Components
        mRestComponent = DaggerRestComponent.create();

        // Only Log with Crashlytics in Release Versions of Acpt and Prod
        if (!BuildConfig.DEBUG && (FlavorConfig.acpt() || FlavorConfig.prod())) {
            Timber.plant(new CrashlyticsTree());
        } else {
            Timber.plant(new Timber.DebugTree());
        }

        Loca.initialize(this);

        initEventBus();

        initRealm();
    }

    public RestComponent getRestComponent() {
        return mRestComponent;
    }

    /**
     * Configure EventBus
     */
    protected void initEventBus() {
        EventBus.builder().logNoSubscriberMessages(false).sendNoSubscriberEvent(false).installDefaultEventBus();
    }

    /**
     * Configure Realm
     */
    protected void initRealm() {
        RealmConfiguration config = new RealmConfiguration.Builder(this)
                .name("default")
                .schemaVersion(3)
                .migration(new SpartanRealmMigration()).build();

        Realm.setDefaultConfiguration(config);
    }

    private static class CrashlyticsTree extends Timber.Tree {

        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

            Crashlytics.log(message);

            if (t != null) {
                Crashlytics.logException(t);
            }
        }
    }
}
