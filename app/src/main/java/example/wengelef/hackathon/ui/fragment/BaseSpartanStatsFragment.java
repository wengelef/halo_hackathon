package example.wengelef.hackathon.ui.fragment;

import example.wengelef.hackathon.ui.fragment.BaseSpartanFragment;

/**
 * Created by fwengelewski on 6/8/16.
 *
 *
 * BaseSpartanStatsFragment
 *
 * Base Fragment Class for Stats Fragments displayed in {@link example.wengelef.hackathon.ui.fragment.SpartanStatsFragment}
 */
public abstract class BaseSpartanStatsFragment extends BaseSpartanFragment {

}
