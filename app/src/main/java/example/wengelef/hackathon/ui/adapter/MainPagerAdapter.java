package example.wengelef.hackathon.ui.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import example.wengelef.hackathon.ui.fragment.BaseSpartanFragment;
import example.wengelef.hackathon.ui.fragment.PagerFragmentFactory;
import example.wengelef.hackathon.ui.fragment.ViewPagerFragment;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * MainPagerAdapter
 *
 * MainViewPagerAdapter in {@link example.wengelef.hackathon.ui.MainActivity}
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PagerFragmentFactory.Target.values().length;
    }

    @Override
    public ViewPagerFragment getItem(int position) {
        return PagerFragmentFactory.create(PagerFragmentFactory.Target.values()[position]);
    }
}
