package example.wengelef.hackathon.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import org.greenrobot.eventbus.EventBus;

import example.wengelef.hackathon.event.NavigationEvent;
import example.wengelef.hackathon.ui.adapter.MainPagerAdapter;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * MainViewPager
 *
 * ViewPager with disabled TouchEvents (To be selected by the NavigationDrawer)
 */
public class MainViewPager extends ViewPager {
    public MainViewPager(Context context) {
        super(context);
    }

    public MainViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /*
     * never allow swiping! This Viewpager is navigated by the Navigation Drawer
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    /*
     * never allow touch! This Viewpager is navigated by the Navigation Drawer
     */
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public void setCurrentItem(int item) {
        super.setCurrentItem(item);
        EventBus.getDefault().post(new NavigationEvent(((MainPagerAdapter) getAdapter()).getItem(item)));
    }
}
