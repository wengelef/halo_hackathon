package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RealmQueryFilter;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.controller.RestMetadataController;
import example.wengelef.hackathon.rest.model.metadata.CampaignMission;
import example.wengelef.hackathon.ui.util.ZoomOutPageTransformer;
import example.wengelef.hackathon.util.RealmHelper;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * CampaignMissionsFragment
 *
 * Display Campaign Missions
 */
public class CampaignMissionsFragment extends ViewPagerFragment {

    @BindView(R.id.campaign_missions_viewpager)
    ViewPager mViewPager;

    @BindView(R.id.campaign_mission_tabs)
    TabLayout mTabLayout;

    @Inject RestMetadataController mMetaDataController;

    private Subscription mCampaignMissionsSubscription;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_campaign_missions;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        if (!RealmHelper.existsInDB(CampaignMission.class)) {
            mMetaDataController.getCampaignMissions()
                    .subscribeOn(Schedulers.io())
                    .doOnNext(new Action1<List<CampaignMission>>() {
                        @Override
                        public void call(final List<CampaignMission> campaignMissions) {
                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(campaignMissions);
                                }
                            });
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }

        mCampaignMissionsSubscription = Realm.getDefaultInstance().where(CampaignMission.class).findAllAsync().asObservable()
                .filter(new RealmQueryFilter<RealmResults<CampaignMission>>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<RealmResults<CampaignMission>>() {
                    @Override
                    public void onNext(RealmResults<CampaignMission> campaignMissions) {
                        super.onNext(campaignMissions);
                        mViewPager.setAdapter(new CampaignMissionPagerAdapter(getChildFragmentManager(), campaignMissions));
                        mTabLayout.setupWithViewPager(mViewPager);
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCampaignMissionsSubscription != null && !mCampaignMissionsSubscription.isUnsubscribed()) {
            mCampaignMissionsSubscription.unsubscribe();
        }
    }

    @Override
    public String getTitle() {
        return Loca.getString(R.string.title_campaigns);
    }

    static class CampaignMissionPagerAdapter extends FragmentStatePagerAdapter {

        @NonNull
        final List<CampaignMission> mCampaignMissionList;

        // Somehow i had to store the Strings in this List
        final private List<String> mTabTitles;

        public CampaignMissionPagerAdapter(FragmentManager fm, @NonNull List<CampaignMission> campaignMissionList) {
            super(fm);
            mCampaignMissionList = campaignMissionList;

            mTabTitles = new ArrayList<>();

            for (CampaignMission campaignMission : mCampaignMissionList) {
                mTabTitles.add(campaignMission.getName());
            }
        }

        @Override
        public BaseSpartanFragment getItem(int position) {
            return CampaignMissionPagerFragment.newInstance(mCampaignMissionList.get(position));
        }

        @Override
        public int getCount() {
            return mCampaignMissionList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabTitles.get(position);
        }
    }
}
