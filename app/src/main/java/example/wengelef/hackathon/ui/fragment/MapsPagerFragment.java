package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.f2prateek.dart.InjectExtra;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RealmQueryFilter;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.ui.adapter.MapsRecyclerViewAdapter;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * MapsPagerFragment
 *
 * Display Maps for a specific Gametype
 */
public class MapsPagerFragment extends BaseSpartanFragment {

    public static final String KEY_GAMETYPE = "key_gametype";

    @BindView(R.id.recycler_view_maps)
    RecyclerView mRecyclerView;

    @InjectExtra(KEY_GAMETYPE)
    String mGameType;

    @NonNull private MapsRecyclerViewAdapter mRecyclerViewAdapter = new MapsRecyclerViewAdapter();

    private List<MapData> mFilteredMaps = new ArrayList<>();

    public static MapsPagerFragment newInstance(String gametype) {
        Bundle args = new Bundle();
        args.putString(KEY_GAMETYPE, gametype);
        MapsPagerFragment fragment = new MapsPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_maps_pager;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        Realm.getDefaultInstance().where(MapData.class).findAllAsync().asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new RealmQueryFilter<RealmResults<MapData>>())
                .map(new Func1<RealmResults<MapData>, List<MapData>>() {
                    @Override
                    public List<MapData> call(RealmResults<MapData> mapDatas) {
                        List<MapData> filteredList = new ArrayList<MapData>();
                        for (MapData mapData : mapDatas) {
                            if (mapData.supportsGameType(mGameType)) {
                                filteredList.add(mapData);
                            }
                        }
                        return filteredList;
                    }
                })
                .subscribe(new SimpleObserver<List<MapData>>() {
                    @Override
                    public void onNext(List<MapData> maps) {
                        super.onNext(maps);
                        mRecyclerViewAdapter.setMapData(maps);
                    }
                });
    }
}
