package example.wengelef.hackathon.ui.viewmodel;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.metadata.Weapon;
import example.wengelef.hackathon.rest.model.stats.servicerecordarena.ArenaPlaylistStat;
import example.wengelef.hackathon.util.DateFormatter;
import io.realm.Realm;

/**
 * Created by fwengelewski on 9/20/16.
 *
 *
 * ArenaStatsViewModel
 *
 * ViewModel for ArenaStats
 */
public class ArenaStatsViewModel extends BaseObservable {

    @Nullable
    private ArenaPlaylistStat mArenaStat = null;

    @Nullable
    private Weapon mWeapon = null;

    @Nullable
    private CsrDesignation mCsrDesignation = null;

    public void setArenaStats(@NonNull final ArenaPlaylistStat arenaStats) {
        mArenaStat = arenaStats;

        mCsrDesignation = null;
        mWeapon = null;

        if (mArenaStat.getCsr() != null) {
            mCsrDesignation = Realm.getDefaultInstance().where(CsrDesignation.class).equalTo("id", mArenaStat.getCsr().getDesignationId()).findFirst();
        }

        mWeapon = Realm.getDefaultInstance().where(Weapon.class).equalTo("id", String.valueOf(mArenaStat.getWeaponWithMostKills().getWeaponId().getStockId())).findFirst();

        notifyChange();
    }

    public String getCsrKey() {
        if (mCsrDesignation != null) {
            return Loca.format("// %s", mCsrDesignation.getName());
        }
        return "";
    }

    public String getCsrValue() {
        if (mArenaStat != null && mArenaStat.getCsr() != null) {
            mArenaStat.getCsr().getTier();
        }
        return "Unranked";
    }

    public int getStatsVisibility() {
        return mArenaStat != null ? View.VISIBLE : View.GONE;
    }

    public int getCsrVisibility() {
        return mCsrDesignation != null ? View.VISIBLE : View.GONE;
    }

    public String getGamesPlayed() {
        if (mArenaStat != null) {
            return mArenaStat.getTotalGamesCompleted().toString();
        }
        return "";
    }

    public String getWinPercentage() {
        if (mArenaStat != null) {
            float winPercentage = ((float) mArenaStat.getTotalGamesWon() / (float) mArenaStat.getTotalGamesCompleted()) * 100;
            return Loca.format("%.2f%%", winPercentage);
        }
        return "";
    }

    public String getKills() {
        if (mArenaStat != null) {
            mArenaStat.getTotalKills();
        }
        return "";
    }

    public String getDeaths() {
        if (mArenaStat != null) {
            mArenaStat.getTotalDeaths();
        }
        return "";
    }

    public String getAssists() {
        if (mArenaStat != null) {
            mArenaStat.getTotalAssists();
        }
        return "";
    }

    public String getKdr() {
        if (mArenaStat != null) {
            return Loca.format("%.2f", (float) mArenaStat.getTotalKills() / (float) mArenaStat.getTotalDeaths());
        }
        return "";
    }

    public String getShotsFired() {
        if (mArenaStat != null) {
            return Loca.format("%s: %d", "ShotsFired", mArenaStat.getTotalShotsFired());
        }
        return "";
    }

    public String getShotsLanded() {
        if (mArenaStat != null) {
            return Loca.format("%s: %d", "ShotsLanded", mArenaStat.getTotalShotsLanded());
        }
        return "";
    }

    public String getTimePlayed() {
        if (mArenaStat != null) {
            return Loca.format("%s: %s", "Time Played", DateFormatter.formattedDuration(mArenaStat.getTotalTimePlayed()));
        }
        return "";
    }

    public String getWeaponWithMostKills() {
        if (mArenaStat != null && mWeapon != null) {
            return Loca.format(
                    "%s: %s // %s: %d",
                    "Tool of Destruction",
                    mWeapon.getName(),
                    "Kills",
                    mArenaStat.getWeaponWithMostKills().getTotalKills());
        }
        return "";
    }

    public String getWeaponUrl() {
        if (mWeapon != null) {
            return mWeapon.getSmallIconImageUrl();
        }
        return "";
    }

    public String getCsrIconUrl() {
        if (mArenaStat != null && mCsrDesignation != null) {
            return mCsrDesignation.getTiers().get(mArenaStat.getCsr().getTier()).getIconImageUrl();
        }
        return "";
    }

    public String getCsrBackgroundUrl() {
        if (mCsrDesignation != null) {
            return mCsrDesignation.getBannerImageUrl();
        }
        return "";
    }
}
