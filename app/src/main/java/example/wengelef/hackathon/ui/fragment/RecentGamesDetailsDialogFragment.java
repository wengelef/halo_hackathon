package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.constants.Playlists;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.rest.model.stats.PlayerStat;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportArena;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCampaign;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportCustom;
import example.wengelef.hackathon.rest.model.stats.PostGameCarnageReportWarzone;
import example.wengelef.hackathon.rest.model.stats.TeamStat;
import example.wengelef.hackathon.ui.SpartanApplication;
import example.wengelef.hackathon.ui.adapter.PostGameCarnageArenaRecyclerViewAdapter;
import example.wengelef.hackathon.ui.adapter.PostGameCarnageWarzoneRecyclerViewAdapter;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fwengelewski on 6/9/16.
 */
public class RecentGamesDetailsDialogFragment extends BaseDialogFragment {

    public static final String KEY_MATCH_ID = "key_match_id";
    public static final String KEY_GAME_MODE = "key_game_mode";

    private static final int DRAG_TO_DISMISS_DISTANCE = 400;

    @BindView(R.id.recent_games_details_container)
    CardView mRecentGamesDetailsContainer;

    @BindView(R.id.recycler_winners)
    RecyclerView mRecyclerWinners;

    @BindView(R.id.games_details_container_winner_card)
    CardView mGamesDetailsContainerWinnerCard;

    @BindView(R.id.recycler_losers)
    RecyclerView mRecyclerLosers;

    @BindView(R.id.games_details_container_loser_card)
    CardView mGamesDetailsContainerLoserCard;

    @Inject RestStatsController mRestStatsController;

    @InjectExtra(KEY_MATCH_ID)
    String mMatchId;

    @InjectExtra(KEY_GAME_MODE)
    int mGameMode;

    private float mInitialDragContainerPosition;

    public static RecentGamesDetailsDialogFragment newInstance(@NonNull String matchId, @NonNull Integer gameMode) {
        Bundle args = new Bundle(2);
        args.putString(KEY_MATCH_ID, matchId);
        args.putInt(KEY_GAME_MODE, gameMode);

        RecentGamesDetailsDialogFragment instance = new RecentGamesDetailsDialogFragment();
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);
    }

    @Override
    public void onStart() {
        mAnimationListener = getDialogAnimationListener();
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fr_recent_games_details, container, false);
        ButterKnife.bind(this, root);

        Dart.inject(this, getArguments());

        ((SpartanApplication) getActivity().getApplication()).getRestComponent().inject(this);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecentGamesDetailsContainer.setOnTouchListener(getTouchListener());

        mRecyclerWinners.setHasFixedSize(true);
        mRecyclerWinners.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerLosers.setHasFixedSize(true);
        mRecyclerLosers.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (mGameMode == (Playlists.WARZONE.getId())) {
            presentWarzoneReport();
        } else if (mGameMode == (Playlists.ARENA.getId())) {
            presentArenaReport();
        } else if (mGameMode == (Playlists.CUSTOM.getId())) {
            presentCustomReport();
        } else if (mGameMode == (Playlists.CAMPAIGN.getId())) {
            presentCampaignReport();
        }
    }

    private void presentWarzoneReport() {
        mRestStatsController.getPostGameReportWarzone(mMatchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<PostGameCarnageReportWarzone>() {
                    @Override
                    public void onNext(PostGameCarnageReportWarzone postGameCarnageReportWarzone) {
                        super.onNext(postGameCarnageReportWarzone);
                        List<TeamStat> sortedTeams = postGameCarnageReportWarzone.getTeamsByRank();

                        if (sortedTeams != null) {
                            List<PlayerStat> winners = postGameCarnageReportWarzone.getPlayersForTeamId(sortedTeams.get(0).getTeamId());
                            if (winners != null) {
                                PostGameCarnageWarzoneRecyclerViewAdapter winnerAdapter = new PostGameCarnageWarzoneRecyclerViewAdapter();
                                mRecyclerWinners.setAdapter(winnerAdapter);
                                winnerAdapter.setPlayers(winners, sortedTeams.get(0));
                            }

                            if (sortedTeams.size() > 1) {
                                List<PlayerStat> losers = postGameCarnageReportWarzone.getPlayersForTeamId(sortedTeams.get(1).getTeamId());
                                if (losers != null) {
                                    PostGameCarnageWarzoneRecyclerViewAdapter losersAdapter = new PostGameCarnageWarzoneRecyclerViewAdapter();
                                    mRecyclerLosers.setAdapter(losersAdapter);
                                    losersAdapter.setPlayers(losers, sortedTeams.get(1));
                                }
                            }
                        }
                    }
                });
    }

    private void presentArenaReport() {
        mRestStatsController.getPostGameReportArena(mMatchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<PostGameCarnageReportArena>() {
                    @Override
                    public void onNext(PostGameCarnageReportArena postGameCarnageReportArena) {
                        super.onNext(postGameCarnageReportArena);
                        List<TeamStat> sortedTeams = postGameCarnageReportArena.getTeamsByRank();

                        // TODO this is assuming we have 2 Teams
                        if (sortedTeams != null) {
                            List<PlayerStat> winners = postGameCarnageReportArena.getPlayersForTeamId(sortedTeams.get(0).getTeamId());
                            if (winners != null) {
                                PostGameCarnageArenaRecyclerViewAdapter winnerAdapter = new PostGameCarnageArenaRecyclerViewAdapter();
                                mRecyclerWinners.setAdapter(winnerAdapter);
                                winnerAdapter.setPlayers(winners, sortedTeams.get(0));
                            }

                            if (sortedTeams.size() > 1) {
                                List<PlayerStat> losers = postGameCarnageReportArena.getPlayersForTeamId(sortedTeams.get(1).getTeamId());
                                if (losers != null) {
                                    PostGameCarnageArenaRecyclerViewAdapter losersAdapter = new PostGameCarnageArenaRecyclerViewAdapter();
                                    mRecyclerLosers.setAdapter(losersAdapter);
                                    losersAdapter.setPlayers(losers, sortedTeams.get(1));
                                }
                            }
                        }
                    }
                });
    }

    private void presentCustomReport() {
        mRestStatsController.getPostGameReportCustom(mMatchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<PostGameCarnageReportCustom>() {
                    @Override
                    public void onNext(PostGameCarnageReportCustom postGameCarnageReportCustom) {
                        super.onNext(postGameCarnageReportCustom);
                        List<TeamStat> sortedTeams = postGameCarnageReportCustom.getTeamsByRank();

                        if (sortedTeams != null) {
                            List<PlayerStat> winners = postGameCarnageReportCustom.getPlayersForTeamId(sortedTeams.get(0).getTeamId());
                            if (winners != null) {
                                PostGameCarnageArenaRecyclerViewAdapter winnerAdapter = new PostGameCarnageArenaRecyclerViewAdapter();
                                mRecyclerWinners.setAdapter(winnerAdapter);
                                winnerAdapter.setPlayers(winners, sortedTeams.get(0));
                            }

                            if (sortedTeams.size() > 1) {
                                List<PlayerStat> losers = postGameCarnageReportCustom.getPlayersForTeamId(sortedTeams.get(1).getTeamId());
                                if (losers != null) {
                                    PostGameCarnageArenaRecyclerViewAdapter losersAdapter = new PostGameCarnageArenaRecyclerViewAdapter();
                                    mRecyclerLosers.setAdapter(losersAdapter);
                                    losersAdapter.setPlayers(losers, sortedTeams.get(1));
                                }
                            }
                        }
                    }
                });
    }

    private void presentCampaignReport() {
        mRestStatsController.getPostGameReportCampaign(mMatchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<PostGameCarnageReportCampaign>() {
                    @Override
                    public void onNext(PostGameCarnageReportCampaign postGameCarnageReportCampaign) {
                        super.onNext(postGameCarnageReportCampaign);
                        Timber.i("Received postGame Report for Campaign");
                    }
                });
    }

    private DialogAnimationListener getDialogAnimationListener() {
        return new DialogAnimationListener() {
            @Override
            public void onAnimationEnd() {
                mInitialDragContainerPosition = mRecentGamesDetailsContainer.getY();
            }
        };
    }

    private View.OnTouchListener getTouchListener() {
        return new View.OnTouchListener() {

            private float mLastTouchY;
            private float mDistanceCovered;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int action = MotionEventCompat.getActionMasked(event);

                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        mLastTouchY = event.getRawY();
                        mDistanceCovered = 0.0f;
                        break;
                    }

                    case MotionEvent.ACTION_MOVE: {
                        final float y = event.getRawY();
                        final float dy = y - mLastTouchY;
                        mDistanceCovered += dy;
                        mRecentGamesDetailsContainer.setY(mRecentGamesDetailsContainer.getY() + dy);
                        mLastTouchY = y;
                        break;
                    }

                    case MotionEvent.ACTION_UP: {
                        onDragEnd();
                        break;
                    }

                    case MotionEvent.ACTION_CANCEL: {
                        onDragEnd();
                        break;
                    }
                }
                return true;
            }

            private void onDragEnd() {
                if (mDistanceCovered > DRAG_TO_DISMISS_DISTANCE) {
                    close();
                } else {
                    mRecentGamesDetailsContainer.animate().y(mInitialDragContainerPosition);
                }
            }
        };
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.exit_button)
    public void onClick() {
        close();
    }
}
