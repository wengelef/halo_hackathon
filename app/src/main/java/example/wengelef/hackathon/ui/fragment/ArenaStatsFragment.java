package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RealmQueryFilter;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.ui.adapter.SeasonsPagerAdapter;
import example.wengelef.hackathon.ui.util.ZoomOutPageTransformer;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * Created by fwengelewski on 6/8/16.
 * <p>
 * <p>
 * ArenaStatsFragment
 * <p>
 * Displays Arena stats in {@link SpartanStatsFragment}
 */
public class ArenaStatsFragment extends BaseSpartanStatsFragment {

    @BindView(R.id.seasons_viewpager)
    ViewPager mSeasonsViewPager;

    @BindView(R.id.season_tabs)
    TabLayout mSeasonsTabs;

    private SeasonsPagerAdapter mSeasonsPagerAdapter;

    @Inject
    RestStatsController mRestStatsController;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_arena_stats;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mSeasonsViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mSeasonsViewPager.setAdapter(mSeasonsPagerAdapter = new ArenaStatsPagerAdapter(getChildFragmentManager()));

        Realm.getDefaultInstance().where(Season.class).findAllAsync().sort("startDate", Sort.DESCENDING).asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new RealmQueryFilter<RealmResults<Season>>())
                .subscribe(new SimpleObserver<RealmResults<Season>>() {
                    @Override
                    public void onNext(RealmResults<Season> seasons) {
                        super.onNext(seasons);
                        mSeasonsViewPager.setOffscreenPageLimit(seasons.size());
                        mSeasonsPagerAdapter.setData(seasons);
                        mSeasonsTabs.setupWithViewPager(mSeasonsViewPager);
                    }
                });
    }

    private static class ArenaStatsPagerAdapter extends SeasonsPagerAdapter {

        ArenaStatsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (mSeasons != null) {
                return ArenaStatsPagerFragment.newInstance(mSeasons.get(position));
            }
            return null;
        }
    }
}
