package example.wengelef.hackathon.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.ui.widget.ProgressImageView;

/**
 * Created by fwengelewski on 6/30/16.
 *
 *
 * PostGameCarnageHeaderViewHolder
 *
 * Header for PostGameCarnage Reports
 */

public class PostGameCarnageHeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.team_emblem)
    ProgressImageView teamEmblem;

    @BindView(R.id.team_color)
    View teamColor;

    public PostGameCarnageHeaderViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ProgressImageView getTeamEmblem() {
        return teamEmblem;
    }

    public View getTeamColor() {
        return teamColor;
    }
}
