package example.wengelef.hackathon.ui.fragment;

/**
 * Created by fwengelewski on 5/4/16.
 *
 * Base Class for Fragments displayed in the Viewpager
 */
public abstract class ViewPagerFragment extends BaseSpartanFragment {

    /**
     * @return Title of the Fragment, will be Displayed in the ActionBar
     */
    public abstract String getTitle();
}
