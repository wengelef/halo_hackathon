package example.wengelef.hackathon.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.f2prateek.dart.InjectExtra;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Module;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.rest.model.stats.Leaderboards;
import example.wengelef.hackathon.ui.adapter.LeaderboardsRecyclerViewAdapter;
import io.realm.Realm;
import io.realm.RealmObject;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by fwengelewski on 5/6/16.
 *
 *
 * LeaderboardsPagerFragment
 *
 * Display Global Leaderboards for a Season
 */
@Module
public class LeaderboardsPagerFragment extends BaseSpartanFragment {

    public static final String KEY_SEASON_NAME = "key_season_name";

    @BindView(R.id.playlist_spinner)
    AppCompatSpinner mSpinner;

    @BindView(R.id.recycler_view_leaderboards)
    RecyclerView mRecyclerView;

    @InjectExtra(KEY_SEASON_NAME)
    String mSeasonName;

    private Subscription mSeasonSubscription;

    @Inject RestStatsController mRestStatsController;

    public static LeaderboardsPagerFragment newInstance(@NonNull Season season) {
        Bundle args = new Bundle(1);
        args.putString(KEY_SEASON_NAME, season.getName());
        LeaderboardsPagerFragment fragment = new LeaderboardsPagerFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_leaderboards_pager;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSeasonSubscription = Realm.getDefaultInstance().where(Season.class).equalTo("name", mSeasonName).findFirstAsync().asObservable()
                .filter(new Func1<RealmObject, Boolean>() {
                    @Override
                    public Boolean call(RealmObject realmObject) {
                        return realmObject.isLoaded();
                    }
                })
                .map(new Func1<RealmObject, Season>() {
                    @Override
                    public Season call(RealmObject realmObject) {
                        return (Season) Realm.getDefaultInstance().copyFromRealm(realmObject);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Season>() {
                    @Override
                    public void onNext(Season season) {
                        super.onNext(season);
                        mSpinner.setAdapter(new PlaylistSpinnerAdapter(getContext(), season.getPlaylists()));
                        mSpinner.setSelection(season.getPlaylists().size() - 1);
                        mSpinner.setOnItemSelectedListener(getItemClicklistener(season));
                    }
                });
    }

    @NonNull
    private AdapterView.OnItemSelectedListener getItemClicklistener(final @NonNull Season season) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Playlist playlist = (Playlist) parent.getItemAtPosition(position);

                // Hint 'Playlist'
                if (playlist.getId() == null) {
                    return;
                }

                mRestStatsController.getLeaderboards(season.getId(), playlist.getId(), null)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SimpleObserver<Leaderboards>() {
                            @Override
                            public void onNext(Leaderboards leaderboards) {
                                super.onNext(leaderboards);
                                mRecyclerView.setAdapter(new LeaderboardsRecyclerViewAdapter(leaderboards));
                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mSeasonSubscription != null && !mSeasonSubscription.isUnsubscribed()) {
            mSeasonSubscription.unsubscribe();
        }
    }

    private static class PlaylistSpinnerAdapter extends ArrayAdapter<Playlist> {

        @NonNull
        private final List<Playlist> mPlaylists;

        public PlaylistSpinnerAdapter(Context context, @NonNull List<Playlist> playlists) {
            super(context, R.layout.wg_playlist_spinner_item, playlists);
            mPlaylists = playlists;

            Playlist hint = new Playlist();
            hint.setName(Loca.getString(R.string.playlist_spinner_hint));

            mPlaylists.add(hint);
        }

        @Override
        public int getCount() {
            return super.getCount() - 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if(row == null) {
                row = LayoutInflater.from(getContext()).inflate(R.layout.wg_playlist_spinner_item, parent, false);
            }

            Playlist playlist = mPlaylists.get(position);
            if(playlist!= null) {
                TextView playlistName = (TextView) row.findViewById(R.id.playlist_name);
                playlistName.setText(playlist.getName());
            }
            return row;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if(row == null) {
                row = LayoutInflater.from(getContext()).inflate(R.layout.wg_playlist_spinner_item, parent, false);
            }

            Playlist playlist = mPlaylists.get(position);
            if(playlist!= null) {
                TextView playlistName = (TextView) row.findViewById(R.id.playlist_name);
                playlistName.setText(playlist.getName());
            }
            return row;
        }
    }
}
