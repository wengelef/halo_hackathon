package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.event.SearchEvent;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.rest.model.stats.servicerecordwarzone.ServiceRecordWarzone;
import example.wengelef.hackathon.rest.model.stats.servicerecordwarzone.WarzoneStat;
import example.wengelef.hackathon.ui.widget.KDProgressView;
import example.wengelef.hackathon.ui.widget.WarzoneProgressView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fwengelewski on 6/8/16.
 *
 *
 * WarzoneStatsFragment
 *
 * Displays Warzone stats in {@link SpartanStatsFragment}
 */
public class WarzoneStatsFragment extends BaseSpartanStatsFragment {

    @BindView(R.id.warzone_kill_stats_progress_view)
    WarzoneProgressView mWarzoneProgressView;

    @BindView(R.id.warzone_kd_progress_view)
    KDProgressView mWarzoneKDView;

    @Inject
    RestStatsController mRestStatsController;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_warzone_stats;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(SearchEvent searchEvent) {
        mWarzoneProgressView.showProgress();
        mWarzoneKDView.showProgress();

        List<String> players = new ArrayList<>();
        players.add(searchEvent.getName());

        mRestStatsController.getServiceRecordsWarzone(players)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<ServiceRecordWarzone>() {
                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        mWarzoneProgressView.hideProgress();
                        mWarzoneKDView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mWarzoneProgressView.hideProgress();
                        mWarzoneKDView.hideProgress();
                    }

                    @Override
                    public void onNext(ServiceRecordWarzone serviceRecordWarzone) {
                        super.onNext(serviceRecordWarzone);

                        WarzoneStat warzoneStat = serviceRecordWarzone.getResults().get(0).getResult().getWarzoneStat();
                        mWarzoneProgressView.setData(warzoneStat);
                        mWarzoneKDView.setData(warzoneStat.getTotalKills(), warzoneStat.getTotalDeaths());
                    }
                });
    }
}
