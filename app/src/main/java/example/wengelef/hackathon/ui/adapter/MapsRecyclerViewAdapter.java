package example.wengelef.hackathon.ui.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.BR;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.ui.viewmodel.MapsViewModel;
import example.wengelef.hackathon.ui.widget.ProgressImageView;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * MapsRecyclerViewAdapter
 *
 * RecyclerAdapter for {@link example.wengelef.hackathon.ui.fragment.MapsFragment}
 */
public class MapsRecyclerViewAdapter extends RecyclerView.Adapter<MapsRecyclerViewAdapter.MapsViewHolder> {

    private List<MapData> mMapData = new ArrayList<>();

    public void setMapData(List<MapData> maps) {
        mMapData = maps;
        notifyDataSetChanged();
    }

    @Override
    public MapsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recyclerview_item_maps, parent, false);

        return new MapsViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(MapsViewHolder holder, int position) {
        holder.getViewDataBinding().setVariable(BR.mapData, new MapsViewModel(mMapData.get(position)));
        holder.loadMapImage(mMapData.get(position).getImageUrl());
    }

    @Override
    public int getItemCount() {
        return mMapData.size();
    }

    static class MapsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.map_image)
        ProgressImageView mMapImage;

        private ViewDataBinding mViewDataBinding;

        MapsViewHolder(@NonNull final ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            ButterKnife.bind(this, viewDataBinding.getRoot());
            mViewDataBinding = viewDataBinding;
        }

        void loadMapImage(String imageUrl) {
            ImageLoader.loadFittedImage(imageUrl, mMapImage);
        }

        ViewDataBinding getViewDataBinding() {
            return mViewDataBinding;
        }
    }
}
