package example.wengelef.hackathon.ui.adapter;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.event.MenuEvent;
import example.wengelef.hackathon.event.PlayerIdEvent;
import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.stats.LeaderboardResult;
import example.wengelef.hackathon.rest.model.stats.Leaderboards;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.ui.fragment.LeaderboardsFragment;
import example.wengelef.hackathon.ui.widget.ProgressImageView;
import example.wengelef.hackathon.util.CsrHelper;

/**
 * Created by fwengelewski on 5/5/16.
 *
 *
 * LeaderboardsRecyclerViewAdapter
 *
 * Adapter for RecyclerView in {@link LeaderboardsFragment}
 */
public class LeaderboardsRecyclerViewAdapter extends RecyclerView.Adapter<LeaderboardsRecyclerViewAdapter.ViewHolder> {

    @NonNull
    private final Leaderboards mLeaderboards;

    public LeaderboardsRecyclerViewAdapter(@NonNull Leaderboards leaderboards) {
        mLeaderboards = leaderboards;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_leaderboards, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final LeaderboardResult result = mLeaderboards.getLeaderboardResults().get(position);

        holder.mPlayerName.setText(result.getPlayerId().getGamerTag());
        holder.mCsr.setText(String.valueOf(result.getScore().getCsr()));
        holder.mRank.setText(String.valueOf(result.getRank()));

        CsrDesignation csrDesignation = CsrHelper.getDesignationForId(result.getScore().getDesignationId());
        if (csrDesignation != null) {
            holder.mCsrRating.setText(csrDesignation.getName());
            ImageLoader.loadFittedImage(csrDesignation.getTiers().get(0).getIconImageUrl(), holder.icon);
            ImageLoader.loadFittedImage(csrDesignation.getBannerImageUrl(), holder.banner);
        }

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PlayerIdEvent(result.getPlayerId()));
                EventBus.getDefault().post(new MenuEvent(R.id.menu_spartan_stats));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLeaderboards.getResultCount();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.player_name)
        TextView mPlayerName;

        @BindView(R.id.rank)
        TextView mRank;

        @BindView(R.id.csr_rating)
        TextView mCsrRating;

        @BindView(R.id.csr)
        TextView mCsr;

        @BindView(R.id.icon_image)
        ProgressImageView icon;

        @BindView(R.id.icon_banner)
        ProgressImageView banner;

        @BindView(R.id.leaderboard_overflow)
        ImageButton overflow;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
