package example.wengelef.hackathon.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;

/**
 * Created by fwengelewski on 7/12/16.
 *
 *
 * StatsTextView
 *
 * Display
 */
@BindingMethods({
        @BindingMethod(type = StatsTextView.class, attribute = "key", method = "setKey"),
        @BindingMethod(type = StatsTextView.class, attribute = "value", method = "setValue")})
public class StatsTextView extends LinearLayout {

    @BindView(R.id.key)
    TextView mKeyTextView;

    @BindView(R.id.value)
    TextView mValueTextView;

    public StatsTextView(Context context) {
        super(context);
        ButterKnife.bind(inflate());
        init(context);
    }

    public StatsTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ButterKnife.bind(inflate());
        init(context, attrs);
    }

    public StatsTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ButterKnife.bind(inflate());
        init(context, attrs);
    }

    public void setKey(final String key) {
        mKeyTextView.setText(key);
    }

    /**
     * Sets the value and sets the View visible
     * @param value
     */
    public void setValue(final String value) {
        setVisibility(VISIBLE);
        mValueTextView.setText(value);
    }

    private View inflate() {
        return inflate(getContext(), R.layout.wg_stats_textview, this);
    }

    private void init(@NonNull Context context) {
        init(context, null);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        setVisibility(GONE);
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.StatsTextView, 0, 0);
            try {
                String key = ta.getString(R.styleable.StatsTextView_key);
                String value = ta.getString(R.styleable.StatsTextView_value);

                mKeyTextView.setText(key);
                mValueTextView.setText(value);
            } finally {
                ta.recycle();
            }
        }
    }
}
