package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import example.wengelef.hackathon.R;
import example.wengelef.hackathon.resources.Loca;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * SpartanStatsFragment
 *
 * Display Spartan Stats
 * {@link RecentGamesFragment}
 * {@link WarzoneStatsFragment}
 * {@link CampaignStatsFragment}
 * {@link ArenaStatsFragment}
 * {@link CustomStatsFragment}
 */
public class SpartanStatsFragment extends ViewPagerFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_spartan_stats, container, false);
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_spartan_stats;
    }

    @Override
    public String getTitle() {
        return Loca.getString(R.string.title_spartan_stats);
    }
}
