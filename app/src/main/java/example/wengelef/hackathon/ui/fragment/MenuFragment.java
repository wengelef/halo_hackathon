package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.event.MenuEvent;
import example.wengelef.hackathon.resources.Loca;
import timber.log.Timber;

/**
 * Created by fwengelewski on 6/8/16.
 *
 *
 * MenuFragment
 *
 * Shows 4 Tiles with Menu Items
 */
public class MenuFragment extends ViewPagerFragment {

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_menu;
    }

    @SuppressWarnings("unused")
    @OnClick({R.id.menu_statistics, R.id.menu_maps, R.id.menu_campaign, R.id.menu_leaderboards})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_statistics: {
                EventBus.getDefault().post(new MenuEvent(R.id.menu_spartan_stats));
            }
            break;

            case R.id.menu_maps: {
                EventBus.getDefault().post(new MenuEvent(R.id.menu_maps));
            }
            break;

            case R.id.menu_campaign: {
                EventBus.getDefault().post(new MenuEvent(R.id.menu_campaign_missions));
            }
            break;

            case R.id.menu_leaderboards: {
                EventBus.getDefault().post(new MenuEvent(R.id.menu_campaign_leaderboards));
            }
            break;

            default: {
                Timber.v("unknown View clicked");
            }
        }
    }

    @Override
    public String getTitle() {
        return Loca.getString(R.string.app_name);
    }
}
