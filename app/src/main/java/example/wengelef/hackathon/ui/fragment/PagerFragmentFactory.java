package example.wengelef.hackathon.ui.fragment;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * PagerFragmentFactory
 *
 * Create {@link BaseSpartanFragment}s for {@link example.wengelef.hackathon.ui.adapter.MainPagerAdapter}
 */
public class PagerFragmentFactory {

    /**
     * ViewPager targets
     */
    public enum Target {
        MENU, SPARTAN_STATS, MAPS, CAMPAIGN_MISSIONS, LEADERBOARDS
    }

    public static ViewPagerFragment create(Target target) {
        if (target.equals(Target.MENU)) {
            return new MenuFragment();
        } else if (target.equals(Target.SPARTAN_STATS)) {
            return new SpartanStatsFragment();
        } else if (target.equals(Target.MAPS)) {
            return new MapsFragment();
        } else if (target.equals(Target.CAMPAIGN_MISSIONS)) {
            return new CampaignMissionsFragment();
        } else if (target.equals(Target.LEADERBOARDS)) {
            return new LeaderboardsFragment();
        }
        throw new IllegalArgumentException("Unknown Viewpager Target!");
    }
}
