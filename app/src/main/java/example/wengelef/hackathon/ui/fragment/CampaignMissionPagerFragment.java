package example.wengelef.hackathon.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.f2prateek.dart.InjectExtra;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.BR;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RealmQueryFilter;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.databinding.FrCampaignMissionPagerBinding;
import example.wengelef.hackathon.rest.model.metadata.CampaignMission;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.ui.viewmodel.CampaignMissionViewModel;
import example.wengelef.hackathon.ui.widget.ProgressImageView;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * CampaignMissionPagerFragment
 *
 * Fragment displayed in the CampaignMissions Fragment ViewPager
 */
public class CampaignMissionPagerFragment extends BaseSpartanFragment {

    public static final String KEY_INDEX = "key_index";

    @BindView(R.id.campaign_image)
    ProgressImageView mCampaignImage;

    @InjectExtra(KEY_INDEX)
    String mCampaignMissionIndex;

    private Subscription mCampaignMissionSubscriptions;

    private CampaignMissionViewModel mCampignMissionViewModel = new CampaignMissionViewModel();

    public static CampaignMissionPagerFragment newInstance(@NonNull CampaignMission campaignMission) {
        Bundle bundle = new Bundle(1);
        bundle.putString(KEY_INDEX, campaignMission.getMissionNumber());
        CampaignMissionPagerFragment fragment = new CampaignMissionPagerFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_campaign_mission_pager;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FrCampaignMissionPagerBinding viewbinding = DataBindingUtil.bind(view);
        viewbinding.setCampaignData(mCampignMissionViewModel);

        mCampaignMissionSubscriptions = Realm.getDefaultInstance().where(CampaignMission.class).equalTo("missionNumber", mCampaignMissionIndex).findAllAsync().asObservable()
                .filter(new RealmQueryFilter<RealmResults<CampaignMission>>())
                .flatMap(new Func1<RealmResults<CampaignMission>, Observable<CampaignMission>>() {
                    @Override
                    public Observable<CampaignMission> call(RealmResults<CampaignMission> campaignMissions) {
                        return Observable.from(campaignMissions);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<CampaignMission>() {
                    @Override
                    public void onNext(CampaignMission campaignMission) {
                        super.onNext(campaignMission);
                        mCampignMissionViewModel.setCampaignData(campaignMission);
                        ImageLoader.loadFittedImage(mCampignMissionViewModel.getImageUrl(), mCampaignImage);
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCampaignMissionSubscriptions != null && !mCampaignMissionSubscriptions.isUnsubscribed()) {
            mCampaignMissionSubscriptions.unsubscribe();
        }
    }
}
