package example.wengelef.hackathon.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.f2prateek.dart.InjectExtra;

import butterknife.BindView;
import butterknife.OnClick;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.databinding.FrRecentGamesPagerBinding;
import example.wengelef.hackathon.rest.model.stats.ServiceRecordResult;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.ui.viewmodel.RecentGameViewModel;
import example.wengelef.hackathon.ui.widget.ProgressImageView;

/**
 * Created by fwengelewski on 4/18/16.
 * <p>
 * <p>
 * RecentGameFragment
 * <p>
 * Displays a recent game
 */
public class RecentGamePagerFragment extends BaseSpartanFragment {

    public static final String KEY_DATA = "key_data";

    @BindView(R.id.game_mode_icon)
    ProgressImageView mGameModeIcon;

    @BindView(R.id.map_image)
    ProgressImageView mMapImage;

    @Nullable
    @InjectExtra(KEY_DATA)
    ServiceRecordResult mGameData = null;

    private RecentGameViewModel mRecentGameViewModel;

    public static RecentGamePagerFragment newInstance(@NonNull ServiceRecordResult serviceRecordResult) {
        Bundle bundle = new Bundle(1);
        bundle.putSerializable(KEY_DATA, serviceRecordResult);

        RecentGamePagerFragment fragment = new RecentGamePagerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_recent_games_pager;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecentGameViewModel = new RecentGameViewModel(mGameData);

        FrRecentGamesPagerBinding viewBinding = DataBindingUtil.bind(view);
        viewBinding.setGameData(mRecentGameViewModel);

        ImageLoader.loadFittedImage(mRecentGameViewModel.getMapImageUrl(), mMapImage);
        ImageLoader.loadFittedImage(mRecentGameViewModel.getGameVariantIconUrl(), mGameModeIcon);
    }

    @OnClick(R.id.recent_games_more_button)
    public void onClick() {
        if (mGameData != null) {
            RecentGamesDetailsDialogFragment.newInstance(mGameData.getId().getMatchId(), mGameData.getId().getGameMode())
                    .show(getChildFragmentManager(), "RecentGamesDetails");
        }
    }
}
