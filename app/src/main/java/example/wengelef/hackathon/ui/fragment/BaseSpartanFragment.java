package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.f2prateek.dart.Dart;

import javax.inject.Inject;

import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.rest.RestComponent;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.ui.SpartanActivity;
import example.wengelef.hackathon.ui.SpartanApplication;

/**
 * Created by Flo on 10.04.2016.
 *
 *
 * BaseSpartanFragment
 *
 * Base Class for all Fragments
 * Provides the RestComponents for all Child Fragments
 */
public abstract class BaseSpartanFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(layoutResourceId(), container, false);
        ButterKnife.bind(this, root);
        Dart.inject(this, getArguments());
        inject();
        return root;
    }

    /**
     * Get the Application Scope RestComponent
     * @return
     */
    protected RestComponent getRestComponent() {
        return ((SpartanActivity) getActivity()).getApp().getRestComponent();
    }

    /**
     * Get the Layout Recource Id of the inheriting Fragment
     * @return Layout Resource Id
     */
    protected abstract @LayoutRes int layoutResourceId();

    /**
     * Give inheritors the change to inject with Dagger before {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} has ended
     */
    protected void inject() {

    }
}
