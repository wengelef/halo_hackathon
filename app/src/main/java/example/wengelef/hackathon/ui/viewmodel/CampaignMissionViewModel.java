package example.wengelef.hackathon.ui.viewmodel;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import example.wengelef.hackathon.rest.model.metadata.CampaignMission;

/**
 * Created by fwengelewski on 9/20/16.
 *
 *
 * CampaignMissionViewModel
 *
 * ViewModel for CampaignMissions
 */
public class CampaignMissionViewModel extends BaseObservable {

    @Nullable
    private CampaignMission mCampaignMission;

    public void setCampaignData(CampaignMission campaignMission) {
        mCampaignMission = campaignMission;
        notifyChange();
    }

    public String getName() {
        if (mCampaignMission != null) {
            return mCampaignMission.getName();
        }
        return "";
    }

    public String getDescription() {
        if (mCampaignMission != null) {
            return mCampaignMission.getDescription();
        }
        return "";
    }

    public String getImageUrl() {
        if (mCampaignMission != null) {
            return mCampaignMission.getImageUrl();
        }
        return "";
    }
}
