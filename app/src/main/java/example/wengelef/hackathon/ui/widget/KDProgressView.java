package example.wengelef.hackathon.ui.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.SpinKitView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;

/**
 * Created by fwengelewski on 4/21/16.
 *
 *
 * WarzoneKDProgressView
 *
 * Display Kill/Death ratio
 */
public class KDProgressView extends FrameLayout {

    @BindView(R.id.warzone_kill_stats_progress_bar)
    SpinKitView mWarzoneProgressBar;

    @BindView(R.id.progress_views)
    FrameLayout mProgressViews;

    @BindView(R.id.kills)
    ProgressBar mProgressKills;

    @BindView(R.id.deaths)
    ProgressBar mProgressDeaths;

    public KDProgressView(Context context) {
        super(context);
        init();
    }

    public KDProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KDProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        ButterKnife.bind(inflate(getContext(), R.layout.wg_warzone_progress_kd, this));
    }

    public void setData(int kills, int deaths) {
        mProgressKills.setProgress(0);
        mProgressDeaths.setProgress(0);

        int max = kills + deaths;

        hideProgress();

        mProgressKills.setMax(max);
        mProgressDeaths.setMax(max);
        mProgressDeaths.setProgress(max / 2);

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(mProgressKills, "progress", 0, kills / 2);
        progressAnimator.setDuration(1000);
        progressAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        progressAnimator.start();
    }

    public void showProgress() {
        mWarzoneProgressBar.setVisibility(VISIBLE);
        mProgressViews.setVisibility(INVISIBLE);
    }

    public void hideProgress() {
        mWarzoneProgressBar.setVisibility(INVISIBLE);
        mProgressViews.setVisibility(VISIBLE);
    }

}
