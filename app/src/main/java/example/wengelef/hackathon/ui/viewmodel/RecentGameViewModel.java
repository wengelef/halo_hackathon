package example.wengelef.hackathon.ui.viewmodel;

import android.databinding.BaseObservable;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;

import example.wengelef.hackathon.R;
import example.wengelef.hackathon.constants.PlayerResult;
import example.wengelef.hackathon.constants.Playlists;
import example.wengelef.hackathon.rest.model.metadata.GameBaseVariant;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.stats.ServiceRecordResult;
import example.wengelef.hackathon.util.DateFormatter;
import io.realm.Realm;

/**
 * Created by fwengelewski on 9/21/16.
 *
 * ViewModel for Recent Games pages
 */
public class RecentGameViewModel extends BaseObservable {

    @Nullable
    private ServiceRecordResult mServiceRecordResult;

    @Nullable
    private MapData mMapData = null;

    @Nullable
    private GameBaseVariant mGameBaseVariant = null;

    @Nullable
    private Playlist mPlaylist = null;

    public RecentGameViewModel(@Nullable final ServiceRecordResult serviceRecordResult) {
        mServiceRecordResult = serviceRecordResult;

        if (mServiceRecordResult != null) {
            mMapData = Realm.getDefaultInstance().where(MapData.class).equalTo("id", mServiceRecordResult.getMapId()).findFirst();
            mGameBaseVariant = Realm.getDefaultInstance().where(GameBaseVariant.class).equalTo("id", mServiceRecordResult.getGameBaseVariantId()).findFirst();
            mPlaylist = Realm.getDefaultInstance().where(Playlist.class).equalTo("id", mServiceRecordResult.getHopperId()).findFirst();
            notifyChange();
        }
    }

    public String getMapName() {
        if (mMapData != null) {
            return mMapData.getName();
        }
        return "";
    }

    public String getMapImageUrl() {
        if (mMapData != null) {
            return mMapData.getImageUrl();
        }
        return "";
    }

    public String getGameVariantIconUrl() {
        if (mGameBaseVariant != null) {
            return mGameBaseVariant.getIconUrl();
        }
        return "";
    }

    public String getGameVariantName() {
        if (mGameBaseVariant != null) {
            return mGameBaseVariant.getName();
        }
        return "";
    }

    public String getResult() {
        if (mServiceRecordResult != null) {
            return mServiceRecordResult.formattedTeamScores();
        }
        return "";
    }

    public String getMatchDate() {
        if (mServiceRecordResult != null) {
            return DateFormatter.formattedDate(mServiceRecordResult.getMatchCompletedDate().getISO8601Date());
        }
        return "";
    }

    public String getMatchDuration() {
        if (mServiceRecordResult != null) {
            return DateFormatter.formattedDuration(mServiceRecordResult.getMatchDuration());
        }
        return "";
    }

    public String getPlaylistName() {
        if (mPlaylist != null) {
            return mPlaylist.getName();
        }
        return Playlists.CUSTOM.getName();
    }

    public boolean getPlayerHasWon() {
        if (mServiceRecordResult != null) {
            return PlayerResult.playerHasWon(mServiceRecordResult.getPlayers().get(0).getResult());
        }
        // always winning!
        return true;
    }
}
