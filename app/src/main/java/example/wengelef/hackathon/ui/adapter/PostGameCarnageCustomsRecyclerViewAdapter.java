package example.wengelef.hackathon.ui.adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import example.wengelef.hackathon.R;
import example.wengelef.hackathon.rest.model.metadata.TeamColor;
import example.wengelef.hackathon.rest.model.stats.PlayerStat;
import example.wengelef.hackathon.rest.model.stats.TeamStat;
import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.ui.viewholder.RecyclerviewItemCarnageCustomsHolder;
import example.wengelef.hackathon.ui.viewholder.RecyclerviewItemCarnageWarzoneHolder;
import io.realm.Realm;

/**
 * Created by fwengelewski on 6/10/16.
 */
public class PostGameCarnageCustomsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private @NonNull List<PlayerStat> mPlayers;
    private @NonNull TeamStat mTeamStat;

    public void setPlayers(@NonNull List<PlayerStat> players, @NonNull TeamStat teamStat) {
        mPlayers = players;
        mTeamStat = teamStat;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return new PostGameCarnageHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_carnage_arena_header, parent, false));
        }
        return new RecyclerviewItemCarnageCustomsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_carnage_warzone, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RecyclerviewItemCarnageCustomsHolder) {
            // Reduce position because of Header slot
            position = position-1;

            RecyclerviewItemCarnageCustomsHolder viewHolder = (RecyclerviewItemCarnageCustomsHolder) holder;

            viewHolder.getPlayerName().setText(mPlayers.get(position).getPlayer().getGamerTag());
            viewHolder.getPlayerKills().setText(String.format("%d", mPlayers.get(position).getTotalKills()));
            viewHolder.getPlayerDeaths().setText(String.format("%d", mPlayers.get(position).getTotalDeaths()));
        } else {
            PostGameCarnageHeaderViewHolder viewholder = (PostGameCarnageHeaderViewHolder) holder;

            TeamColor teamColor = Realm.getDefaultInstance().where(TeamColor.class).equalTo("id", mTeamStat.getTeamId().toString()).findFirst();

            if (teamColor != null) {
                ImageLoader.loadFittedImage(teamColor.getIconUrl(), viewholder.getTeamEmblem());
                viewholder.getTeamColor().setBackgroundColor(Color.parseColor(teamColor.getColor()));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mPlayers.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }
}
