package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RedirectObserver;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.RxUtil.TextEventStringConverter;
import example.wengelef.hackathon.RxUtil.TextIsEmptyFilter;
import example.wengelef.hackathon.event.PlayerIdEvent;
import example.wengelef.hackathon.event.SearchEvent;
import example.wengelef.hackathon.rest.api.RestProfileInterface;
import example.wengelef.hackathon.rest.controller.RestProfileController;
import example.wengelef.hackathon.ui.widget.ProgressImageView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Flo on 10.04.2016.
 * <p>
 * <p>
 * SpartanSearchFragment
 * <p>
 * Search for a Spartan and Display his SpartanImage, EmblemImage
 */
public class SpartanSearchFragment extends BaseSpartanFragment {

    public interface DataLoadedListener<DataType> {
        void onDataLoaded(DataType data);
    }

    @BindView(R.id.emblem_image)
    ProgressImageView mEmblemImage;

    @BindView(R.id.spartan_image)
    ProgressImageView mSpartanImage;

    @BindView(R.id.spartan_name_search)
    EditText mSpartanNameSearchEditText;

    @BindView(R.id.text_error_background)
    CardView mErrorView;

    @Inject RestProfileController mProfileRestController;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_spartan_search;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RxTextView.textChangeEvents(mSpartanNameSearchEditText)
                .debounce(1500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new TextEventStringConverter())
                .filter(new TextIsEmptyFilter())
                .subscribe(new SimpleObserver<String>() {

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mErrorView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onNext(String name) {
                        super.onNext(name);

                        mErrorView.setVisibility(View.INVISIBLE);

                        EventBus.getDefault().post(new SearchEvent(name));

                        mEmblemImage.showProgress();
                        mSpartanImage.showProgress();

                        mProfileRestController.getSpartanImage(name, RestProfileInterface.EMBLEM_SIZE_512, RestProfileInterface.CROP_MODE_PORTRAIT)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new RedirectObserver(mSpartanImage) {
                                    @Override
                                    protected void onNotFound() {
                                        mErrorView.setVisibility(View.VISIBLE);
                                    }
                                });

                        mProfileRestController.getEmblemImage(name, RestProfileInterface.EMBLEM_SIZE_512)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new RedirectObserver(mEmblemImage) {
                                    @Override
                                    protected void onNotFound() {
                                        mErrorView.setVisibility(View.VISIBLE);
                                    }
                                });
                    }
                });
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(PlayerIdEvent playerIdEvent) {
        mSpartanNameSearchEditText.setText(playerIdEvent.getPlayerId().getGamerTag());
    }
}
