package example.wengelef.hackathon.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.event.MenuEvent;
import example.wengelef.hackathon.rest.controller.RestMetadataController;
import example.wengelef.hackathon.rest.model.metadata.Commendation;
import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;
import example.wengelef.hackathon.rest.model.metadata.Medal;
import example.wengelef.hackathon.rest.model.metadata.Playlist;
import example.wengelef.hackathon.rest.model.metadata.Season;
import example.wengelef.hackathon.rest.model.metadata.Skull;
import example.wengelef.hackathon.rest.model.metadata.SpartanRank;
import example.wengelef.hackathon.rest.model.metadata.TeamColor;
import example.wengelef.hackathon.rest.model.metadata.Weapon;
import example.wengelef.hackathon.ui.adapter.MainPagerAdapter;
import example.wengelef.hackathon.ui.dialog.ProgressDialogFragment;
import example.wengelef.hackathon.ui.widget.MainViewPager;
import example.wengelef.hackathon.util.RealmHelper;
import io.realm.Realm;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func9;
import rx.schedulers.Schedulers;

public class MainActivity extends SpartanActivity {

    @BindView(R.id.fragment_viewpager)
    MainViewPager mViewPager;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.drawer_list)
    NavigationView mNavigationView;

    @BindView(R.id.app_bar)
    AppBarLayout mAppBarLayout;

    private ActionBarDrawerToggle mDrawerToggle;

    private NavigationView.OnNavigationItemSelectedListener mNavigationClickListener = null;

    @Inject RestMetadataController mRestMetadataController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);

        getApp().getRestComponent().inject(this);

        ButterKnife.bind(this);

        // Set inherited fields
        mCollapsingToolbar = ButterKnife.findById(this, R.id.collapsing_toolbar);
        mProgressBar = ButterKnife.findById(this, R.id.buttery_progress_bar);

        fetchMetaData();

        mViewPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
        mViewPager.setOffscreenPageLimit(5);

        mNavigationView.setNavigationItemSelectedListener(getNavigationListener());

        mCollapsingToolbar.setExpandedTitleColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));

        // Fuck you ActionBar!
        setSupportActionBar((Toolbar) ButterKnife.findById(this, R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close
        );
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    /**
     * Fetch some MetaData and save it to DB
     * TODO theres a better solution for this
     */
    private void fetchMetaData() {
        if (!RealmHelper.existsInDB(Season.class)) {
            final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance();
            progressDialogFragment.show(getSupportFragmentManager(), "Progress");

            Observable.zip(
                    mRestMetadataController.getSeasons(),
                    mRestMetadataController.getPlaylists().delaySubscription(2, TimeUnit.SECONDS),
                    mRestMetadataController.getSpartanRanks().delaySubscription(4, TimeUnit.SECONDS),
                    mRestMetadataController.getCsrDesignations().delaySubscription(6, TimeUnit.SECONDS),
                    mRestMetadataController.getSkulls().delaySubscription(8, TimeUnit.SECONDS),
                    mRestMetadataController.getMedals().delaySubscription(10, TimeUnit.SECONDS),
                    mRestMetadataController.getCommendations().delaySubscription(12, TimeUnit.SECONDS),
                    mRestMetadataController.getWeapons().delaySubscription(14, TimeUnit.SECONDS),
                    mRestMetadataController.getTeamColors().delaySubscription(16, TimeUnit.SECONDS),
                    new Func9<List<Season>, List<Playlist>, List<SpartanRank>, List<CsrDesignation>, List<Skull>, List<Medal>, List<Commendation>, List<Weapon>, List<TeamColor>, Object>() {
                        @Override
                        public Object call(
                                final List<Season> seasons,
                                final List<Playlist> playlists,
                                final List<SpartanRank> spartanRanks,
                                final List<CsrDesignation> csrDesignations,
                                final List<Skull> skulls,
                                final List<Medal> medals,
                                final List<Commendation> commendations,
                                final List<Weapon> weapons,
                                final List<TeamColor> teamColors) {
                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(seasons);
                                    realm.copyToRealmOrUpdate(teamColors);
                                    realm.copyToRealmOrUpdate(spartanRanks);
                                    realm.copyToRealmOrUpdate(csrDesignations);
                                    realm.copyToRealmOrUpdate(skulls);
                                    realm.copyToRealmOrUpdate(medals);
                                    realm.copyToRealmOrUpdate(commendations);
                                    realm.copyToRealmOrUpdate(weapons);
                                    realm.copyToRealmOrUpdate(playlists);
                                }
                            });
                            return null;
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SimpleObserver<Object>() {
                        @Override
                        public void onNext(Object o) {
                            super.onNext(o);
                            progressDialogFragment.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            super.onError(e);
                            progressDialogFragment.dismiss();
                        }
                    });
        }
    }

    private void navigate(int menuItemId) {
        MenuItem item = mNavigationView.getMenu().findItem(menuItemId);

        if (!item.isChecked()) {
            item.setChecked(true);
        }
        mDrawerLayout.closeDrawers();

        switch (item.getItemId()) {
            case R.id.menu_spartan_stats: {
                mAppBarLayout.setExpanded(true);
                mViewPager.setCurrentItem(1);
            }
            break;

            case R.id.menu_maps: {
                mAppBarLayout.setExpanded(false);
                mViewPager.setCurrentItem(2);
            }
            break;

            case R.id.menu_campaign_missions: {
                mAppBarLayout.setExpanded(false);
                mViewPager.setCurrentItem(3);
            }
            break;

            case R.id.menu_campaign_leaderboards: {
                mAppBarLayout.setExpanded(false);
                mViewPager.setCurrentItem(4);
            }
            break;

            default:
                break;
        }
    }

    /**
     * Create NavigationClickListener
     *
     * @return NavigationClickListener
     */
    public NavigationView.OnNavigationItemSelectedListener getNavigationListener() {
        if (mNavigationClickListener == null) {
            mNavigationClickListener = new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    navigate(item.getItemId());
                    return true;
                }
            };
        }
        return mNavigationClickListener;
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(MenuEvent menuEvent) {
        navigate(menuEvent.getItemId());
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(0);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}
