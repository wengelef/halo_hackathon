package example.wengelef.hackathon.ui.viewmodel;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;

import example.wengelef.hackathon.rest.model.metadata.MapData;

/**
 * Created by fwengelewski on 9/20/16.
 *
 *
 * MapsViewModel
 *
 * ViewModel for Maps
 */
public class MapsViewModel extends BaseObservable {

    @NonNull private final MapData mMapData;

    public MapsViewModel(@NonNull final MapData mapData) {
        mMapData = mapData;
        notifyChange();
    }

    public String getName() {
        return mMapData.getName();
    }

    public String getDescription() {
        return mMapData.getDescription();
    }
}
