package example.wengelef.hackathon.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.ui.widget.ProgressImageView;

public class RecyclerviewItemCarnageWarzoneHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.player_emblem)
    ProgressImageView playerEmblem;

    @BindView(R.id.player_name)
    TextView playerName;

    @BindView(R.id.captured_bases)
    TextView capturedBases;

    @BindView(R.id.boss_kills)
    TextView bossKills;

    @BindView(R.id.spartan_kills)
    TextView spartanKills;

    @BindView(R.id.npc_kills)
    TextView npcKills;

    @BindView(R.id.player_kdr)
    TextView playerKdr;

    @BindView(R.id.player_deaths)
    TextView playerDeaths;

    public RecyclerviewItemCarnageWarzoneHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getPlayerName() {
        return playerName;
    }

    public TextView getCapturedBases() {
        return capturedBases;
    }

    public TextView getBossKills() {
        return bossKills;
    }

    public TextView getSpartanKills() {
        return spartanKills;
    }

    public TextView getNpcKills() {
        return npcKills;
    }

    public TextView getPlayerKdr() {
        return playerKdr;
    }

    public TextView getPlayerDeaths() {
        return playerDeaths;
    }

    public ProgressImageView getPlayerEmblem() {
        return playerEmblem;
    }
}
