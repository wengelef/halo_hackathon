package example.wengelef.hackathon.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.ui.widget.ProgressImageView;

public class RecyclerviewItemCarnageCustomsHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.player_name)
    TextView playerName;
    @BindView(R.id.player_kills)
    TextView playerKills;
    @BindView(R.id.player_deaths)
    TextView playerDeaths;
    @BindView(R.id.player_kdr)
    TextView playerKdr;
    @BindView(R.id.player_headshots)
    TextView playerHeadshots;
    @BindView(R.id.player_assists)
    TextView playerAssists;
    @BindView(R.id.player_perfects)
    TextView playerPerfects;

    public RecyclerviewItemCarnageCustomsHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getPlayerDeaths() {
        return playerDeaths;
    }

    public TextView getPlayerPerfects() {
        return playerPerfects;
    }

    public TextView getPlayerName() {
        return playerName;
    }

    public TextView getPlayerKills() {
        return playerKills;
    }

    public TextView getPlayerKdr() {
        return playerKdr;
    }

    public TextView getPlayerHeadshots() {
        return playerHeadshots;
    }

    public TextView getPlayerAssists() {
        return playerAssists;
    }
}
