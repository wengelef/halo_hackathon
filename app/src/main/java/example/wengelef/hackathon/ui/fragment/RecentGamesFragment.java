package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.event.SearchEvent;
import example.wengelef.hackathon.rest.controller.RestMetadataController;
import example.wengelef.hackathon.rest.controller.RestStatsController;
import example.wengelef.hackathon.rest.model.metadata.GameBaseVariant;
import example.wengelef.hackathon.rest.model.stats.PlayerMatches;
import example.wengelef.hackathon.rest.model.stats.ServiceRecordResult;
import example.wengelef.hackathon.ui.util.ZoomOutPageTransformer;
import example.wengelef.hackathon.util.RealmHelper;
import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fwengelewski on 6/8/16.
 *
 *
 * RecentGamesFragment
 *
 * Displays recent games in {@link SpartanStatsFragment}
 */
public class RecentGamesFragment extends BaseSpartanFragment {

    @BindView(R.id.recent_games_pager)
    ViewPager mRecentGamesPager;

    @Inject RestStatsController mRestStatsController;
    @Inject RestMetadataController mRestMetaDataController;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_recent_games;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecentGamesPager.setPageTransformer(true, new ZoomOutPageTransformer());

        if (!RealmHelper.existsInDB(GameBaseVariant.class)) {
            mRestMetaDataController.getGameBaseVariants()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SimpleObserver<List<GameBaseVariant>>() {
                        @Override
                        public void onNext(final List<GameBaseVariant> gameBaseVariants) {
                            super.onNext(gameBaseVariants);
                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(gameBaseVariants);
                                }
                            });
                        }
                    });
        }
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(SearchEvent searchEvent) {
        mRestStatsController.getPlayerMatches(searchEvent.getName(), null, 0, 10)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<PlayerMatches>() {
                    @Override
                    public void onNext(PlayerMatches playerMatches) {
                        super.onNext(playerMatches);
                        mRecentGamesPager.setAdapter(new RecentGamesPagerAdapter(getChildFragmentManager(), playerMatches));
                    }
                });
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    private static class RecentGamesPagerAdapter extends FragmentStatePagerAdapter {

        @NonNull
        private final List<ServiceRecordResult> mResults;

        RecentGamesPagerAdapter(FragmentManager fm, @NonNull PlayerMatches playerMatches) {
            super(fm);
            mResults = playerMatches.getResults();
        }

        @Override
        public RecentGamePagerFragment getItem(int position) {
            return RecentGamePagerFragment.newInstance(mResults.get(position));
        }

        @Override
        public int getCount() {
            return mResults.size();
        }
    }
}
