package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.RxUtil.RealmQueryFilter;
import example.wengelef.hackathon.RxUtil.SimpleObserver;
import example.wengelef.hackathon.resources.Loca;
import example.wengelef.hackathon.rest.controller.RestMetadataController;
import example.wengelef.hackathon.rest.model.metadata.MapData;
import example.wengelef.hackathon.ui.util.ZoomOutPageTransformer;
import example.wengelef.hackathon.util.RealmHelper;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * MapsFragment
 *
 * Display all Multiplayer Maps (tabbed)
 */
public class MapsFragment extends ViewPagerFragment {

    @BindView(R.id.maps_viewpager)
    ViewPager mMapsViewPager;

    @BindView(R.id.gametype_tabs)
    TabLayout mTabLayout;

    @Inject RestMetadataController mMetaDataController;

    private Subscription mMapsSubscription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_maps;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapsViewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        if (!RealmHelper.existsInDB(MapData.class)) {
            mMetaDataController.getMaps()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SimpleObserver<List<MapData>>() {
                        @Override
                        public void onNext(final List<MapData> mapDatas) {
                            super.onNext(mapDatas);
                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(mapDatas);
                                }
                            });
                        }
                    });
        }

        mMapsSubscription = Realm.getDefaultInstance().where(MapData.class).findAllAsync().asObservable()
                .filter(new RealmQueryFilter<RealmResults<MapData>>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<RealmResults<MapData>>() {
                    @Override
                    public void onNext(RealmResults<MapData> mapData) {
                        super.onNext(mapData);
                        mMapsViewPager.setAdapter(new MapsPagerAdapter(getChildFragmentManager()));
                        mTabLayout.setupWithViewPager(mMapsViewPager);
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mMapsSubscription != null && !mMapsSubscription.isUnsubscribed()) {
            mMapsSubscription.unsubscribe();
        }
    }

    public static class MapsPagerAdapter extends FragmentStatePagerAdapter {

        private String[] gameModes = { "Warzone", "Campaign", "Arena" };

        public MapsPagerAdapter(FragmentManager childFragmentManager) {
            super(childFragmentManager);
        }

        @Override
        public MapsPagerFragment getItem(int position) {
            return MapsPagerFragment.newInstance(gameModes[position]);
        }

        @Override
        public int getCount() {
            return gameModes.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return gameModes[position];
        }
    }

    @Override
    public String getTitle() {
        return Loca.getString(R.string.title_maps);
    }
}
