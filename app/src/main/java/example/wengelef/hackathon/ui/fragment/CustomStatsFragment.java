package example.wengelef.hackathon.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.rest.controller.RestStatsController;

/**
 * Created by fwengelewski on 6/8/16.
 *
 *
 * CustomStatsFragment
 *
 * Displays Campaign stats in {@link SpartanStatsFragment}
 */
public class CustomStatsFragment extends BaseSpartanStatsFragment {

    @Inject
    RestStatsController mRestStatsController;

    @Override
    protected int layoutResourceId() {
        return R.layout.fr_customs_stats;
    }

    @Override
    protected void inject() {
        getRestComponent().inject(this);
    }
}
