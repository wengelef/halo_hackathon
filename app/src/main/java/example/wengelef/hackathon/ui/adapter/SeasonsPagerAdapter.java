package example.wengelef.hackathon.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import example.wengelef.hackathon.rest.model.metadata.Season;

/**
 * Created by fwengelewski on 7/1/16.
 */

public abstract class SeasonsPagerAdapter extends FragmentStatePagerAdapter {

    @Nullable
    protected List<Season> mSeasons;

    protected int count = 0;

    public SeasonsPagerAdapter(FragmentManager fm) {
        super(fm);
        notifyDataSetChanged();
    }

    public void setData(List<Season> seasons) {
        mSeasons = seasons;
        count = seasons.size();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mSeasons != null) {
            return mSeasons.get(position).getName();
        }
        return "";
    }
}
