package example.wengelef.hackathon.ui.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.SpinKitView;

import butterknife.BindView;
import butterknife.ButterKnife;
import example.wengelef.hackathon.R;
import example.wengelef.hackathon.rest.model.stats.servicerecordwarzone.WarzoneStat;

/**
 * Created by fwengelewski on 4/21/16.
 *
 *
 * WarzoneProgressView
 *
 * Display Warzone Stats in Circular Progress Bars
 */
public class WarzoneProgressView extends FrameLayout {

    @BindView(R.id.warzone_kill_stats_progress_bar)
    SpinKitView mWarzoneProgressBar;

    @BindView(R.id.progress_views)
    FrameLayout mProgressContainer;

    @BindView(R.id.headshots)
    ProgressBar mProgressHeadshots;

    @BindView(R.id.melee)
    ProgressBar mProgressMelee;

    @BindView(R.id.assassinations)
    ProgressBar mProgressAssassinations;

    @BindView(R.id.ground_pounds)
    ProgressBar mProgressGroundPounds;

    @BindView(R.id.shoulder_bash)
    ProgressBar mProgressShoulderBash;

    @BindView(R.id.power_weapon)
    ProgressBar mProgressPowerWeapons;

    @BindView(R.id.grenade)
    ProgressBar mProgressGrenades;

    public WarzoneProgressView(Context context) {
        super(context);
        init();
    }

    public WarzoneProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WarzoneProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        ButterKnife.bind(inflate(getContext(), R.layout.wg_warzone_kills_progress, this));
    }

    public void setData(WarzoneStat warzoneStat) {
        mProgressHeadshots.setProgress(0);
        mProgressAssassinations.setProgress(0);
        mProgressGrenades.setProgress(0);
        mProgressGroundPounds.setProgress(0);
        mProgressMelee.setProgress(0);
        mProgressPowerWeapons.setProgress(0);
        mProgressShoulderBash.setProgress(0);

        hideProgress();

        mProgressHeadshots.setMax(warzoneStat.getTotalKills());
        mProgressAssassinations.setMax(warzoneStat.getTotalKills());
        mProgressGrenades.setMax(warzoneStat.getTotalKills());
        mProgressGroundPounds.setMax(warzoneStat.getTotalKills());
        mProgressMelee.setMax(warzoneStat.getTotalKills());
        mProgressPowerWeapons.setMax(warzoneStat.getTotalKills());
        mProgressShoulderBash.setMax(warzoneStat.getTotalKills());

        ObjectAnimator headshotAnimator = ObjectAnimator.ofInt(mProgressHeadshots, "progress", 0, warzoneStat.getTotalHeadshots());
        headshotAnimator.setDuration(1000);
        headshotAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        headshotAnimator.start();

        ObjectAnimator assassinationAnimator = ObjectAnimator.ofInt(mProgressAssassinations, "progress", 0, warzoneStat.getTotalAssassinations());
        assassinationAnimator.setDuration(1000);
        assassinationAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        assassinationAnimator.start();

        ObjectAnimator grenadesAnimator = ObjectAnimator.ofInt(mProgressGrenades, "progress", 0, warzoneStat.getTotalGrenadeKills());
        grenadesAnimator.setDuration(1000);
        grenadesAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        grenadesAnimator.start();

        ObjectAnimator groundpoundAnimator = ObjectAnimator.ofInt(mProgressGroundPounds, "progress", 0, warzoneStat.getTotalGroundPoundKills());
        groundpoundAnimator.setDuration(1000);
        groundpoundAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        groundpoundAnimator.start();

        ObjectAnimator meleeAnimator = ObjectAnimator.ofInt(mProgressMelee, "progress", 0, warzoneStat.getTotalMeleeKills());
        meleeAnimator.setDuration(1000);
        meleeAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        meleeAnimator.start();

        ObjectAnimator powerWeaponsAnimator = ObjectAnimator.ofInt(mProgressPowerWeapons, "progress", 0, warzoneStat.getTotalPowerWeaponKills());
        powerWeaponsAnimator.setDuration(1000);
        powerWeaponsAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        powerWeaponsAnimator.start();

        ObjectAnimator shoulderBashAnimator = ObjectAnimator.ofInt(mProgressShoulderBash, "progress", 0, warzoneStat.getTotalShoulderBashKills());
        shoulderBashAnimator.setDuration(1000);
        shoulderBashAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        shoulderBashAnimator.start();
    }

    public void showProgress() {
        mWarzoneProgressBar.setVisibility(VISIBLE);
        mProgressContainer.setVisibility(INVISIBLE);
    }

    public void hideProgress() {
        mWarzoneProgressBar.setVisibility(INVISIBLE);
        mProgressContainer.setVisibility(VISIBLE);
    }
}
