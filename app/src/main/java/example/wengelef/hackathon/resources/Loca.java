package example.wengelef.hackathon.resources;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import java.util.Locale;

/**
 * Created by fwengelewski on 4/15/16.
 *
 *
 * Loca
 *
 * Convenience to get Strings
 */
public class Loca {

    private Context mContext = null;

    private static Loca instance = null;

    public static void initialize(@NonNull Context context) {
        getInstance().mContext = context;
    }

    private static Loca getInstance() {
        if (instance == null) {
            instance = new Loca();
        }
        return instance;
    }

    private static boolean initialized() {
        if (getInstance().mContext == null) {
            throw new IllegalStateException("Loca Context was not initialized, initialize it in your Application");
        }
        return true;
    }

    @NonNull
    public static String getString(@StringRes int id) {
        if (initialized()) {
            return getInstance().mContext.getString(id);
        }
        return "";
    }

    /**
     * Format String with US Locale
     *
     * @param format
     * @param args
     * @return formatted String
     */
    public static String format(@NonNull String format, @NonNull Object... args) {
        return String.format(Locale.US, format, args);
    }
}
