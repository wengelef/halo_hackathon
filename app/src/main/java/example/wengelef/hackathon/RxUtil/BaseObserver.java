package example.wengelef.hackathon.RxUtil;

import android.support.annotation.CallSuper;

import org.greenrobot.eventbus.EventBus;

import example.wengelef.hackathon.event.ProgressEvent;
import rx.Observer;
import timber.log.Timber;

/**
 * Created by Flo on 10.04.2016.
 *
 *
 * BaseObserver
 *
 * Adds ProgressEvent to all ObserverCallbacks
 */
public abstract class BaseObserver<ResponseType> implements Observer<ResponseType> {

    @CallSuper
    @Override
    public void onCompleted() {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.HIDE));
    }

    @CallSuper
    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.HIDE, e));
        Timber.e("onError : %s", ExceptionUtil.printException(e));
    }

    @CallSuper
    @Override
    public void onNext(ResponseType responseType) {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.Type.HIDE));
    }
}
