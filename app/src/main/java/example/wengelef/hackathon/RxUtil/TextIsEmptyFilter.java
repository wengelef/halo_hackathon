package example.wengelef.hackathon.RxUtil;

import android.text.TextUtils;

import rx.functions.Func1;

/**
 * Created by Flo on 10.04.2016.
 *
 *
 * TextIsEmptyFilter
 *
 * RxUtil for filtering empty Strings
 */
public class TextIsEmptyFilter implements Func1<String, Boolean> {
    @Override
    public Boolean call(String s) {
        return !TextUtils.isEmpty(s);
    }
}
