package example.wengelef.hackathon.RxUtil;

/**
 * Created by fwengelewski on 4/13/16.
 *
 *
 * ExceptionUtil
 *
 * Print an Exception
 */
public class ExceptionUtil {
    public static String printException(Throwable t) {
        String message;
        if (t.getCause() != null) {
            message =  t.getCause().getLocalizedMessage();
        } else {
            message =  t.getClass().getCanonicalName();
        }

        if (t.getLocalizedMessage() != null) {
            message = String.format("%s \n%s", message, t.getLocalizedMessage());
        }
        return message;
    }
}
