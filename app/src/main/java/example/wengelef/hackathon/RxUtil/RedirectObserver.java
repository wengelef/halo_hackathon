package example.wengelef.hackathon.RxUtil;

import android.support.annotation.NonNull;

import java.net.HttpURLConnection;

import example.wengelef.hackathon.rest.util.ImageLoader;
import example.wengelef.hackathon.rest.util.RedirectHelper;
import example.wengelef.hackathon.ui.widget.ProgressImageView;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by fwengelewski on 4/7/16.
 *
 *
 * RedirectObserver
 *
 * Observe Http Redirects and Load Images with Picasso
 */
public abstract class RedirectObserver extends BaseObserver<Void> {

    private final @NonNull ProgressImageView mImageView;

    public RedirectObserver(@NonNull ProgressImageView imageView) {
        mImageView = imageView;
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        mImageView.hideProgress();
        if (e instanceof HttpException) {
            if (((HttpException) e).code() == HttpURLConnection.HTTP_MOVED_TEMP) {
                onRedirect(RedirectHelper.getRedirectUrl(((HttpException) e).response()));
            } else if (((HttpException) e).code() == HttpURLConnection.HTTP_NOT_FOUND) {
                onNotFound();
            }
        }
    }

    protected void onRedirect(String url) {
        ImageLoader.loadFittedImage(url, mImageView);
    }

    protected abstract void onNotFound();
}
