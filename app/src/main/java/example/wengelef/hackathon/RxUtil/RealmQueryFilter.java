package example.wengelef.hackathon.RxUtil;

import io.realm.RealmResults;
import rx.functions.Func1;

/**
 * Created by fwengelewski on 9/20/16.
 *
 * Convenience Filter for Realm Query observables
 */
public class RealmQueryFilter<Model extends RealmResults> implements Func1<Model, Boolean> {

    @Override
    public Boolean call(Model model) {
        return model.isLoaded() && !model.isEmpty();
    }
}
