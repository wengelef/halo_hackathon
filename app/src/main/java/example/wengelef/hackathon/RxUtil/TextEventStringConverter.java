package example.wengelef.hackathon.RxUtil;

import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import rx.functions.Func1;

/**
 * Created by Flo on 10.04.2016.
 *
 *
 * TextEventStringConverter
 *
 * Use for RxBinding TextViewTextChangeEvent mapping
 */
public class TextEventStringConverter implements Func1<TextViewTextChangeEvent, String> {
    @Override
    public String call(TextViewTextChangeEvent textViewTextChangeEvent) {
        return textViewTextChangeEvent.text().toString().trim();
    }
}
