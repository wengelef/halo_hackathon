package example.wengelef.hackathon.RxUtil;

/**
 * Created by fwengelewski on 4/6/16.
 *
 *
 * SimpleObserver
 *
 * Convenience Observer, if you don't want to specifically handle <code>onError</code> or <code>onCompleted</code> Events
 */
public abstract class SimpleObserver<ResponseType> extends BaseObserver<ResponseType> {
}
