package example.wengelef.hackathon.util;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by fwengelewski on 5/6/16.
 *
 *
 * RealmHelper
 *
 * Helper for <code>RealmIO</code> operations
 */
public class RealmHelper {

    /**
     * Checks if an RealmObject already exists in DB
     * @param model object
     * @param <T> RealmObject
     * @return true if exists, false otherwise
     */
    public static <T extends RealmObject> boolean existsInDB(T model) {
        return Realm.getDefaultInstance().where(model.getClass()).count() > 0;
    }

    /**
     * Checks if an RealmObject already exists in DB
     * @param clazz Class of RealmObject
     * @return true if exists, false otherwise
     */
    public static boolean existsInDB(Class clazz) {
        return Realm.getDefaultInstance().where(clazz).count() > 0;
    }
}
