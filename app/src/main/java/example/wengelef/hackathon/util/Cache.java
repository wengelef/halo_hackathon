package example.wengelef.hackathon.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import example.wengelef.hackathon.rest.model.metadata.Season;
import timber.log.Timber;

/**
 * Created by fwengelewski on 9/22/16.
 *
 *
 * Cache
 *
 * Store Cache values For Models
 */
public class Cache {

    private static final String SHARED_PREFS_ID = "hackathon_cache";

    private static final Map<Class, Integer> mCacheConfigs;
    static {
        Map<Class, Integer> configs = new HashMap<>();
        configs.put(Season.class, 14);
        mCacheConfigs = Collections.unmodifiableMap(configs);
    }

    private static Cache instance = null;

    @NonNull
    private final SharedPreferences mPreferences;

    private Cache(@NonNull final Context context) {
        mPreferences = context.getSharedPreferences(SHARED_PREFS_ID, Context.MODE_PRIVATE);
    }

    public static void initialize(@NonNull final Context context) {
        if (instance == null) {
            instance = new Cache(context);
        }
    }

    /**
     * Check if a Model Classes cache is Expired
     * @param clazz Model to check
     * @return true if Model cache is expired, false if still valid
     */
    public static boolean cacheExpired(Class clazz) {
        if (instance != null) {
            String cacheExpiry = instance.mPreferences.getString(clazz.getSimpleName(), "");

            if (!cacheExpiry.isEmpty()) {
                return DateTime.parse(cacheExpiry).isBefore(DateTime.now());
            }
        }
        return false;
    }

    /**
     * Set cache expiry Date for a Model Class
     * Will do nothing is theres no assotiated Cache config for the Clas
     * @param clazz Model to set Cache value for
     */
    public static void setCache(Class clazz) {
        if (!mCacheConfigs.containsKey(clazz)) {
            Timber.w("No cache Config for this Model Class");
            return;
        }
        if (instance != null) {
            String cacheExpiry = DateTime.now().plusDays(mCacheConfigs.get(clazz)).toString();
            Timber.v("Cache for %s expires on %s", clazz.getSimpleName(), cacheExpiry);
            instance.mPreferences.edit().putString(clazz.getSimpleName(), cacheExpiry).apply();
        }
    }
}
