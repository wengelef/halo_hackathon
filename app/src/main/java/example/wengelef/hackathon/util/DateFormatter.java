package example.wengelef.hackathon.util;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 * Created by fwengelewski on 7/11/16.
 *
 *
 * DateFormatter
 *
 * Format Dates
 */
public class DateFormatter {
    /**
     * Format Date to MM.dd.yyyy
     * Ex. 2016-03-27T00:00:00Z -> 06.05.2016
     * @return formatted Match Date
     */
    @NonNull
    public static String formattedDate(String date) {
        return DateTimeFormat.forPattern("MM.dd.yyyy").print(new DateTime(date));
    }

    /**
     * Format Duration to HH:MM:ss
     * Ex. PT6M34.1909034S -> 00:06:34
     * @return formatted Match Duration
     */
    @NonNull
    public static String formattedDuration(String duration) {
        return new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendHours().appendSeparator(":")
                .appendMinutes().printZeroIfSupported().minimumPrintedDigits(2).appendSeparator(":")
                .appendSeconds().minimumPrintedDigits(2)
                .toFormatter().print(Period.parse(duration).normalizedStandard());
    }
}
