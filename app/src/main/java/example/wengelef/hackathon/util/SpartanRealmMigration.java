package example.wengelef.hackathon.util;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import timber.log.Timber;

/**
 * Created by fwengelewski on 6/6/16.
 */

public class SpartanRealmMigration implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        Timber.i("Migration from version %d to %d", oldVersion, newVersion);
        if (newVersion == 1) {
                /*
                    PlayerId
                 */
            RealmObjectSchema playerIdSchema = realm.getSchema().create("PlayerId");
            playerIdSchema.addField("GamerTag", String.class);

                /*
                    Score
                 */
            RealmObjectSchema scoreSchema = realm.getSchema().create("Score");
            scoreSchema.addField("Tier", Integer.class);
            scoreSchema.addField("DesignationId", Integer.class);
            scoreSchema.addField("Csr", Integer.class);
            scoreSchema.addField("PercentToNextTier", Integer.class);
            scoreSchema.addField("Rank", Integer.class);

                /*
                    LeaderboardResult
                 */
            RealmObjectSchema leaderboardResultSchema = realm.getSchema().create("LeaderboardResult");
            //leaderboardResultSchema.addRealmObjectField("PlayerId", playerIdSchema);
            leaderboardResultSchema.addField("Rank", Integer.class);
            leaderboardResultSchema.addRealmObjectField("Score", scoreSchema);

                /*
                    Leaderboard
                 */
            RealmObjectSchema leaderboardSchema = realm.getSchema().create("Leaderboards");
            leaderboardSchema.addField("Start", Integer.class, FieldAttribute.PRIMARY_KEY);
            leaderboardSchema.addField("Count", Integer.class);
            leaderboardSchema.addField("ResultCount", Integer.class);
            leaderboardSchema.addRealmListField("LeaderboardResults", leaderboardResultSchema);
        } else if (newVersion == 2) {
            realm.getSchema().get("PlayerId").renameField("Gamertag", "GamerTag");
            realm.getSchema().get("PlayerId").setNullable("GamerTag", true);

                /*
                    Season change isActive type From String to Boolean
                 */
            RealmObjectSchema seasonObjectSchema = realm.getSchema().get("Season");
            seasonObjectSchema.addField("isActive_tmp", Boolean.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            Boolean value = Boolean.valueOf(obj.getString("isActive"));
                            obj.setBoolean("isActive_tmp", value);
                        }
                    })
                    .removeField("isActive")
                    .renameField("isActive_tmp", "isActive");
        } else if (newVersion == 3) {
                /*
                    CsrDesignation change type of id from String to Integer
                 */
            RealmObjectSchema CsrDesignationSchema = realm.getSchema().get("CsrDesignation");
            CsrDesignationSchema.addField("id_tmp", Integer.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            Integer value = Integer.valueOf(obj.getString("id"));
                            obj.setInt("id_tmp", value);
                        }
                    })
                    .removeField("id")
                    .renameField("id_tmp", "id");

        }
    }

}
