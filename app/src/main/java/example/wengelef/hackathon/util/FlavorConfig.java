package example.wengelef.hackathon.util;

import android.os.Build;
import android.support.annotation.NonNull;

import io.fabric.sdk.android.BuildConfig;

/**
 * Created by fwengelewski on 6/7/16.
 *
 *
 * FlavorConfig
 *
 * Helper for Flavor Configurations
 */

public enum FlavorConfig {

    dev("dev"),
    acpt("acpt"),
    prod("prod");

    FlavorConfig(@NonNull String name) {
        this.name = name;
    }

    private final @NonNull String name;

    public static boolean acpt() {
        return BuildConfig.FLAVOR.equals(acpt.name);
    }

    public static boolean dev() {
        return BuildConfig.FLAVOR.equals(dev.name);
    }

    public static boolean prod() {
        return BuildConfig.FLAVOR.equals(prod.name);
    }
}