package example.wengelef.hackathon.util;

import java.util.List;

import example.wengelef.hackathon.rest.model.stats.Team;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * ServiceRecordHelper
 *
 * Helper for the ServiceRecord Model
 */
public class ServiceRecordHelper {

    /**
     * Check if player has won the Match
     * @param teams participating teams
     * @param playerTeamId Team Id of the Player
     * @return true if Player has won the Match
     */
    public static boolean playerHasWon(List<Team> teams, Integer playerTeamId) {
        for (Team team : teams) {
            if (team.getRank().equals(1)) {
                if (team.getId().equals(playerTeamId)) {
                    return true;
                }
            }
        }
        return false;
    }
}
