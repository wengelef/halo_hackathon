package example.wengelef.hackathon.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import example.wengelef.hackathon.rest.model.metadata.CsrDesignation;

/**
 * Created by fwengelewski on 5/6/16.
 *
 *
 * CsrHelper
 *
 * Helper for the Csr/CsrDesignationModel
 */
public class CsrHelper {

    static CsrHelper instance;

    @NonNull
    private final List<CsrDesignation> mCsrDesignations;

    private CsrHelper(@NonNull List<CsrDesignation> csrDesignations) {
        mCsrDesignations = csrDesignations;
    }

    public static void initialize(@NonNull List<CsrDesignation> csrDesignations) {
        if (instance == null) {
            instance = new CsrHelper(csrDesignations);
        }
    }

    @Nullable
    public static CsrDesignation getDesignationForId(int designationId) {
        for (CsrDesignation designation : instance.mCsrDesignations) {
            if (designation.getId() == designationId) {
                return designation;
            }
        }
        return null;
    }
}
