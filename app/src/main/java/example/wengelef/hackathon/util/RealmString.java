package example.wengelef.hackathon.util;

import android.support.annotation.Nullable;

import io.realm.RealmObject;

/**
 * Created by fwengelewski on 5/5/16.
 *
 *
 * RealmString
 *
 * <code>Realm</code> doesn't support Strings for some ridiculous Reason.
 */
public class RealmString extends RealmObject {

    public RealmString() {
        this.val = null;
    }

    public RealmString(@Nullable String val) {
        this.val = val;
    }

    @Nullable
    private String val;

    public String get() {
        return val;
    }

    public void set(@Nullable String val) {
        this.val = val;
    }
}
