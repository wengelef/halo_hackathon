package example.wengelef.hackathon.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by fwengelewski on 9/20/16.
 */
public class StringUtil {

    @NonNull
    public static String sanitize(@Nullable String value) {
        if (value != null) {
            return value;
        }
        return "";
    }
}
