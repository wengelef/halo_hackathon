package example.wengelef.hackathon.constants;

import android.support.annotation.NonNull;

/**
 * Created by fwengelewski on 4/18/16.
 *
 * GameMode
 *
 * Enum for {@link example.wengelef.hackathon.rest.api.RestStatsInterface.GameMode}
 */
public enum Playlists {
    ERROR(0, "Error"), ARENA(1, "Arena"), CAMPAIGN(2, "Campaign"), CUSTOM(3, "Custom"), WARZONE(4, "Warzone");

    Playlists(Integer mode, String name) {
        mId = mode;
        mName = name;
    }

    final Integer mId;

    final String mName;

    public Integer getId() {
        return mId;
    }

    @NonNull
    public String getName() {
        return mName;
    }

    public static String fromInteger(Integer mode) {
        switch (mode) {
            case 0: {
                return ERROR.mName;
            }
            case 1: {
                return ARENA.mName;
            }
            case 2: {
                return CAMPAIGN.mName;
            }
            case 3: {
                return CUSTOM.mName;
            }
            case 4: {
                return WARZONE.mName;
            }
        }
        return ERROR.mName;
    }
}
