package example.wengelef.hackathon.constants;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * PlayerResult
 *
 * Enum if Player has won a Match
 */
public enum  PlayerResult {
    DNF(0, "Did not finish"), LOST(1, "Lost"), TIED(2, "Tied"), WON(3, "Won");

    PlayerResult(Integer result, String name) {
        mResult = result;
        mName = name;
    }

    final Integer mResult;
    final String mName;

    public static boolean playerHasWon(Integer id) {
        return id.equals(WON.mResult);
    }

    public static String fromInteger(Integer result) {
        switch (result) {
            case 0: {
                return DNF.mName;
            }
            case 1: {
                return LOST.mName;
            }
            case 2: {
                return TIED.mName;
            }
            case 3: {
                return WON.mName;
            }
        }
        return DNF.mName;
    }
}
