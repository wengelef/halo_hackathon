package example.wengelef.hackathon.constants;

/**
 * Created by fwengelewski on 4/18/16.
 *
 *
 * Difficulty
 *
 * Campaign Difficulties
 */
public enum Difficulty {
    EASY(0, "Easy"), NORMAL(1, "Normal"), HEROIC(2, "Heroic"), LEGENDARY(3, "Legendary");

    Difficulty(Integer id, String name) {
        mId = id;
        mName = name;
    }

    private final Integer mId;
    private final String mName;

    public static String fromInteger(Integer id) {
        switch (id) {
            case 0: {
                return EASY.mName;
            }
            case 1: {
                return NORMAL.mName;
            }
            case 2: {
                return HEROIC.mName;
            }
            case 3: {
                return LEGENDARY.mName;
            }
        }
        return EASY.mName;
    }
}
