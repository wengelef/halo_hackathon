package example.wengelef.hackathon;

import junit.framework.Assert;

import org.junit.Test;

import example.wengelef.hackathon.util.StringUtil;

/**
 * Created by fwengelewski on 9/22/16.
 *
 * Test StringUtils
 */
public class StringUtilTest extends BaseTest {

    @Test
    public void test_sanitize() {
        Assert.assertNotNull(StringUtil.sanitize(null));
        Assert.assertEquals("test", StringUtil.sanitize("test"));
    }
}
