package example.wengelef.hackathon;

import example.wengelef.hackathon.ui.SpartanApplication;

/**
 * Created by fwengelewski on 9/22/16.
 */
public class SpartanTestApplication extends SpartanApplication {

    @Override
    protected void initRealm() {
        // Skip
    }

    @Override
    protected void initEventBus() {
        // Skip
    }
}
