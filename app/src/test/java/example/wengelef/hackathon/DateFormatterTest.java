package example.wengelef.hackathon;

import junit.framework.Assert;

import org.junit.Test;

import example.wengelef.hackathon.util.DateFormatter;

/**
 * Created by fwengelewski on 9/22/16.
 *
 * Test Date Formatting
 */
public class DateFormatterTest extends BaseTest {

    @Test
    public void test_dateFormat() {
        String in = "2016-09-18T00:00:00Z";
        String expected = "09.18.2016";
        Assert.assertEquals(expected, DateFormatter.formattedDate(in));
    }

    @Test
    public void test_durationFormat() {
        String in = "PT7M8.3264477S";
        String expected = "00:07:08";
        Assert.assertEquals(expected, DateFormatter.formattedDuration(in));
    }
}
